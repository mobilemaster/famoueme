package com.views.progressbar;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.View;

import com.hadif.famousme.R;

/**
 * Created by shineman on 10/13/2017.
 */

public class SMProgressBar extends View
{
    // % value of the progressbar.
    int progressBarValue = 0;

    public SMProgressBar(Context context)
    {
        super(context);
    }

    public SMProgressBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        progressBarValue = attrs.getAttributeIntValue(null, "progressBarValue", 0);
    }

    public void setValue(int value) {
        progressBarValue = value;
        invalidate();
    }

    protected synchronized void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        float cornerRadius = getHeight()/2;

        // Draw the background of the bar.
        Paint backgroundPaint = new Paint();
        backgroundPaint.setColor(getContext().getResources().getColor(R.color.progressBgColor));
        backgroundPaint.setStyle(Paint.Style.FILL);
        backgroundPaint.setAntiAlias(true);

        RectF backgroundRect = new RectF(0, 0, getWidth(), getHeight());
        canvas.drawRoundRect(backgroundRect, cornerRadius, cornerRadius, backgroundPaint);

        // Draw the progress bar.
        Paint barPaint = new Paint();
        barPaint.setColor(getContext().getResources().getColor(R.color.yellowBgColor));
        barPaint.setStyle(Paint.Style.FILL);
        barPaint.setAntiAlias(true);

        float progress = (backgroundRect.width() / 100) * progressBarValue;
        RectF barRect = new RectF(0, 0, progress, canvas.getClipBounds().bottom);
        canvas.drawRoundRect(barRect, cornerRadius, cornerRadius, barPaint);

        if (progressBarValue == 0)
            return;

        // Draw progress text in the middle.
        Paint textPaint = new Paint();
        textPaint.setColor(Color.WHITE);
        textPaint.setTextSize(46);

        String text = progressBarValue + "%";
        Rect textBounds = new Rect();

        textPaint.getTextBounds(text, 0, text.length(), textBounds);
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(),"fonts/Karla-Bold.ttf");
        textPaint.setTypeface(tf);

        //((textPaint.descent() + textPaint.ascent()) / 2) is the distance from the baseline to the center.
        int xPos = (int)barRect.centerX() - (textBounds.width()/2);
        int yPos = (int)((canvas.getHeight() / 2) - ((textPaint.descent() + textPaint.ascent()) / 2)) ;

        canvas.drawText(text, xPos, yPos, textPaint);
    }
}
