package com.views.textstyle;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.EditText;

public class EditTextBold extends EditText {
    private Context context;

    public EditTextBold(Context context) {
        super(context);
        this.context=context;
        init();
    }

    public EditTextBold(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context=context;
        init();
    }


    public EditTextBold(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context=context;
        init();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public EditTextBold(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        this.context=context;
        init();
    }

    private void init() {
        setTypeface(Typeface.createFromAsset(context.getAssets(),"fonts/Karla-Bold.ttf"));
    }
}
