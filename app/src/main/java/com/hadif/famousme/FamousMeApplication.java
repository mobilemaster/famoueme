package com.hadif.famousme;

import android.app.Application;
import android.content.Context;

import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.google.firebase.FirebaseApp;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Logger;
import com.hadif.famousme.managers.basedata.ThemeManager;
import com.hadif.famousme.utils.LocaleHelper;
import com.hadif.famousme.utils.PrefsUtils;
import com.twitter.sdk.android.core.Twitter;

import java.net.CookieHandler;
import java.net.CookieManager;

public class FamousMeApplication extends Application {
    private static Context mContext;

    @Override
    public void onCreate() {
        super.onCreate();

        //For Offline
        FirebaseDatabase.getInstance().setLogLevel(Logger.Level.DEBUG);
        FirebaseDatabase.getInstance().setLogLevel(Logger.Level.valueOf("DEBUG"));

        //FirebaseDatabase.getInstance().setPersistenceEnabled(true);
        //FirebaseDatabase.getInstance().getReference().child("conversations").keepSynced(true);

        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);

        Twitter.initialize(this);

        PrefsUtils.setContext(this);
        ThemeManager.setContext(this);
        mContext = this;

    }

    public static Context getContext(){
        return mContext;
    }
}
