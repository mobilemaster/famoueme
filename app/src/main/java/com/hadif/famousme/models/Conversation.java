package com.hadif.famousme.models;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class Conversation {
    public String userId;
    public String conversationId;
    public long timestamp = 0;
    public String lastMessage;
    public BlockUser blockUser;
    public boolean isRead = false;

    @Exclude
    public UserProfile userProfile;

    public Conversation() {
        blockUser = new BlockUser();
    }

    @Exclude
    public void setData(Conversation newData) {
        this.userId = newData.userId;
        this.timestamp = newData.timestamp;
        this.conversationId = newData.conversationId;
        this.lastMessage = newData.lastMessage;
        this.isRead = newData.isRead;
    }
}
