package com.hadif.famousme.models;

import com.google.firebase.database.IgnoreExtraProperties;


@IgnoreExtraProperties
public class Achievement {
    public String type;
    public String name;
    public long timestamp;
    public int value;
    public Achievement() {

    }
}
