package com.hadif.famousme.models;

import android.content.ContentUris;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.provider.ContactsContract;

import java.io.IOException;
import java.io.InputStream;


public class ContactObject {
    private Bitmap mContactImage;
    private String mContactName;
    private String mContactNumber;
    private long mContactId;

    public boolean hasImage() {
        if (mContactImage == null) {
            return false;
        }
        else {
            return true;
        }
    }

    public Bitmap getContactImage() {
        return mContactImage;
    }

    public void setContactImage(Bitmap contactImage) {
        mContactImage = contactImage;
    }

    public String getContactName() {
        return mContactName;
    }

    public void setContactName(String contactName) {
        mContactName = contactName;
    }

    public String getContactNumber() {
        return mContactNumber;
    }

    public void setContactNumber(String contactNumber) {
        mContactNumber = contactNumber;
    }

    public long getContactId() {
        return mContactId;
    }

    public void setContactId(long id) {
        mContactId = id;
    }

    public static Bitmap retrieveContactPhoto(Context context, long contactID) {

        Bitmap photo = null;

        try {
            InputStream inputStream = ContactsContract.Contacts.openContactPhotoInputStream(context.getContentResolver(),
                    ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, new Long(contactID)));

            if (inputStream != null) {
                photo = BitmapFactory.decodeStream(inputStream);
            }

            assert inputStream != null;
            inputStream.close();

        } catch (IOException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        return photo;
    }
}