package com.hadif.famousme.models;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

/**
 * Created by shineman on 10/9/2017.
 */

@IgnoreExtraProperties
public class Message {

    public class MessageType {
        public static final int TEXT = 0;
        public static final int IMAGE = 1;
    }

    public String messageId;
    public String senderId;
    public String receiverId;
    public long sentTimestamp = 0;
    public String text;
    public boolean isRead = false;
    public long readTimestamp = 0;
    public boolean isFirstOfDay = false;
    public boolean isNotified = false;
    public int messageType = MessageType.TEXT;
    public String imageUrl;

    public Message() {
    }

    @Exclude
    public void setData(Message newData) {
        this.messageId = newData.messageId;
        this.senderId = newData.senderId;
        this.receiverId = newData.receiverId;
        this.sentTimestamp = newData.sentTimestamp;
        this.text = newData.text;
        this.isRead = newData.isRead;
        this.isNotified = newData.isNotified;
        this.readTimestamp = newData.readTimestamp;
        this.messageType = newData.messageType;
        this.imageUrl = newData.imageUrl;
    }
}
