package com.hadif.famousme.models;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;
import com.hadif.famousme.AppConfig;

import java.util.HashMap;
import java.util.Map;

@IgnoreExtraProperties
public class Notification {
    public String key;
    public String userId;

    public UserProfile userProfile;

    public String details;
    public long timestamp = 0;
    public int notificationType = AppConfig.Notification.NOTIFICATION_VIEWED_PROFILE;
    public boolean isRead = Boolean.FALSE;


    public Notification() {
        userProfile = new UserProfile();
    }
}
