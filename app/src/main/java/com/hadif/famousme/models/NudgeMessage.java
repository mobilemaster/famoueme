package com.hadif.famousme.models;

import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class NudgeMessage {
    public String key;
    public String title;
    public String photoUrl;
    public String message;
    public String senderId;

    public NudgeMessage() {

    }
}
