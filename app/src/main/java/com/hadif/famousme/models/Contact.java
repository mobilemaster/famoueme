package com.hadif.famousme.models;

import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class Contact {
    public String contactKey;
    public String userId;

    public UserProfile userProfile;

    public Contact() {
        userProfile = new UserProfile();
    }
}
