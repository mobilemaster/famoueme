package com.hadif.famousme.models;


public class TimeGap{
    public static final int GAP_TIME_DAYS = 1;
    public static final int GAP_TIME_HOURS = 2;
    public static final int GAP_TIME_MINUTES = 3;

    public int gapValue;
    public int gapType;

    public TimeGap(){
        gapValue = 0;
        gapType = GAP_TIME_DAYS;
    }
}
