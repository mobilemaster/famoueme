package com.hadif.famousme.models;


public class Social{
    public String website;
    public String facebook;
    public String snapchat;
    public String linkedin;
    public String twitter;
    public String instagram;
    public String youtube;

    public Social(){
        website = "";
        facebook = "";
        snapchat = "";
        linkedin = "";
        twitter = "";
        instagram = "";
        youtube = "";
    }
}
