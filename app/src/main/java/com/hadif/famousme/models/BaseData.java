package com.hadif.famousme.models;

import com.google.firebase.database.IgnoreExtraProperties;

// [START blog_user_class]
@IgnoreExtraProperties
public class BaseData {
    public String name;
    public BaseData() {
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
    }

}


// [END blog_user_class]
