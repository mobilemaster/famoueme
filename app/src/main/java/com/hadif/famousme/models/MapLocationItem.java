package com.hadif.famousme.models;

import android.location.Location;

import com.firebase.geofire.GeoLocation;
import com.hadif.famousme.AppConfig;

/**
 * Created by shineman on 10/9/2017.
 */

public class MapLocationItem {
    public User user;
    public double latitude;
    public double longitude;
    public float distance;

    public MapLocationItem() {
        latitude = 0;
        longitude = 0;
    }

    /**
     * Set members
     * @param user
     * @param location
     * @param currentLocation
     */
    public void setData(User user, GeoLocation location, Location currentLocation) {
        this.user = user;
        this.latitude = location.latitude;
        this.longitude = location.longitude;

        Location userLocation = new Location("User Location");
        userLocation.setLatitude(location.latitude);
        userLocation.setLongitude(location.longitude);

        this.distance = currentLocation.distanceTo(userLocation);
    }

}
