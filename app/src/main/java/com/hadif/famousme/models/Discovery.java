package com.hadif.famousme.models;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

import java.util.HashMap;
import java.util.Map;

@IgnoreExtraProperties
public class Discovery {
    public String userKey;

    public long timestamp = 0;
    public double distance = 0;
    public String lastMessage;

    @Exclude
    public UserProfile userProfile;

    @Exclude
    public Status status;

    public Discovery() {
    }

    @Exclude
    public void setData(Discovery newData) {
        this.userKey = newData.userKey;
        this.timestamp = newData.timestamp;
        this.distance = newData.distance;
        this.lastMessage = newData.lastMessage;
    }
}
