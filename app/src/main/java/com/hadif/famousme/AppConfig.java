package com.hadif.famousme;

public class AppConfig {

    public static final int READ_MESSAGES_COUNT = 30;

    public static final int RUNNING_NORMAL = 0;
    public static final int RUNNING_GO_TO_MAP = 1;
    public static final int RUNNING_EXIT = 2;

    public static final String FCM_SERVER_KEY = "key=AIzaSyBdcYThNB7WCqqbkIxzHsZ9imBJ-aP9xd0";
    public static final String FCM_SERVER_URL = "https://fcm.googleapis.com/fcm/send";

    public static final int IMAGE_REQUIRED_SIZE = 300;
    public static final int AVATAR_REQUIRED_SIZE = 60;

    public static final String GPU_IMAGE_FOLDER  =   ".gpuimage";

    public static final int MAX_GET_DISCOVERIES_SKIP_TIME = 100;

    public class Location {

        public static final long LOCATION_INTERVAL = 2000;
        public static final long LOCATION_FASTEST_INTERVAL = 1000;
        public static final long LOCATION_SMALLEST_DISPLACEMENT = 5;

        /**
         * The desired interval for location updates. Inexact. Updates may be more or less frequent.
         */
        public static final long UPDATE_INTERVAL_IN_MILLISECONDS = 600000;

        /**
         * The fastest rate for active location updates. Exact. Updates will never be more frequent
         * than this value.
         */
        public static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS =  UPDATE_INTERVAL_IN_MILLISECONDS / 2;

        public static final int NEAR_FINDING_RADIUS = 1; //1km for map view
        public static final int DISCOVERY_FINDING_RADIUS = 40000; //40K km  this value can be controlled on setting pages.

    }

    public class Notification {
        public static final int NOTIFICATION_VIEWED_PROFILE = 1;
        public static final int NOTIFICATION_IMPORTED_CONTACT = 2;
    }

    public class Achievement {
        public static final String ACHIEVEMENT_START_CONVERSATION = "start_conversation";
        public static final String ACHIEVEMENT_USE_APP_TWO_WEEKS = "use_app_2_weeks";
        public static final String ACHIEVEMENT_IMPORT_CONTACTS = "import_contacts";
    }

    public class Request {
        public static final int REQUEST_USER_PROFILE = 1000;
        public static final int REQUEST_CHAT = 1001;
    }

    public class ChartFrom {
        public static final int FROM_DISCOVERY = 1;
        public static final int FROM_CONTACT = 2;
    }

    public class OnlineFilter {
        public static final int NO_TIME_LIMIT = 0;
        public static final int A_DAY = 1;
        public static final int A_WEEK = 2;
        public static final int A_MONTH = 3;
    }

    public class DiscoveryMode {
        public final static int RANGE_DISCOVERY_MODE = 0;
        public final static int COUNTRY_DISCOVERY_MODE = 1;
    }

}

