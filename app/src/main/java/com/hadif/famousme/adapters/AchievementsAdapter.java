package com.hadif.famousme.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.hadif.famousme.AppConfig;
import com.hadif.famousme.R;
import com.hadif.famousme.models.Achievement;
import com.hadif.famousme.utils.DateUtils;
import com.views.imageview.CircularImageView;

import java.util.List;

public class AchievementsAdapter<T> extends ArrayAdapter<Achievement> {
    Context mContext;
    LayoutInflater mLayoutInflater;

    public AchievementsAdapter(Context context) {
        super(context, R.layout.row_achievement);

        mContext = context;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    /*
     * Set List Item Data
     *
     */
    public void setData(List<Achievement> data) {
        clear();
        if (data != null) {
            addAll(data);
        }
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parentView) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = mLayoutInflater.inflate(R.layout.row_achievement, null);

            holder = new ViewHolder();
            holder.rootView = convertView;
            holder.tvName = (TextView) convertView.findViewById(R.id.tvName);
            holder.tvDateTime = (TextView)convertView.findViewById(R.id.tvDateTime);
            holder.ivIcon = (ImageView) convertView.findViewById(R.id.ivIcon);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final Achievement achievement = getItem(position);
        holder.tvName.setText(achievement.name);

        if (achievement.type.equals(AppConfig.Achievement.ACHIEVEMENT_START_CONVERSATION))
            holder.ivIcon.setImageResource(R.drawable.chats_inactive);
        else if (achievement.type.equals(AppConfig.Achievement.ACHIEVEMENT_USE_APP_TWO_WEEKS))
            holder.ivIcon.setImageResource(R.drawable.adjust);
        else if (achievement.type.equals(AppConfig.Achievement.ACHIEVEMENT_IMPORT_CONTACTS))
            holder.ivIcon.setImageResource(R.drawable.address_book);

        holder.tvDateTime.setText(DateUtils.getMonthAndDayFromTimestamp(achievement.timestamp));

        return holder.rootView;
    }

    static class ViewHolder {
        View rootView;
        TextView tvName, tvDateTime;
        ImageView ivIcon;
    }
}