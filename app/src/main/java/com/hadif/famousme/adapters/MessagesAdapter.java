package com.hadif.famousme.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.signature.StringSignature;
import com.hadif.famousme.R;
import com.hadif.famousme.activities.ChatActivity;
import com.hadif.famousme.managers.basedata.ThemeManager;
import com.hadif.famousme.models.Conversation;
import com.hadif.famousme.models.InterestItem;
import com.hadif.famousme.models.Message;
import com.hadif.famousme.utils.DateUtils;
import com.hadif.famousme.utils.FirebaseUtils;
import com.views.checkbox.SmoothCheckBox;

import java.util.ArrayList;
import java.util.List;

import jp.wasabeef.glide.transformations.CropCircleTransformation;

public class MessagesAdapter<T> extends ArrayAdapter<Message> {
    private Context mContext;
    LayoutInflater mLayoutInflater;
    private int mCurrentDays;

    MessagesAdapter.OnItemSelectedListener mOnItemSelectedListener;

    public interface OnItemSelectedListener {
        void onItemSelected(Message message, View view);
    }

    public MessagesAdapter(Context context) {
        super(context, R.layout.row_chat_send);
        mContext = context;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setOnItemSelectedListener(MessagesAdapter.OnItemSelectedListener listener) {
        mOnItemSelectedListener = listener;
    }

    /**
     * Set List Item Data
     */
    public void setData(List<Message> data) {
        clear();
        if (data != null) {
            addAll(data);
        }
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parentView) {
        ViewHolder holder = new ViewHolder();
        int viewType = getItemViewType(position);
        if (convertView == null) {
            if (viewType == ChatActivity.VIEW_TYPE_RECEIVED_MESSAGE) {
                convertView = LayoutInflater.from(mContext).inflate(R.layout.row_chat_receive, parentView, false);
                holder.rootView = convertView;
                holder.messageContainer = convertView.findViewById(R.id.messageContainer);
                holder.tvMessageText = (TextView)convertView.findViewById(R.id.tvMessageText);
                holder.ivMessageImage = convertView.findViewById(R.id.ivMessageImage);
                holder.tvTime = (TextView)convertView.findViewById(R.id.tvTime);
                holder.tvDate = (TextView)convertView.findViewById(R.id.tvDate);

            } else if (viewType == ChatActivity.VIEW_TYPE_SENT_MESSAGE) {
                convertView = LayoutInflater.from(mContext).inflate(R.layout.row_chat_send, parentView, false);
                holder.rootView = convertView;
                holder.messageContainer = convertView.findViewById(R.id.messageContainer);
                holder.tvMessageText = (TextView)convertView.findViewById(R.id.tvMessageText);
                holder.ivMessageImage = convertView.findViewById(R.id.ivMessageImage);
                holder.tvTime = (TextView)convertView.findViewById(R.id.tvTime);
                holder.tvDate = (TextView)convertView.findViewById(R.id.tvDate);
                holder.tvReadStatus = (TextView)convertView.findViewById(R.id.tvReadStatus);
            }

            convertView.setTag(holder);
        } else {
            holder = (MessagesAdapter.ViewHolder) convertView.getTag();
        }

        final Message message = getItem(position);
        ThemeManager.getInstance().setTheme(holder.tvDate);
        if (message.messageType == Message.MessageType.TEXT) {
            if (viewType == ChatActivity.VIEW_TYPE_SENT_MESSAGE )
                ThemeManager.getInstance().setTheme(holder.messageContainer);
            else if (viewType == ChatActivity.VIEW_TYPE_RECEIVED_MESSAGE)
                holder.messageContainer.setBackgroundColor(getContext().getResources().getColor(R.color.lightDarkBgColor));

            holder.tvMessageText.setVisibility(View.VISIBLE);
            holder.ivMessageImage.setVisibility(View.GONE);
            holder.tvMessageText.setText(message.text);

            holder.tvTime.setTextColor(getContext().getResources().getColor(R.color.receivedTimeColor));

        } else if (message.messageType == Message.MessageType.IMAGE) {
            if (holder.messageContainer != null)
                holder.messageContainer.setBackgroundColor(Color.TRANSPARENT);

            holder.tvMessageText.setVisibility(View.GONE);
            holder.ivMessageImage.setVisibility(View.VISIBLE);
            Glide.with(getContext()).load(message.imageUrl)
                    .signature(new StringSignature(String.valueOf(System.currentTimeMillis())))
                    .into(holder.ivMessageImage);

            holder.tvTime.setTextColor(getContext().getResources().getColor(ThemeManager.getInstance().getCurrentThemeColor()));
        }

        holder.tvTime.setText(DateUtils.getTimeFromTimestamp(message.sentTimestamp));
        setDateGroup(position, holder);
        if (holder.tvReadStatus != null) {
            if (!message.isRead) {
                holder.tvReadStatus.setVisibility(View.VISIBLE);
                if (message.isNotified)
                    holder.tvReadStatus.setText(R.string.has_been_notified);
                else
                    holder.tvReadStatus.setText(R.string.unread);

            } else {
                holder.tvReadStatus.setVisibility(View.GONE);
            }
        }

        return holder.rootView;
    }

    @Override
    public int getItemViewType(int position) {
        Log.e("Position---------------", String.valueOf(position));
        return getItem(position).senderId.equals(FirebaseUtils.getUserId()) ?
                ChatActivity.VIEW_TYPE_SENT_MESSAGE : ChatActivity.VIEW_TYPE_RECEIVED_MESSAGE;
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    /**
     * Classify messages by date.
     * @param position
     * @param holder
     */
    private void setDateGroup(int position,
                              MessagesAdapter.ViewHolder holder) {
        TextView tvDate = holder.tvDate;

        Message message = getItem(position);
        if (message.isFirstOfDay || position == 0) {
            tvDate.setVisibility(View.VISIBLE);
            tvDate.setText(DateUtils.getGapOfDateWithString(message.sentTimestamp, mContext));
        } else
            tvDate.setVisibility(View.GONE);

        //Set Theme to Date
        Drawable drawable = tvDate.getContext().getResources().getDrawable(R.drawable.round_chat_date);
        GradientDrawable gradientDrawable = (GradientDrawable)drawable;
        gradientDrawable.setColor(ContextCompat.getColor(tvDate.getContext(), ThemeManager.getInstance().getCurrentThemeColor()));
        gradientDrawable.setStroke(0, ThemeManager.getInstance().getCurrentThemeColor());
        gradientDrawable.setCornerRadius(ThemeManager.dpToPx(15));

        tvDate.setBackground(drawable);
        tvDate.setPadding(ThemeManager.dpToPx(15), ThemeManager.dpToPx(0), ThemeManager.dpToPx(15), ThemeManager.dpToPx(0));
    }

    static class ViewHolder {
        public View rootView;
        public ViewGroup messageContainer;
        public TextView tvMessageText;
        public ImageView ivMessageImage;
        public TextView tvTime;
        public TextView tvDate;
        public TextView tvReadStatus;
    }
}

