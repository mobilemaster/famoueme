package com.hadif.famousme.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.hadif.famousme.R;
import com.hadif.famousme.models.InterestItem;
import com.hadif.famousme.models.NationalityItem;
import com.views.checkbox.SmoothCheckBox;

import java.util.List;

public class NationalityAdapter<T> extends ArrayAdapter<NationalityItem> {
    Context mContext;
    LayoutInflater mLayoutInflater;
    OnItemSelectedListener mOnItemSelectedListener;

    public interface OnItemSelectedListener {
        void onItemSelected(int position, boolean isChecked);
    }

    public NationalityAdapter(Context context) {
        super(context, R.layout.row_interest);

        mContext = context;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setOnItemSelectedListener(NationalityAdapter.OnItemSelectedListener listener) {
        mOnItemSelectedListener = listener;
    }

    /*
     * Set List Item Data
     *
     */
    public void setData(List<NationalityItem> data) {
        clear();
        if (data != null) {
            addAll(data);
        }
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parentView) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = mLayoutInflater.inflate(R.layout.row_interest, parentView, false);

            holder = new ViewHolder();
            holder.rootView = convertView;
            holder.tvInterestName = (TextView) convertView.findViewById(R.id.tvInterestName);
            holder.chCheckItem = (SmoothCheckBox)convertView.findViewById(R.id.chCheckItem);
            holder.chCheckItem.setOnCheckedChangeListener(new SmoothCheckBox.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(SmoothCheckBox checkBox, boolean isChecked) {
                    mOnItemSelectedListener.onItemSelected(position, isChecked);
                }
            });

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final NationalityItem item = getItem(position);
        holder.tvInterestName.setText(item.getName());
        holder.chCheckItem.setChecked(item.isChecked());

        return convertView;
    }

    static class ViewHolder {
        View rootView;
        TextView tvInterestName;
        SmoothCheckBox chCheckItem;
    }
}