package com.hadif.famousme.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hadif.famousme.R;
import com.hadif.famousme.managers.basedata.ThemeManager;
import com.hadif.famousme.models.InterestItem;
import com.views.checkbox.SmoothCheckBox;

import java.util.ArrayList;
import java.util.List;

public class InterestsAdapter<T> extends ArrayAdapter<InterestItem> {
    Context mContext;
    LayoutInflater mLayoutInflater;
    OnItemSelectedListener mOnItemSelectedListener;
    private boolean mIsFromView = false;

    public interface OnItemSelectedListener {
        void onItemSelected(int position, boolean isChecked);
    }

    public InterestsAdapter(Context context) {
        super(context, R.layout.row_interest);

        mContext = context;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setOnItemSelectedListener(InterestsAdapter.OnItemSelectedListener listener) {
        mOnItemSelectedListener = listener;
    }

    /*
     * Set List Item Data
     *
     */
    public void setData(List<InterestItem> data) {
        clear();
        if (data != null) {
            addAll(data);
        }
    }
//    @Override
//    public View getDropDownView(int position, View convertView, ViewGroup parent) {
//        return getCustomView(position, convertView, parent);
//    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    public View getCustomView(final int position, View convertView, ViewGroup parentView) {
        ViewHolder holder;
        if (convertView == null) {
            LayoutInflater layoutInflator = LayoutInflater.from(mContext);
            convertView = layoutInflator.inflate(R.layout.row_interest, null);

            holder = new ViewHolder();
            holder.tvInterestName = (TextView) convertView.findViewById(R.id.tvInterestName);
            holder.chCheckItem = (SmoothCheckBox)convertView.findViewById(R.id.chCheckItem);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final InterestItem interestItem = getItem(position);
        holder.tvInterestName.setText(interestItem.getName());
        mIsFromView = true;
        holder.chCheckItem.setChecked(interestItem.isChecked());
        mIsFromView = false;
        holder.chCheckItem.setOnCheckedChangeListener(new SmoothCheckBox.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(SmoothCheckBox checkBox, boolean isChecked) {
                if (!mIsFromView)
                    mOnItemSelectedListener.onItemSelected(position, isChecked);
            }
        });

        return convertView;
    }

    static class ViewHolder {
        TextView tvInterestName;
        SmoothCheckBox chCheckItem;
    }
}