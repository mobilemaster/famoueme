package com.hadif.famousme.adapters;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.hadif.famousme.R;
import com.hadif.famousme.models.MapLocationItem;
import com.hadif.famousme.utils.BaseUtils;
import com.hadif.famousme.utils.DateUtils;
import com.views.imageview.CircularImageView;

import java.util.List;

import jp.wasabeef.glide.transformations.CropCircleTransformation;

public class MapLocationsAdapter<T> extends ArrayAdapter<MapLocationItem> {
    Context mContext;
    LayoutInflater mLayoutInflater;
    OnItemSelectedListener mOnItemSelectedListener;

    public interface OnItemSelectedListener {
        void onItemSelected(MapLocationItem mapLocationItem);
    }

    public MapLocationsAdapter(Context context) {
        super(context, R.layout.row_map_location_item);

        mContext = context;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setOnItemSelectedListener(MapLocationsAdapter.OnItemSelectedListener listener) {
        mOnItemSelectedListener = listener;
    }

    /*
     * Set List Item Data
     *
     */
    public void setData(List<MapLocationItem> data) {
        clear();
        if (data != null) {
            addAll(data);
        }
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parentView) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = mLayoutInflater.inflate(R.layout.row_map_location_item, parentView, false);

            holder = new ViewHolder();
            holder.rootView = convertView;
            holder.tvName = (TextView) convertView.findViewById(R.id.tvName);
            holder.tvProfession = (TextView) convertView.findViewById(R.id.tvProfession);
            holder.tvOnlineStatus = (TextView) convertView.findViewById(R.id.tvOnlineStatus);
            holder.tvDistanceStatus = (TextView) convertView.findViewById(R.id.tvDistanceStatus);
            holder.ivProfilePhoto = (CircularImageView) convertView.findViewById(R.id.ivProfilePhoto);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final MapLocationItem mapLocationItem = getItem(position);
        if (!TextUtils.isEmpty(mapLocationItem.user.userProfile.avatarUrl)) {
            Glide.with(mContext).load(mapLocationItem.user.userProfile.avatarUrl)
                    .bitmapTransform(new CropCircleTransformation(mContext))
                    .into(holder.ivProfilePhoto);
        }

        holder.tvName.setText(mapLocationItem.user.userProfile.username);
        holder.tvProfession.setText(mapLocationItem.user.userProfile.profession);
        if (mapLocationItem.user.status.isOnline) {
            holder.tvOnlineStatus.setText(R.string.online_now);
            holder.tvOnlineStatus.setTextColor(getContext().getResources().getColor(R.color.mainColor));
        } else {
            holder.tvOnlineStatus.setText(DateUtils.getGapOfTimesWithString(mapLocationItem.user.status.timestamp, getContext()));
            holder.tvOnlineStatus.setTextColor(getContext().getResources().getColor(R.color.contentTextColor));
        }

        holder.tvDistanceStatus.setText(BaseUtils.getDistanceString((int)(mapLocationItem.distance), getContext()));

        return holder.rootView;
    }

    static class ViewHolder {
        View rootView;
        TextView tvName, tvProfession, tvOnlineStatus, tvDistanceStatus;
        CircularImageView ivProfilePhoto;
    }
}