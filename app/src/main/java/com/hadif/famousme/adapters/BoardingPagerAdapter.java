package com.hadif.famousme.adapters;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hadif.famousme.R;


public class BoardingPagerAdapter extends PagerAdapter{
    Context context;
    int mTitles[] = {R.string.network_beyond_digital, R.string.own_profile_title, R.string.use_this_way, R.string.set_privacy_settings };
    int mDetails[] = {R.string.beyond_digital_details_2, R.string.own_profile_details, R.string.own_profile_details, R.string.privacy_settings_details};
    LayoutInflater layoutInflater;

    boolean mHasAroundUsers = false;


    public BoardingPagerAdapter(Context context) {
        this.context = context;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return mTitles.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View itemView = layoutInflater.inflate(R.layout.page_boarding_item, container, false);

        TextView titleView = (TextView) itemView.findViewById(R.id.tvTitle);
        titleView.setText(mTitles[position]);

        TextView detailView = (TextView) itemView.findViewById(R.id.tvDetails);
        if (position == 0) {
            if (!mHasAroundUsers)
                detailView.setText(mDetails[position]);
            else
                detailView.setText(R.string.beyond_digital_details_1);
        } else
            detailView.setText(mDetails[position]);


        container.addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }

    public void setHasAroundUsersFlage(boolean flag) {
        mHasAroundUsers = flag;
    }
}