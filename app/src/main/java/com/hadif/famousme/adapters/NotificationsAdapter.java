package com.hadif.famousme.adapters;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.hadif.famousme.AppConfig;
import com.hadif.famousme.R;
import com.hadif.famousme.managers.basedata.ThemeManager;
import com.hadif.famousme.models.Conversation;
import com.hadif.famousme.models.Notification;
import com.hadif.famousme.utils.DateUtils;
import com.views.imageview.CircularImageView;

import java.util.List;

import jp.wasabeef.glide.transformations.CropCircleTransformation;

public class NotificationsAdapter<T> extends ArrayAdapter<Notification> {
    Context mContext;
    LayoutInflater mLayoutInflater;

    NotificationsAdapter.OnProfileLongPressListener mOnProfileLongPressListener;

    public interface OnProfileLongPressListener {
        void onProfileLongPressed(int position);
    }

    public NotificationsAdapter(Context context) {
        super(context, R.layout.row_notification_item);

        mContext = context;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setOnProfileLongPressListener(NotificationsAdapter.OnProfileLongPressListener listener) {
        mOnProfileLongPressListener = listener;
    }

    /*
     * Set List Item Data
     */
    public void setData(List<Notification> data) {
        clear();
        if (data != null) {
            addAll(data);
        }
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parentView) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = mLayoutInflater.inflate(R.layout.row_notification_item, null);

            holder = new ViewHolder();
            holder.rootView = convertView;
            holder.tvName = (TextView) convertView.findViewById(R.id.tvName);
            holder.tvProfession = (TextView) convertView.findViewById(R.id.tvProfession);
            holder.tvActiveTime = (TextView) convertView.findViewById(R.id.tvActiveTime);
            holder.tvDetails = (TextView) convertView.findViewById(R.id.tvDetails);
            holder.ivProfilePhoto = (CircularImageView) convertView.findViewById(R.id.ivProfilePhoto);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final Notification notification = getItem(position);

        holder.tvName.setText(notification.userProfile.username);
        holder.tvProfession.setText(notification.userProfile.profession);
        holder.tvActiveTime.setText(DateUtils.getGapOfTimesWithString(notification.timestamp, getContext()));

        if (notification.notificationType == AppConfig.Notification.NOTIFICATION_VIEWED_PROFILE)
            holder.tvDetails.setText(R.string.viewed_profile);
        else if (notification.notificationType == AppConfig.Notification.NOTIFICATION_IMPORTED_CONTACT)
            holder.tvDetails.setText(R.string.add_you_to_contact);

        holder.tvDetails.setTextColor(holder.tvDetails.getContext().getResources().getColor(ThemeManager.getInstance().getCurrentThemeColor()));

        String photoUri = notification.userProfile.avatarUrl;
        if (TextUtils.isEmpty(photoUri))
            photoUri = notification.userProfile.photoUrl;

        if (!TextUtils.isEmpty(photoUri)) {
            Glide.with(getContext()).load(photoUri)
                    .bitmapTransform(new CropCircleTransformation(getContext()))
                    .into(holder.ivProfilePhoto);
        }

        holder.ivProfilePhoto.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                mOnProfileLongPressListener.onProfileLongPressed(position);
                return false;
            }
        });

        return holder.rootView;
    }

    static class ViewHolder {
        View rootView;
        TextView tvName, tvProfession, tvActiveTime, tvDetails;
        CircularImageView ivProfilePhoto;
    }
}