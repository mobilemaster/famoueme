package com.hadif.famousme.adapters;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.hadif.famousme.R;
import com.hadif.famousme.models.Conversation;
import com.hadif.famousme.utils.DateUtils;
import com.views.imageview.CircularImageView;

import java.util.List;

import jp.wasabeef.glide.transformations.CropCircleTransformation;

public class ConversationsAdapter<T> extends ArrayAdapter<Conversation> {
    Context mContext;
    LayoutInflater mLayoutInflater;
    OnProfileLongPressListener mOnProfileLongPressListener;

    public interface OnProfileLongPressListener {
        void onProfileLongPressed(int position);
    }

    public ConversationsAdapter(Context context) {
        super(context, R.layout.row_conversion_item);

        mContext = context;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setOnProfileLongPressListener(ConversationsAdapter.OnProfileLongPressListener listener) {
        mOnProfileLongPressListener = listener;
    }

    /*
     * Set List Item Data
     *
     */
    public void setData(List<Conversation> data) {
        clear();
        if (data != null) {
            addAll(data);
        }
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parentView) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = mLayoutInflater.inflate(R.layout.row_conversion_item, parentView, false);

            holder = new ViewHolder();
            holder.rootView = convertView;
            holder.rowContainer = convertView.findViewById(R.id.rowContainer);
            holder.tvName = (TextView)convertView.findViewById(R.id.tvName);
            holder.tvProfession = (TextView)convertView.findViewById(R.id.tvProfession);
            holder.tvActiveTime = (TextView)convertView.findViewById(R.id.tvActiveTime);
            holder.tvLastMessage = (TextView)convertView.findViewById(R.id.tvLastMessage);
            holder.ivProfilePhoto = (CircularImageView)convertView.findViewById(R.id.ivProfilePhoto);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final Conversation conversation = getItem(position);
        if (!conversation.isRead && !conversation.lastMessage.contains("You:")) {
            holder.tvLastMessage.setTextColor(getContext().getResources().getColor(android.R.color.black));
            holder.rowContainer.setBackgroundColor(getContext().getResources().getColor(R.color.lightDarkBgColor));
            holder.tvName.setBackgroundColor(getContext().getResources().getColor(R.color.lightDarkBgColor));
        }
        else {
            holder.rowContainer.setBackgroundColor(getContext().getResources().getColor(android.R.color.white));
            holder.tvLastMessage.setTextColor(getContext().getResources().getColor(R.color.normalTextGrayColor));
            holder.tvName.setBackgroundColor(getContext().getResources().getColor(android.R.color.white));
        }

        String photoUri = conversation.userProfile.avatarUrl;
        if (TextUtils.isEmpty(photoUri))
            photoUri = conversation.userProfile.photoUrl;

        if (!TextUtils.isEmpty(photoUri)) {
            Glide.with(mContext).load(photoUri)
                    .bitmapTransform(new CropCircleTransformation(mContext))
                    .into(holder.ivProfilePhoto);
            holder.ivProfilePhoto.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    mOnProfileLongPressListener.onProfileLongPressed(position);
                    return false;
                }
            });
        }

        holder.tvName.setText(conversation.userProfile.username);
        holder.tvProfession.setText(conversation.userProfile.profession);
        holder.tvActiveTime.setText(DateUtils.getGapOfTimesWithString(conversation.timestamp, getContext()));
        if (!TextUtils.isEmpty(conversation.lastMessage)) {
            holder.tvLastMessage.setText(conversation.lastMessage);
        } else
            holder.tvLastMessage.setText(""); //For last image message

        return holder.rootView;
    }

    static class ViewHolder {
        View rootView;
        View rowContainer;
        TextView tvName, tvProfession, tvActiveTime, tvLastMessage;
        CircularImageView ivProfilePhoto;
    }
}