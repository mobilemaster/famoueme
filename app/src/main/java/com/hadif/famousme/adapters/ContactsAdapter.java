package com.hadif.famousme.adapters;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.hadif.famousme.R;
import com.hadif.famousme.models.Contact;
import com.views.imageview.CircularImageView;

import java.util.List;

import jp.wasabeef.glide.transformations.CropCircleTransformation;

public class ContactsAdapter<T> extends ArrayAdapter<Contact> {
    Context mContext;
    LayoutInflater mLayoutInflater;
    OnProfileLongPressListener mOnProfileLongPressListener;

    public interface OnProfileLongPressListener {
        void onProfileLongPressed(int position);
    }

    public interface OnItemSelectedListener {
        void onItemSelected(Contact contactItem);
    }

    public ContactsAdapter(Context context) {
        super(context, R.layout.row_contact);

        mContext = context;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setOnProfileLongPressListener(ContactsAdapter.OnProfileLongPressListener listener) {
        mOnProfileLongPressListener = listener;
    }

    /*
     * Set List Item Data
     *
     */
    public void setData(List<Contact> data) {
        clear();
        if (data != null) {
            addAll(data);
        }
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parentView) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = mLayoutInflater.inflate(R.layout.row_contact, parentView, false);

            holder = new ViewHolder();
            holder.rootView = convertView;
            holder.tvName = (TextView) convertView.findViewById(R.id.tvName);
            holder.tvProfession = (TextView) convertView.findViewById(R.id.tvProfession);
            holder.ivProfilePhoto = (CircularImageView) convertView.findViewById(R.id.ivProfilePhoto);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final Contact contact = getItem(position);
        if (!TextUtils.isEmpty(contact.userProfile.avatarUrl)) {
            Glide.with(getContext()).load(contact.userProfile.avatarUrl)
                    .bitmapTransform(new CropCircleTransformation(getContext()))
                    .into(holder.ivProfilePhoto);

            holder.ivProfilePhoto.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    mOnProfileLongPressListener.onProfileLongPressed(position);
                    return false;
                }
            });
        }

        holder.tvName.setText(contact.userProfile.username);
        holder.tvProfession.setText(contact.userProfile.profession);

        return holder.rootView;
    }

    static class ViewHolder {
        View rootView;
        TextView tvName, tvProfession;
        CircularImageView ivProfilePhoto;
    }
}