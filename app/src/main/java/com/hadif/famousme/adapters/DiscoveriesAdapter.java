package com.hadif.famousme.adapters;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.hadif.famousme.R;
import com.hadif.famousme.models.Discovery;
import com.hadif.famousme.utils.BaseUtils;
import com.hadif.famousme.utils.DateUtils;
import com.views.imageview.CircularImageView;

import java.util.List;

import jp.wasabeef.glide.transformations.CropCircleTransformation;

public class DiscoveriesAdapter<T> extends ArrayAdapter<Discovery> {
    Context mContext;
    LayoutInflater mLayoutInflater;
    DiscoveriesAdapter.OnProfileLongPressListener mOnProfileLongPressListener;

    public interface OnProfileLongPressListener {
        void onProfileLongPressed(int position);
    }

    public DiscoveriesAdapter(Context context) {
        super(context, R.layout.row_conversion_item);

        mContext = context;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setOnProfileLongPressListener(DiscoveriesAdapter.OnProfileLongPressListener listener) {
        mOnProfileLongPressListener = listener;
    }

    /*
     * Set List Item Data
     *
     */
    public void setData(List<Discovery> data) {
        clear();
        if (data != null) {
            addAll(data);
        }
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parentView) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = mLayoutInflater.inflate(R.layout.row_map_location_item, parentView, false);

            holder = new ViewHolder();
            holder.rootView = convertView;
            holder.tvName = (TextView) convertView.findViewById(R.id.tvName);
            holder.tvProfession = (TextView) convertView.findViewById(R.id.tvProfession);
            holder.tvOnlineStatus = (TextView) convertView.findViewById(R.id.tvOnlineStatus);
            holder.tvDistanceStatus = (TextView) convertView.findViewById(R.id.tvDistanceStatus);
            holder.ivProfilePhoto = (CircularImageView) convertView.findViewById(R.id.ivProfilePhoto);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final Discovery discovery = getItem(position);
        String photoUri = discovery.userProfile.avatarUrl;
        if (TextUtils.isEmpty(photoUri))
            photoUri = discovery.userProfile.photoUrl;

        if (!TextUtils.isEmpty(photoUri)) {
            Glide.with(mContext).load(photoUri)
                    .bitmapTransform(new CropCircleTransformation(mContext))
                    .into(holder.ivProfilePhoto);

            holder.ivProfilePhoto.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    mOnProfileLongPressListener.onProfileLongPressed(position);
                    return false;
                }
            });
        }

        holder.tvName.setText(discovery.userProfile.username);
        holder.tvProfession.setText(discovery.userProfile.profession);
        holder.tvProfession.setText(discovery.userProfile.profession);
        if (discovery.status.isOnline) {
            holder.tvOnlineStatus.setText(R.string.online_now);
            holder.tvOnlineStatus.setTextColor(getContext().getResources().getColor(R.color.mainColor));
        } else {
            holder.tvOnlineStatus.setText(DateUtils.getGapOfTimesWithString(discovery.status.timestamp, getContext()));
            holder.tvOnlineStatus.setTextColor(getContext().getResources().getColor(R.color.contentTextColor));
        }

        holder.tvDistanceStatus.setText(BaseUtils.getDistanceString((int)(discovery.distance), getContext()));

        return holder.rootView;
    }

    static class ViewHolder {
        View rootView;
        TextView tvName, tvProfession, tvOnlineStatus, tvDistanceStatus;
        CircularImageView ivProfilePhoto;
    }
}