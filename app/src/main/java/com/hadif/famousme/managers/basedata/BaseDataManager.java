package com.hadif.famousme.managers.basedata;

import android.content.Context;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;

import com.firebase.geofire.GeoQuery;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.hadif.famousme.R;
import com.hadif.famousme.activities.VerifyPhoneActivity;
import com.hadif.famousme.models.Achievement;
import com.hadif.famousme.models.Contact;
import com.hadif.famousme.models.Discovery;
import com.hadif.famousme.models.User;
import com.hadif.famousme.utils.DateUtils;
import com.hadif.famousme.utils.pickers.Country;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import static com.hadif.famousme.AppConfig.OnlineFilter.A_DAY;
import static com.hadif.famousme.AppConfig.OnlineFilter.A_MONTH;
import static com.hadif.famousme.AppConfig.OnlineFilter.A_WEEK;
import static com.hadif.famousme.AppConfig.OnlineFilter.NO_TIME_LIMIT;

public class BaseDataManager {
    private static final BaseDataManager ourInstance = new BaseDataManager();

    public synchronized static BaseDataManager getInstance() {
        return ourInstance;
    }

    private int mNewNotifications = 0;

    private ArrayList<String> mNationalityList = null;
    private ArrayList<String> mInterestList = null;
    private ArrayList<Discovery> mDiscoveryList = new ArrayList<>();
    private ArrayList<Contact> mContactList = new ArrayList<>();

    private User mUser = null;

    private int mConversationCount = 0;

    private HashMap<DatabaseReference, ValueEventListener> mValueEventHashMap = new HashMap<>();
    private HashMap<DatabaseReference, ChildEventListener> mChildEventHashMap = new HashMap<>();
    private GeoQuery mGeoQuery = null;

    public double serverTimeOffset = 0;

    private BaseDataManager() {

    }

    public void setNationalityList(ArrayList<String> nationalityList) {
        this.mNationalityList = nationalityList;
    }
    public ArrayList<String> getNationalityList() {
        return mNationalityList;
    }

    public ArrayList<String> getInterestList() {
        return mInterestList;
    }
    public void setInterestList(ArrayList<String> interestList) {
        this.mInterestList = interestList;
    }
    public void addInterest(String interest) {
        if (mInterestList == null)
            mInterestList = new ArrayList<>();

        mInterestList.add(interest);
    }

    public ArrayList<Discovery> getDiscoveryList() {
        return mDiscoveryList;
    }
    public void addDiscovery(Discovery discovery) {
        if (mDiscoveryList == null)
            mDiscoveryList = new ArrayList<>();

        mDiscoveryList.add(0, discovery);
    }

    public ArrayList<Contact> getContactList() {
        return mContactList;
    }
    public void addContact(Contact contact) {
        if (mContactList == null)
            mContactList = new ArrayList<>();

        mContactList.add(0, contact);
    }

    public void setUser(User user) {
        this.mUser = user;
    }
    public User getUser() {
        return mUser;
    }

    public void setNewNotifications(int newNotifications) {
        this.mNewNotifications = newNotifications;
    }

    public int getNewNotifications() {
        return mNewNotifications;
    }

    public void setConversationCount(int conversationCount) {
        this.mConversationCount = conversationCount;
    }

    public int getConversationCount() {
        return this.mConversationCount;
    }

    public HashMap<DatabaseReference, ValueEventListener> getValueEventHashMap() {
        return mValueEventHashMap;
    }

    public HashMap<DatabaseReference, ChildEventListener> getChildEventHashMap() {
        return mChildEventHashMap;
    }

    public void setGeoQuery(GeoQuery geoQuery) {
        mGeoQuery = geoQuery;
    }

    public GeoQuery geoQuery() {
        return mGeoQuery;
    }

    public void removeGeoQuery() {
        if (mGeoQuery != null)
            mGeoQuery.removeAllListeners();

        mGeoQuery = null;
    }

    public void removeAllBaseData() {
        removeAllEventListener();

        if (mNationalityList != null) {
            mNationalityList.clear();
            mNationalityList = null;
        }

        if (mInterestList != null) {
            mInterestList.clear();
            mInterestList = null;
        }

        if (mDiscoveryList != null)
            mDiscoveryList.clear();

        if (mContactList != null)
            mContactList.clear();

        if (mValueEventHashMap != null)
            mValueEventHashMap.clear();

        if (mChildEventHashMap != null)
            mChildEventHashMap.clear();

        mUser = null;
    }

    //[START - Static Functions]

    /**
     * Check if it has right user info or not
     * @return boolean
     */
    public static boolean hasUserInfo() {
        if (BaseDataManager.getInstance().getUser() != null)
            return true;

        return false;
    }

    /**
     * Check if it got a valid list of nationality.
     * @return boolean
     */
    public static boolean hasValidNationalityList() {
        ArrayList<String> nationalityList = BaseDataManager.getInstance().getNationalityList();
        if (nationalityList != null && nationalityList.size() > 0)
            return true;

        return false;
    }

    /**
     * Check if it got a valid list of interests.
     * @return
     */
    public static boolean hasValidInterestList() {
        ArrayList<String> interestList = BaseDataManager.getInstance().getInterestList();
        if (interestList != null && interestList.size() > 0)
            return true;

        return false;
    }

    /**
     * Now, User info has nationality as string type.
     * If the nationality values would be changed by admin, it can cause some problems.
     * At that time, -1 would be returned.
     * @param inNationality
     * @return
     */
    public static int getNationalityIndex(String inNationality) {
        if (!hasValidNationalityList())
            return -1;

        ArrayList<String> nationalityList = BaseDataManager.getInstance().getNationalityList();
        for (int i = 0; i < nationalityList.size(); i++ ) {
            String nationality = nationalityList.get(i);
            if (nationality.equals(inNationality))
                return i;
        }
        return -1;
    }

    /**
     * Now, User info has interests as string array.
     * If the interest value would be changed by admin, it can cause some problems.
     * At that time, -1 would be returned.
     * @param inInterest
     * @return int
     */
    public static int getInterestIndex(String inInterest) {
        if (!hasValidInterestList())
            return -1;

        ArrayList<String> interestsList = BaseDataManager.getInstance().getInterestList();
        for (int i = 0; i < interestsList.size(); i++ ) {
            String interest = interestsList.get(i);
            if (interest.equals(inInterest))
                return i;
        }

        return -1;
    }

    /**
     * Add 1 to new notifications
     */
    public static void addNewNotification() {
        int newNotifications = BaseDataManager.getInstance().getNewNotifications() + 1;
        BaseDataManager.getInstance().setNewNotifications(newNotifications);
    }

    /**
     * Check if has conversations or not
     * @return
     */
    public static boolean hasConversations() {
        return BaseDataManager.getInstance().getConversationCount() > 0? Boolean.TRUE: Boolean.FALSE;
    }

    /**
     * Put ValueEventListener for removing.
     * @param databaseReference
     * @param valueEventListener
     */
    public static void putValueEventListener(DatabaseReference databaseReference,
                                                ValueEventListener valueEventListener) {
        HashMap<DatabaseReference, ValueEventListener> hashMap = BaseDataManager.getInstance().getValueEventHashMap();
        hashMap.put(databaseReference, valueEventListener);
    }

    /**
     * Put ChildEventListener for removing.
     * @param databaseReference
     * @param childEventListener
     */
    public static void putChildEventListener(DatabaseReference databaseReference,
                                             ChildEventListener childEventListener) {
        HashMap<DatabaseReference, ChildEventListener> hashMap = BaseDataManager.getInstance().getChildEventHashMap();
        hashMap.put(databaseReference, childEventListener);
    }

    /**
     * Remove All EventListeners
     * Note, it would be called only once the MainActivity stop.
     */
    public static void removeAllEventListener() {
        HashMap<DatabaseReference, ValueEventListener> valueEventHashMap = BaseDataManager.getInstance().getValueEventHashMap();
        for (Map.Entry<DatabaseReference, ValueEventListener> entry : valueEventHashMap.entrySet()) {
            DatabaseReference databaseReference = entry.getKey();
            ValueEventListener valueEventListener = entry.getValue();
            databaseReference.removeEventListener(valueEventListener);
        }

        HashMap<DatabaseReference, ChildEventListener> ChildEventHashMap = BaseDataManager.getInstance().getChildEventHashMap();
        for (Map.Entry<DatabaseReference, ChildEventListener> entry : ChildEventHashMap.entrySet()) {
            DatabaseReference databaseReference = entry.getKey();
            ChildEventListener childEventListener = entry.getValue();
            databaseReference.removeEventListener(childEventListener);
        }

        GeoQuery geoQuery = BaseDataManager.getInstance().geoQuery();
        if (geoQuery != null)
            geoQuery.removeAllListeners();
    }

    /**
     * Check with user key if the discovery is duplicated or not
     * When the app is launched, get all discoveries at first.
     * and then when the user would be added by Geofire, it should be checked whether it has already added or not.
     * @param userKey
     * @return
     */
    public static boolean isDuplicatedDiscovery(String userKey) {
        ArrayList<Discovery> discoveries = BaseDataManager.getInstance().getDiscoveryList();
        for (Discovery discovery:discoveries) {
            if (discovery.userKey.equals(userKey))
                return true;
        }

        return false;
    }

    /**
     * Check with user key if the contact is duplicated or not.
     * @param userKey
     * @return
     */
    public static boolean isDuplicatedContact(String userKey) {
        ArrayList<Contact> contacts = BaseDataManager.getInstance().getContactList();
        for (Contact contact:contacts) {
            if (contact.userId.equals(userKey))
                return true;
        }

        return false;
    }

    /**
     * Check if the user is valid or not for discovering.     *
     * @param user a user who discovered
     * @param context current context.
     * @return boolean valid or not.
     */
    public static boolean checkValidUser(User user, Context context) {
        if (context == null)
            return false;

        if (user == null || user.userProfile == null || !user.userProfile.isValid)
            return false;

        User myProfile = BaseDataManager.getInstance().getUser();
        if (myProfile == null || myProfile.discoveryRules == null)
            return false;

        if (myProfile.discoveryRules.discoverGender != 0 &&
                (myProfile.discoveryRules.discoverGender - 1) != user.gender) {
            return false;
        }

        //Online Filter
        if (myProfile.discoveryRules.onlineFilter != NO_TIME_LIMIT) { //0 is No Limit
            int timeGap = DateUtils.getGapOfDate(user.status.timestamp);
            int onlineFilter = 0;
            switch (myProfile.discoveryRules.onlineFilter) {
                case A_DAY:
                    onlineFilter = 1;
                    break;
                case A_WEEK:
                    onlineFilter = 7;
                    break;
                case A_MONTH:
                    onlineFilter = 31;
                    break;
                default:
                    break;
            }

            if (timeGap > onlineFilter)
                return false;
        }

        //Service Filter
        if (myProfile.discoveryRules.serviceFilter != 0) { // 0 is No Service Filter.
            ArrayList<String> serviceList = BaseDataManager.getServicesList(context);
            String serviceFilter = serviceList.get(myProfile.discoveryRules.serviceFilter);

            if (TextUtils.isEmpty(user.serviceTitle))
                return false;

            if (!user.serviceTitle.equals(serviceFilter))
                return false;
        }

        //Age Range
        if (myProfile.discoveryRules.ageRange != 0) {
            int userAge = DateUtils.getAge(user.dateOfBirth);

            String[] rangeArray = context.getResources().getStringArray(R.array.age_range_array);
            String range = rangeArray[myProfile.discoveryRules.ageRange].replace(" ", "");
            int startRange = Integer.parseInt(range.split("~")[0]);
            int endRange  = 200;
            if (startRange < 60)
                endRange = Integer.parseInt(range.split("~")[1]);

            if (userAge < startRange || userAge > endRange)
                return false;
        }

        if (TextUtils.isEmpty(myProfile.discoveryRules.discoverNationality))
            return false;

        //Nationality
        if (!myProfile.discoveryRules.discoverNationality.equals(context.getString(R.string.all))) {
            if (!myProfile.discoveryRules.discoverNationality.equals(user.nationality))
                return false;
        }

        if (user.interests == null)
            return false;

        String[] userInterests = user.interests.split("/");
        //Interests
        if (!myProfile.discoveryRules.discoveryInterest.equals(context.getString(R.string.all))) {
            for (String userInterest: userInterests) {
                if (myProfile.discoveryRules.discoveryInterest.contains(userInterest)) {
                    return true;
                }
            }
        }

        return true;
    }

    /**
     * Get services list
     */
    public static ArrayList<String> getServicesList(Context context) {
        return new ArrayList<String>(Arrays.asList(context.getResources().getStringArray(R.array.services_list)));
    }


    /**
     * Get Country Name by Dial code.
     * @param dialCode phone dial country code. e.g +971
     * @param context Context.
     * @return Country Name.
     */
    public static String getCountryNameByCode(String dialCode, Context context) {
        if (dialCode.equals("+1"))
            return "Canada, United States";

        try {
            String allCountriesCode = BaseDataManager.readEncodedJsonString(context);

            JSONArray countryArray = new JSONArray(allCountriesCode);

            for (int i = 0; i < countryArray.length(); i++) {
                JSONObject jsonObject = countryArray.getJSONObject(i);
                String countryName = jsonObject.getString("name");
                String countryDialCode = jsonObject.getString("dial_code");
                //String countryCode = jsonObject.getString("code");
                if (dialCode.equals(countryDialCode)) {
                    return countryName;
                }
            }

        }catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public static String readEncodedJsonString(Context context)
            throws java.io.IOException {
        String base64 = context.getResources().getString(R.string.countries_code);
        byte[] data = Base64.decode(base64, Base64.DEFAULT);
        return new String(data, "UTF-8");
    }

    //[END - Static Functions]
}
