package com.hadif.famousme.managers.basedata;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hadif.famousme.AppConfig;
import com.hadif.famousme.R;
import com.hadif.famousme.utils.PrefsUtils;

public class ThemeManager {

    public final static int SalmonColor = 0;
    public final static int  MediumGreenColor = 1;
    public final static int  BlueColor = 2;
    public final static int  HotPinkColor = 3;
    public final static int  DarkIndigoColor = 4;
    public final static int  DarkSalmonColor = 5;
    public final static int  BlueVioletColor = 6;
    public final static int  SlatGrayColor = 7;
    public final static int  RoyalBlueColor = 8;

    private final static String FIELD_THEME_TYPE = "theme_type";

    private static Context mContext;
    public static void setContext(Context context) {
        mContext = context;
    }

    private static final ThemeManager instance = new ThemeManager();
    public synchronized static ThemeManager getInstance() {
        return instance;
    }

    public void setThemeType(int themeType) {
        SharedPreferences.Editor editor = PrefsUtils.getPrefs().edit();
        editor.putInt(FIELD_THEME_TYPE, themeType);
        editor.apply();
    }

    public int getThemeType() {
        SharedPreferences sharedPreferences = PrefsUtils.getPrefs();
        return sharedPreferences.getInt(FIELD_THEME_TYPE, 0);
    }

    public int getThemeColor(int themeType) {
        switch (themeType) {
            case SalmonColor:
                return R.color.salmon_color;
            case MediumGreenColor:
                return R.color.medium_green_color;
            case HotPinkColor:
                return R.color.hot_pink_color;
            case BlueColor:
                return R.color.blue_color;
            case DarkIndigoColor:
                return R.color.dark_indigo_color;
            case DarkSalmonColor:
                return R.color.dark_salmon_color;
            case BlueVioletColor:
                return R.color.blue_violet_color;
            case SlatGrayColor:
                return R.color.slat_gray_color;
            case RoyalBlueColor:
                return R.color.royal_blue_color;

            default:
                break;
        }

        return R.color.medium_green_color;
    }

    public int getCurrentThemeColor() {
        return getThemeColor(getThemeType());
    }

    public int getThemeStyle(int themeType) {
        switch (themeType) {
            case SalmonColor:
                return R.style.AppSalmonTheme;
            case MediumGreenColor:
                return R.style.AppMediumGreenTheme;
            case HotPinkColor:
                return R.style.AppHotPinkTheme;
            case BlueColor:
                return R.style.AppBlueTheme;
            case DarkIndigoColor:
                return R.style.AppDarkIndigoTheme;
            case DarkSalmonColor:
                return R.style.AppDarkSalmonTheme;
            case BlueVioletColor:
                return R.style.AppBlueVioletTheme;
            case SlatGrayColor:
                return R.style.AppSlatGrayTheme;
            case RoyalBlueColor:
                return R.style.AppRoyalBlueTheme;
            default:
                break;
        }

        return R.style.AppMediumGreenTheme;
    }

    public int getCurrentThemeStyle() {
        return getThemeStyle(getThemeType());
    }

    public void setThemeWithChildViews(ViewGroup viewGroup) {
        if (mContext == null)
            return;

        viewGroup.setBackgroundColor(mContext.getResources().getColor(getCurrentThemeColor()));
        for(int index = 0; index < viewGroup.getChildCount(); ++index) {
            View nextChild = viewGroup.getChildAt(index);
            nextChild.setBackgroundColor(mContext.getResources().getColor(getCurrentThemeColor()));
        }
    }

    public void setTheme(View view) {
        if (mContext == null)
            return;

        view.setBackgroundColor(mContext.getResources().getColor(getCurrentThemeColor()));
    }

    public void setMainColorButton(TextView button) {
        Drawable drawable = button.getContext().getResources().getDrawable(R.drawable.round_maincolor_button);
        GradientDrawable gradientDrawable = (GradientDrawable) drawable;
        gradientDrawable.setColor(ContextCompat.getColor(button.getContext(), ThemeManager.getInstance().getCurrentThemeColor()));
        gradientDrawable.setStroke(0, ThemeManager.getInstance().getCurrentThemeColor());
        gradientDrawable.setCornerRadius(dpToPx(8));

        button.setBackground(drawable);
        button.setPadding(dpToPx(20), dpToPx(15), dpToPx(20), dpToPx(15));
    }

    public static int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    public static int getConversationIcon() {
        int themeType = ThemeManager.getInstance().getThemeType();
        switch (themeType) {
            case ThemeManager.SalmonColor:
                return R.drawable.chats_active_salmon;
            case ThemeManager.MediumGreenColor:
                return R.drawable.chats_active;
            case ThemeManager.HotPinkColor:
                return R.drawable.chats_active_pink;
            case ThemeManager.BlueColor:
                return R.drawable.chats_active_blue;
            case ThemeManager.DarkIndigoColor:
                return R.drawable.chats_active_darkindigo;
            case ThemeManager.DarkSalmonColor:
                return R.drawable.chats_active_darksalmon;
            case ThemeManager.BlueVioletColor:
                return R.drawable.chats_active_blueviolet;
            case ThemeManager.SlatGrayColor:
                return R.drawable.chats_active_slatgray;
            case ThemeManager.RoyalBlueColor:
                return R.drawable.chats_active_royalblue;

            default:
                break;
        }

        return R.drawable.chats_active_salmon;
    }

    public static int getMapIcon() {
        int themeType = ThemeManager.getInstance().getThemeType();
        switch (themeType) {
            case ThemeManager.SalmonColor:
                return R.drawable.map_active_salmon;
            case ThemeManager.MediumGreenColor:
                return R.drawable.map_active;
            case ThemeManager.HotPinkColor:
                return R.drawable.map_active_pink;
            case ThemeManager.BlueColor:
                return R.drawable.map_active_blue;
            case ThemeManager.DarkIndigoColor:
                return R.drawable.map_active_darkindigo;
            case ThemeManager.DarkSalmonColor:
                return R.drawable.map_active_darksalmon;
            case ThemeManager.BlueVioletColor:
                return R.drawable.map_active_blueviolet;
            case ThemeManager.SlatGrayColor:
                return R.drawable.map_active_slatgray;
            case ThemeManager.RoyalBlueColor:
                return R.drawable.map_active_royalblue;
            default:
                break;
        }

        return R.drawable.map_active_salmon;
    }

    public static int getNotificationIcon() {
        int themeType = ThemeManager.getInstance().getThemeType();
        switch (themeType) {
            case ThemeManager.SalmonColor:
                return R.drawable.notifications_active_salmon;
            case ThemeManager.MediumGreenColor:
                return R.drawable.notifications_active;
            case ThemeManager.HotPinkColor:
                return R.drawable.notifications_active_pink;
            case ThemeManager.BlueColor:
                return R.drawable.notifications_active_blue;
            case ThemeManager.DarkIndigoColor:
                return R.drawable.notifications_active_darkindigo;
            case ThemeManager.DarkSalmonColor:
                return R.drawable.notifications_active_darksalmon;
            case ThemeManager.BlueVioletColor:
                return R.drawable.notifications_active_blueviolet;
            case ThemeManager.SlatGrayColor:
                return R.drawable.notifications_active_slatgray;
            case ThemeManager.RoyalBlueColor:
                return R.drawable.notifications_active_royalblue;
            default:
                break;
        }

        return R.drawable.notifications_active_salmon;
    }

    public static int getSettingsIcon() {
        int themeType = ThemeManager.getInstance().getThemeType();
        switch (themeType) {
            case ThemeManager.SalmonColor:
                return R.drawable.settings_active_salmon;
            case ThemeManager.MediumGreenColor:
                return R.drawable.settings_active;
            case ThemeManager.HotPinkColor:
                return R.drawable.settings_active_pink;
            case ThemeManager.BlueColor:
                return R.drawable.settings_active_blue;
            case ThemeManager.DarkIndigoColor:
                return R.drawable.settings_active_darkindigo;
            case ThemeManager.DarkSalmonColor:
                return R.drawable.settings_active_darksalmon;
            case ThemeManager.BlueVioletColor:
                return R.drawable.settings_active_blueviolet;
            case ThemeManager.SlatGrayColor:
                return R.drawable.settings_active_slatgray;
            case ThemeManager.RoyalBlueColor:
                return R.drawable.settings_active_royalblue;
            default:
                break;
        }

        return R.drawable.settings_active_salmon;
    }

    public static int getLoadingIcon() {
        int themeType = ThemeManager.getInstance().getThemeType();
        switch (themeType) {
            case ThemeManager.SalmonColor:
                return R.drawable.map_loading_salmon;
            case ThemeManager.MediumGreenColor:
                return R.drawable.map_loading;
            case ThemeManager.HotPinkColor:
                return R.drawable.map_loading_pink;
            case ThemeManager.BlueColor:
                return R.drawable.map_loading_blue;
            case ThemeManager.DarkIndigoColor:
                return R.drawable.map_loading_darkindigo;
            case ThemeManager.DarkSalmonColor:
                return R.drawable.map_loading_darksalmon;
            case ThemeManager.BlueVioletColor:
                return R.drawable.map_loading_blueviolet;
            case ThemeManager.SlatGrayColor:
                return R.drawable.map_loading_slatgray;
            case ThemeManager.RoyalBlueColor:
                return R.drawable.map_loading_royalblue;
            default:
                break;
        }

        return R.drawable.map_loading_salmon;
    }


}
