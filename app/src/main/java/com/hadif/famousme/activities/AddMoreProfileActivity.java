package com.hadif.famousme.activities;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.firebase.database.FirebaseDatabase;
import com.hadif.famousme.R;
import com.hadif.famousme.managers.basedata.BaseDataManager;
import com.hadif.famousme.managers.basedata.ThemeManager;
import com.hadif.famousme.models.User;
import com.hadif.famousme.utils.FirebaseUtils;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddMoreProfileActivity extends BaseActivity {

    public static void start (Context context) {
        Intent intent = new Intent(context, AddMoreProfileActivity.class);
        context.startActivity(intent);
    }

    //UI control
    @BindView(R.id.rlHeader)        RelativeLayout rlHeader;
    @BindView(R.id.etWebSite)       EditText etWebSite;
    @BindView(R.id.etFacebook)      EditText etFacebook;
    @BindView(R.id.etSnapchat)      EditText etSnapchat;
    @BindView(R.id.etLinkein)       EditText etLinkein;
    @BindView(R.id.etTwitter)       EditText etTwitter;
    @BindView(R.id.etInstagram)     EditText etInstagram;
    @BindView(R.id.etYoutube)       EditText etYoutube;
    @BindView(R.id.btnSave)         TextView btnSave;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(ThemeManager.getInstance().getCurrentThemeStyle());
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_more_profile);

        ButterKnife.bind(this);

        applyTheme();

        setUI();
    }

    private void applyTheme() {
        ThemeManager.getInstance().setThemeWithChildViews(rlHeader);
        ThemeManager.getInstance().setTheme(btnSave);
    }

    /**
     * Set UI
     */
    private void setUI() {
        User user = BaseDataManager.getInstance().getUser();
        if (user == null)
            return;

        if (user.social == null)
            return;

        if (!TextUtils.isEmpty(user.social.website))
            etWebSite.setText(user.social.website);

        if (!TextUtils.isEmpty(user.social.facebook))
            etFacebook.setText(user.social.facebook);

        if (!TextUtils.isEmpty(user.social.snapchat))
            etSnapchat.setText(user.social.snapchat);

        if (!TextUtils.isEmpty(user.social.linkedin))
            etLinkein.setText(user.social.linkedin);

        if (!TextUtils.isEmpty(user.social.twitter))
            etTwitter.setText(user.social.twitter);

        if (!TextUtils.isEmpty(user.social.instagram))
            etInstagram.setText(user.social.instagram);

        if (!TextUtils.isEmpty(user.social.youtube))
            etYoutube.setText(user.social.youtube);
    }

    /**
     * Save data to server.
     */
    private void submitSocialData() {
        User user = BaseDataManager.getInstance().getUser();
        if (user == null)
            return;

        user.social.website = etWebSite.getText().toString();
        user.social.facebook = etFacebook.getText().toString();
        user.social.snapchat = etSnapchat.getText().toString();
        user.social.linkedin = etLinkein.getText().toString();
        user.social.twitter = etTwitter.getText().toString();
        user.social.instagram = etInstagram.getText().toString();
        user.social.youtube = etYoutube.getText().toString();

        Map<String, Object> updateData = new HashMap<>();
        updateData.put("website", etWebSite.getText().toString());
        updateData.put("facebook", etFacebook.getText().toString());
        updateData.put("snapchat", etSnapchat.getText().toString());
        updateData.put("linkedin", etLinkein.getText().toString());
        updateData.put("twitter", etTwitter.getText().toString());
        updateData.put("instagram", etInstagram.getText().toString());
        updateData.put("youtube", etYoutube.getText().toString());

        FirebaseDatabase.getInstance().getReference().child("users")
                .child(user.key).child("social").updateChildren(updateData);

        Map<String, Object> upgradeData = new HashMap<>();
        upgradeData.put("isAddedSocial", Boolean.TRUE);
        user.isAddedSocial = Boolean.TRUE;

        FirebaseDatabase.getInstance().getReference().child("users")
                .child(user.key).updateChildren(upgradeData);
    }

    @OnClick(R.id.btnBack)
    public void onBack() {
        finish();
    }

    @OnClick(R.id.btnSave)
    public void onSave() {
        submitSocialData();
        finish();
    }
}
