package com.hadif.famousme.activities;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.hadif.famousme.R;
import com.twitter.sdk.android.tweetcomposer.TweetComposer;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class InviteFriendsActivity extends AppCompatActivity {

    public static void start (Context context) {
        Intent intent = new Intent(context, InviteFriendsActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invite_friends);

        ButterKnife.bind(this);
    }

    private void inviteByEmail() {

    }

    private void inviteBySMS() {

    }

    private void inviteFacebookFriends() {

    }

    private void inviteTwitterContacts() {
        Uri imageUri = Uri.parse("android.resource://com.hadif.famousme/" + R.mipmap.ic_launcher);
        TweetComposer.Builder builder = new TweetComposer.Builder(this)
                .text(getString(R.string.twitter_message))
                .image(imageUri);

        builder.show();
    }

    private void postShareLinked() {

    }

    @OnClick(R.id.btnBack)
    public void onBack(){
        onBackPressed();
    }

    @OnClick(R.id.btnInviteEmail)
    public void onInviteByEmail() {
        inviteByEmail();
    }

    @OnClick(R.id.btnInviteSMS)
    public void onInviteBySMS() {
        inviteBySMS();
    }

    @OnClick(R.id.btnInviteFacebook)
    public void onInviteFacebookFriends() {
        inviteFacebookFriends();
    }

    @OnClick(R.id.btnInviteTwitter)
    public void onInviteTwitterContacts() {
        inviteTwitterContacts();
    }

    @OnClick(R.id.btnPostShareLinkedin)
    public void onPostShareLinkedin() {
        postShareLinked();
    }
}
