package com.hadif.famousme.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.content.FileProvider;
import android.support.v7.view.menu.MenuBuilder;
import android.support.v7.view.menu.MenuPopupHelper;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.signature.StringSignature;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.hadif.famousme.AppConfig;
import com.hadif.famousme.R;
import com.hadif.famousme.adapters.MessagesAdapter;
import com.hadif.famousme.managers.basedata.BaseDataManager;
import com.hadif.famousme.managers.basedata.ThemeManager;
import com.hadif.famousme.models.BlockUser;
import com.hadif.famousme.models.Discovery;
import com.hadif.famousme.models.Message;
import com.hadif.famousme.models.Status;
import com.hadif.famousme.models.User;
import com.hadif.famousme.utils.BaseUtils;
import com.hadif.famousme.utils.DateUtils;
import com.hadif.famousme.utils.FirebaseUtils;
import com.hadif.famousme.utils.ImageUtils;
import com.hadif.famousme.utils.StringFormatter;
import com.hadif.famousme.utils.Utility;
import com.views.popuplist.PopupList;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import jp.wasabeef.glide.transformations.CropCircleTransformation;

public class ChatActivity extends BaseActivity {

    public static void start(Context context) {
        Intent intent = new Intent(context, ChatActivity.class);
        context.startActivity(intent);
    }

    public static void start(Context context, String username, String userid, boolean isValid) {
        Intent intent = new Intent(context, ChatActivity.class);
        intent.putExtra("username", username);
        intent.putExtra("isValid", isValid);
        intent.putExtra("userId", userid);

        context.startActivity(intent);
    }

    public static final int VIEW_TYPE_SENT_MESSAGE = 0;
    public static final int VIEW_TYPE_RECEIVED_MESSAGE = 1;
    private static final String TAG = "ChatActivity";

    //UI Controls
    @BindView(R.id.rlHeader)                RelativeLayout rlHeader;
    @BindView(R.id.llUserProfileLayout)     View llUserProfileLayout;
    @BindView(R.id.btnPlus)                 ImageView btnPlus;
    @BindView(R.id.tvName)                  TextView tvName;
    @BindView(R.id.tvStatus)                TextView tvStatus;
    @BindView(R.id.etEnterMessage)          EditText etEnterMessage;
    @BindView(R.id.btnSend)                 TextView btnSend;
    @BindView(R.id.lvChatView)              ListView lvChatView;
    @BindView(R.id.llInputLayout)           View llInputLayout;

    //Private Members
    private View mRootView;
    private String mUserId;
    private boolean mIsValid;
    private BlockUser mBlockUser = null;
    private ArrayList<Message> mMessageList = new ArrayList<>();
    private MessagesAdapter mMessagesAdapter;
    //private LinearLayoutManager mLinearLayoutManager;

    private DatabaseReference mUserStatusRef;
    private ValueEventListener mUserStatusRefListener;

    private DatabaseReference mMessagesRef;
    private ChildEventListener mMessageEventListener;

    private DatabaseReference mConversationRef;
    private ValueEventListener mConversationListener;

    private List<String> mMessageEditPopupMenuList = new ArrayList<>();

    private User mCurrentUser;
    private boolean mShouldCheckUpdate = false;

    private String mOrgUsername = null;
    private String mOrgPhotoUrl = null;
    private String mOrgProfession = null;
    private boolean mIsUpdated = false;
    private int mStartFrom = AppConfig.ChartFrom.FROM_DISCOVERY;

    //Photo
    private int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    private String mUserChoosenTask;
    private Uri mPhotoUri = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(ThemeManager.getInstance().getCurrentThemeStyle());
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        ButterKnife.bind(this);

        applyTheme();
        Intent intent = getIntent();
        mUserId = intent.getStringExtra("userId");
        mIsValid = intent.getBooleanExtra("isValid", true);
        mShouldCheckUpdate = getIntent().getBooleanExtra("shouldCheckUpdate", false);
        if (mShouldCheckUpdate) {
            mOrgUsername = getIntent().getStringExtra("username");
            mOrgPhotoUrl = getIntent().getStringExtra("photoUrl");
            mOrgProfession = getIntent().getStringExtra("profession");
        }
        mStartFrom = intent.getIntExtra("startFrom", AppConfig.ChartFrom.FROM_DISCOVERY);

        mMessagesAdapter = new MessagesAdapter(ChatActivity.this);
        lvChatView.setAdapter(mMessagesAdapter);
        mMessagesAdapter.setData(mMessageList);
        lvChatView.setClickable(true);

        //mMessageEditPopupMenuList.add(getString(R.string.nudge));
        mMessageEditPopupMenuList.add(getString(R.string.edit));
        mMessageEditPopupMenuList.add(getString(R.string.delete));
        final PopupList popupList = new PopupList(this);
        popupList.bind(lvChatView, mMessageEditPopupMenuList, new PopupList.PopupListListener() {
            @Override
            public boolean showPopupList(View adapterView, View contextView, int contextPosition) {
                Message message = mMessageList.get(contextPosition);
                if (!message.isRead) {
                    mMessageEditPopupMenuList.clear();
                    mMessageEditPopupMenuList.add(getString(R.string.nudge));
                    mMessageEditPopupMenuList.add(getString(R.string.edit));
                    mMessageEditPopupMenuList.add(getString(R.string.delete));
                }
                else {
                    mMessageEditPopupMenuList.clear();
                    mMessageEditPopupMenuList.add(getString(R.string.edit));
                    mMessageEditPopupMenuList.add(getString(R.string.delete));
                }

                popupList.resetMenuItems(mMessageEditPopupMenuList);

                if (message.senderId.equals(FirebaseUtils.getUserId()))
                    return true;
                else
                    return false;
            }

            @Override
            public void onPopupListClick(View contextView, int contextPosition, int position) {
                Message message = mMessageList.get(contextPosition);
                if (!message.isRead) {
                    if (position == 0)
                        nudgeMessage(contextPosition);
                    else if (position == 1)
                        editMessage(contextPosition);
                    else if (position == 2)
                        deleteMessage(contextPosition);
                } else {
                    if (position == 0)
                        editMessage(contextPosition);
                    else if (position == 1)
                        deleteMessage(contextPosition);
                }
            }
        });

        etEnterMessage.addTextChangedListener(onTextChangeListener());

        //For resizing chat view when the keyboard would be shown.
        mRootView = findViewById(R.id.rootView);
        mRootView.getViewTreeObserver().addOnGlobalLayoutListener(onGlobalLayoutListener());

        showProgressDialog();
        setProgressDialogMessage("Getting messages!");
        getUserInfo(mUserId);
        getUserStatus();
        getMessages();

        getTimeOffset();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void onBackPressed() {
        finishActivity();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //Remove Listeners
        if (mUserStatusRef != null && mUserStatusRefListener != null) {
            mUserStatusRef.removeEventListener(mUserStatusRefListener);
            mUserStatusRef = null;
            mUserStatusRefListener = null;
        }

        if (mMessagesRef != null && mMessageEventListener != null) {
            mMessagesRef.removeEventListener(mMessageEventListener);
            mMessagesRef = null;
            mMessageEventListener = null;
        }

        if (mConversationRef != null && mConversationListener != null) {
            mConversationRef.removeEventListener(mConversationListener);
            mConversationRef = null;
            mConversationListener = null;
        }
    }

    private void applyTheme() {
        ThemeManager.getInstance().setThemeWithChildViews(rlHeader);
    }

    private void setUI(){
        if (activityDestroyed)
            return;

        tvName.setText(mCurrentUser.userProfile.username);

        if (!mIsValid) {
            showAlertDialogMessage(R.string.notice_title, R.string.cannot_chat_delete_account);
            llInputLayout.setVisibility(View.GONE);
        }
    }

    private void getTimeOffset() {
        DatabaseReference offsetRef = FirebaseDatabase.getInstance().getReference(".info/serverTimeOffset");
        offsetRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                double offset = snapshot.getValue(Double.class);
                BaseDataManager.getInstance().serverTimeOffset = offset;

                Log.e("--Offset--", String.valueOf(offset));
            }

            @Override
            public void onCancelled(DatabaseError error) {
                System.err.println("Listener was cancelled");
            }
        });
    }

    /**
     * Get full user info with key
     * @param userId
     */
    private void getUserInfo(String userId) {
        FirebaseDatabase.getInstance().getReference().child("users").child(userId)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot snapshot) {
                        mCurrentUser = snapshot.getValue(User.class);
                        mCurrentUser.key = snapshot.getKey();
                        if (mCurrentUser != null) {
                            setUI();

                            if (mCurrentUser.userProfile.isValid)
                                monitoringBlockUser();

                            if (isUpdatedProfile()) {
                                mIsUpdated = true;
                                Toast.makeText(getApplicationContext(), R.string.profile_updated, Toast.LENGTH_LONG).show();
                            }
                        }

                        hideProgressDialog();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Toast.makeText(getApplicationContext(), "Can't get user data!", Toast.LENGTH_LONG).show();
                    }
                });
    }

    /**
     * Check whether the user's profile was updated or not.
     * @return result.
     */
    private boolean isUpdatedProfile() {
        if (!TextUtils.isEmpty(mOrgUsername) &&
                !mOrgUsername.equals(mCurrentUser.userProfile.username))
            return true;

        if (!TextUtils.isEmpty(mOrgPhotoUrl) &&
                !mOrgPhotoUrl.equals(mCurrentUser.userProfile.photoUrl))
            return true;

        if (!TextUtils.isEmpty(mOrgProfession) &&
                !mOrgProfession.equals(mCurrentUser.userProfile.profession))
            return true;

        return false;
    }

    /**
     * Get user status, online timestamp
     */
    private void getUserStatus() {
        mUserStatusRef = FirebaseDatabase.getInstance().getReference()
                .child("users").child(mUserId).child("status");
        mUserStatusRefListener = mUserStatusRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                Status status = snapshot.getValue(Status.class);
                if (status != null) {
                    if (status.isOnline) {
                        tvStatus.setText(R.string.online_now);
                    } else {
                        String onlineStatus = getString(R.string.online) + " " +
                                DateUtils.getGapOfTimesWithString(status.timestamp, ChatActivity.this);
                        tvStatus.setText(onlineStatus);
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }

    /**
     * Get messages
     */
    private void getMessages() {
        DatabaseReference databaseRef = FirebaseDatabase.getInstance().getReference();
        final String conversationId = FirebaseUtils.getConversationId(mUserId);

        mMessagesRef = databaseRef.child("messages").child(conversationId);
        mMessageEventListener = mMessagesRef.limitToLast(AppConfig.READ_MESSAGES_COUNT).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                if (dataSnapshot.getValue() != null) {
                    Message message = dataSnapshot.getValue(Message.class);
                    message.messageId = dataSnapshot.getKey();
                    if (TextUtils.isEmpty(message.senderId))
                        return;

                    mMessageList.add(message);

                    if (!message.isRead &&
                            !message.senderId.equals(FirebaseUtils.getUserId())) {
                        //Update unread
                        Map<String, Object> conversation = new HashMap<>();
                        conversation.put("isRead", true);
                        FirebaseUtils.getDatabaseReference().child("conversations")
                                .child(FirebaseUtils.getUserId()).child(conversationId).updateChildren(conversation);

                        Map<String, Object> updateMap = new HashMap<>();
                        updateMap.put("readTimestamp", ServerValue.TIMESTAMP);
                        updateMap.put("isRead", Boolean.TRUE);

                        FirebaseUtils.getDatabaseReference().child("messages")
                                .child(conversationId).child(dataSnapshot.getKey()).updateChildren(updateMap);

                    }

                    reloadMessageList();
                    lvChatView.setSelection(mMessageList.size() - 1);
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                if (dataSnapshot.getValue() != null) {
                    Message message = dataSnapshot.getValue(Message.class);
                    for (Message item: mMessageList) {
                        if (item.messageId.equals(dataSnapshot.getKey())) {
                            item.setData(message);
                            item.messageId = dataSnapshot.getKey();
                            break;
                        }
                    }

                    reloadMessageList();
                }
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue() != null) {
                    for (int i = 0; i < mMessageList.size(); i++) {
                        Message message = mMessageList.get(i);
                        if (message.messageId.equals(dataSnapshot.getKey()))
                            mMessageList.remove(i);
                    }

                    reloadMessageList();
                }
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }

    class SendPushAsyncTask extends AsyncTask<Void,Void,Void> {
        private String mMessage;
        public SendPushAsyncTask(String message) {
            mMessage = message;
        }

        @Override
        protected Void doInBackground(Void... params) {
            User me = BaseDataManager.getInstance().getUser();
            if (me == null || me.userProfile == null)
                return null;

            try {
                URL url = new URL(AppConfig.FCM_SERVER_URL); //Enter URL here
                HttpURLConnection httpURLConnection = (HttpURLConnection)url.openConnection();
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                httpURLConnection.setInstanceFollowRedirects(false);
                httpURLConnection.setRequestMethod("POST"); // here you are telling that it is a POST request, which can be changed into "PUT", "GET", "DELETE" etc.
                httpURLConnection.setRequestProperty("Content-Type", "application/json;charset=UTF-8"); // here you are setting the `Content-Type` for the data you are sending which is `application/json`
                httpURLConnection.setRequestProperty("Authorization", AppConfig.FCM_SERVER_KEY);
                httpURLConnection.connect();

                JSONObject jsonObject = new JSONObject();
                jsonObject.put("to", mCurrentUser.token);

                JSONObject dataObject = new JSONObject();

                String title = me.userProfile.username + " " + getString(R.string.just_send_a_message);
                dataObject.put("title", URLEncoder.encode(title, "UTF-8"));
                dataObject.put("message", URLEncoder.encode(mMessage, "UTF-8"));

                jsonObject.put("data", dataObject);

                JSONObject notificationObject = new JSONObject();
                //For iOS devices. Please note convertStringToUTF8
                notificationObject.put("title", StringFormatter.convertStringToUTF8(title));
                notificationObject.put("body", StringFormatter.convertStringToUTF8(mMessage));

                notificationObject.put("content_available", true);

                jsonObject.put("notification", notificationObject);

                DataOutputStream wr = new DataOutputStream(httpURLConnection.getOutputStream());
                wr.writeBytes(jsonObject.toString());
                wr.flush();

                //  Here you read any answer from server.
                BufferedReader serverAnswer = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));
                String line;
                while ((line = serverAnswer.readLine()) != null) {
                    System.out.println("LINE: " + line); //<--If any response from server
                    //use it as you need, if server send something back you will get it here.
                }

                serverAnswer.close();

                wr.close();

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }
    }

    /**
     * Send nudge message push notification.
     * @param position  index of message.
     */
    private void nudgeMessage(int position) {
        User me = BaseDataManager.getInstance().getUser();

        Message message = mMessageList.get(position);
        String title = me.userProfile.username + " " + getString(R.string.just_send_a_message);
        Map<String, Object> nudgeMessage = new HashMap<>();
        nudgeMessage.put("photoUrl", me.userProfile.avatarUrl);
        nudgeMessage.put("senderId", me.key);
        nudgeMessage.put("title", title);
        nudgeMessage.put("message", message.text);

        FirebaseDatabase.getInstance().getReference()
                .child("nudge").child(mUserId).push().updateChildren(nudgeMessage);

        SendPushAsyncTask asyncTask = new SendPushAsyncTask(message.text);
        asyncTask.execute();

        String conversationId = FirebaseUtils.getConversationId(mUserId);
        Map<String, Object> messageMap = new HashMap<>();
        messageMap.put("isNotified", true);
        FirebaseUtils.getDatabaseReference().child("messages").child(conversationId)
                                .child(message.messageId).updateChildren(messageMap);
    }

    /**
     * Delete Message
     * @param position
     */
    private void deleteMessage(int position) {
        if (position < 0 || position >= mMessageList.size()) {
            Toast.makeText(getApplicationContext(), "Wrong message index " + String.valueOf(position), Toast.LENGTH_LONG).show();
            return;
        }

        //After deleting a message, update last messages!
        if (position == mMessageList.size() - 1) {
            String lastMessage = "";
            boolean isYours = true;
            if (mMessageList.size() > 2) {
                Message message = mMessageList.get(mMessageList.size() - 2);
                lastMessage = message.text;
                if (!message.senderId.equals(FirebaseUtils.getUserId()))
                    isYours = false;
            }

            updateConversations(lastMessage, isYours);
            updateDiscoveries(lastMessage, isYours);
        }

        Message message = mMessageList.get(position);
        mMessagesRef.child(message.messageId).removeValue();
        //mMessageList.remove(position);

        reloadMessageList();
    }

    /**
     * Edit Message
     * @param position
     */
    private void editMessage(final int position) {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.edit_message_title));

        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.layout_alert_confirm_delete_code, null);
        builder.setView(dialogView);
        final Message message = mMessageList.get(position);
        final EditText etEditMessage = (EditText)dialogView.findViewById(R.id.etDeleteCode);
        etEditMessage.setText(message.text);

        builder.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        builder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                String editMessage = etEditMessage.getText().toString();

                Map<String, Object> updateMessage = new HashMap<>();
                updateMessage.put("text", editMessage);

                mMessagesRef.child(message.messageId).updateChildren(updateMessage);
                message.text = editMessage;
                reloadMessageList();
                if (position == mMessageList.size() - 1) {
                    updateConversations(editMessage, true);
                    updateDiscoveries(editMessage, true);
                }
            }
        });

        builder.create().show();
    }

    /**
     * Upload image data
     */
    private void uploadImageData() {
        if (mPhotoUri == null)
            return;

//        showProgressDialog();
//        setProgressDialogMessage("Saving...");
//        String conversationId = FirebaseUtils.getConversationId(mUserId);

        String photoName = FirebaseUtils.getUserId() + String.valueOf(System.currentTimeMillis()) + ".jpg";
        final StorageReference photoRef = FirebaseStorage.getInstance().getReference().child("messages").child(photoName);

        Bitmap imageData = null;
        try {
            imageData = ImageUtils.decodeUri(mPhotoUri, true, AppConfig.IMAGE_REQUIRED_SIZE, this);
        } catch (FileNotFoundException e) {
            hideProgressDialog();
            e.printStackTrace();
        }

        if (imageData == null) {
            hideProgressDialog();
            Toast.makeText(getApplicationContext(), "Can't get image.", Toast.LENGTH_LONG).show();
            return;
        }

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        imageData.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        imageData.recycle();
        byte[] byteData = baos.toByteArray();

        UploadTask uploadTask = photoRef.putBytes(byteData);
        uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
            @Override
            public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                if (!task.isSuccessful()) {
                    throw task.getException();
                }

                // Continue with the task to get the download URL
                return photoRef.getDownloadUrl();
            }
        }).addOnCompleteListener(new OnCompleteListener<Uri>() {
            @Override
            public void onComplete(@NonNull Task<Uri> task) {
                if (task.isSuccessful()) {
                    Uri downloadUri = task.getResult();

                    sendMessage(Message.MessageType.IMAGE, downloadUri.toString());
                } else {
                    // Handle failures
                }
            }
        });
    }

    /**
     * Send a message
     */
    private void sendMessage(int messageType, String fileUrl) {
        if (mCurrentUser == null)
            return;

        String myId = FirebaseUtils.getUserId();
        if (TextUtils.isEmpty(myId))
            return;

        String conversationId = FirebaseUtils.getConversationId(mUserId);

        Map<String, Object> message = new HashMap<>();
        message.put("senderId", FirebaseUtils.getUserId());
        message.put("receiverId", mUserId);
        double estimatedServerTimeMs = System.currentTimeMillis() + BaseDataManager.getInstance().serverTimeOffset;
        //message.put("sentTimestamp", ServerValue.TIMESTAMP);
        message.put("sentTimestamp", estimatedServerTimeMs);
        message.put("text", etEnterMessage.getText().toString());
        message.put("isRead", Boolean.FALSE);
        message.put("messageType", messageType);
        if (messageType == Message.MessageType.IMAGE)
            message.put("imageUrl", fileUrl);

        if (mMessageList.size() > 0) {
            Message lastMessage = mMessageList.get(mMessageList.size() - 1);
            int gap = DateUtils.getGapOfDate(lastMessage.sentTimestamp);
            if (gap > 0)
                message.put("isFirstOfDay", Boolean.TRUE);
            else
                message.put("isFirstOfDay", Boolean.FALSE);
        } else
            message.put("isFirstOfDay", Boolean.FALSE);

        FirebaseUtils.getDatabaseReference().child("messages").child(conversationId).push().setValue(message);

        if (mCurrentUser.discoveryRules.pushNotification) {
            SendPushAsyncTask asyncTask = new SendPushAsyncTask(etEnterMessage.getText().toString());
            asyncTask.execute();
        }

        //Update Conversation data.
        updateConversations(etEnterMessage.getText().toString(), true);

        //Update Discoveries data.
        updateDiscoveries(etEnterMessage.getText().toString(), true);

        etEnterMessage.setText("");

        if (!BaseDataManager.hasConversations()) {
            FirebaseUtils.updateAchievements(AppConfig.Achievement.ACHIEVEMENT_START_CONVERSATION, ChatActivity.this);
        }
    }

    /**
     * Reload Message List
     */
    private void reloadMessageList() {
        mMessagesAdapter.setData(mMessageList);
        mMessagesAdapter.notifyDataSetChanged();
    }

    /**
     * Update Conversation data.
     * @param lastMessage
     */
    private void updateConversations(String lastMessage, boolean isYours) {
        User myProfile = BaseDataManager.getInstance().getUser();
        if (myProfile == null)
            return;

        //Update Conversation data.
        String conversationId = FirebaseUtils.getConversationId(mUserId);

        Map<String, Object> myConversation = new HashMap<>();
        myConversation.put("userId", mUserId);
        myConversation.put("timestamp", ServerValue.TIMESTAMP);
        if (isYours && !TextUtils.isEmpty(lastMessage))
            myConversation.put("lastMessage", "You:" + lastMessage);
        else
            myConversation.put("lastMessage", lastMessage);

        myConversation.put("isRead", true);

        FirebaseUtils.getDatabaseReference().child("conversations")
                .child(myProfile.key).child(conversationId).updateChildren(myConversation);

        Map<String, Object> opponentConversation = new HashMap<>();
        opponentConversation.put("userId", myProfile.key);
        opponentConversation.put("timestamp", ServerValue.TIMESTAMP);
        if (!isYours && !TextUtils.isEmpty(lastMessage)) {
            opponentConversation.put("lastMessage", "You:" + lastMessage);
        }
        else
            opponentConversation.put("lastMessage", lastMessage);

        opponentConversation.put("isRead", false);

        FirebaseUtils.getDatabaseReference().child("conversations")
                .child(mUserId).child(conversationId).updateChildren(opponentConversation);
    }

    /**
     * Update discoveries after sending a message.
     * @param lastMessage
     */
    private void updateDiscoveries(String lastMessage, boolean isYours) {
        User myProfile = BaseDataManager.getInstance().getUser();

        Map<String, Object> mine = new HashMap<>();
        if (isYours && !TextUtils.isEmpty(lastMessage))
            mine.put("lastMessage", "You:" + lastMessage);
        else
            mine.put("lastMessage", lastMessage);

        FirebaseUtils.getDatabaseReference().child("discoveries")
                .child(myProfile.key).child(mUserId).updateChildren(mine);

        Map<String, Object> opponent = new HashMap<>();
        opponent.put("lastMessage", lastMessage);
        if (!isYours && !TextUtils.isEmpty(lastMessage)) {
            opponent.put("lastMessage", "You:" + lastMessage);
        }
        else
            opponent.put("lastMessage", lastMessage);

        FirebaseUtils.getDatabaseReference().child("discoveries")
                .child(mUserId).child(myProfile.key).updateChildren(opponent);
    }

    /**
     * Check if the conversation is blocked or not.
     */
    private void monitoringBlockUser() {
        User myProfile = BaseDataManager.getInstance().getUser();
        if (myProfile == null)
            return;

        String conversationId = FirebaseUtils.getConversationId(mUserId);
        DatabaseReference databaseRef = FirebaseUtils.getDatabaseReference();
        mConversationRef = databaseRef.child("conversations").child(myProfile.key).child(conversationId).child("blockUser");
        mConversationListener = mConversationRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                if (snapshot != null) {
                    BlockUser blockUser = snapshot.getValue(BlockUser.class);
                    if (blockUser == null) {
                        mBlockUser = null;
                        llInputLayout.setVisibility(View.VISIBLE);
                    } else  {
                        if (TextUtils.isEmpty(blockUser.Ids)){
                            mBlockUser = null;
                            llInputLayout.setVisibility(View.VISIBLE);
                        } else {
                            mBlockUser = blockUser;
                            llInputLayout.setVisibility(View.GONE);
                        }
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Toast.makeText(getApplicationContext(), "Can't get data!", Toast.LENGTH_LONG).show();
            }
        });
    }

    /**
     * Block user.
     */
    private void doBlockUser() {
        if (mMessageList == null || mMessageList.size() == 0) {
            AlertDialog alertDialog = new AlertDialog.Builder(ChatActivity.this).create();
            alertDialog.setTitle(R.string.note);
            alertDialog.setMessage(getResources().getString(R.string.not_allowed_blockuser));
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            alertDialog.show();
            return;
        }

        User myProfile = BaseDataManager.getInstance().getUser();
        String conversationId = FirebaseUtils.getConversationId(mUserId);

        String blockIds = "";
        if (mBlockUser != null && !TextUtils.isEmpty(mBlockUser.Ids)) {
             blockIds = mBlockUser.Ids + ":" + myProfile.key;
        } else
            blockIds = myProfile.key;

        Map<String, Object> hashMap = new HashMap<>();
        hashMap.put("Ids", blockIds);

        FirebaseUtils.getDatabaseReference().child("conversations")
                .child(myProfile.key).child(conversationId).child("blockUser").updateChildren(hashMap);

        FirebaseUtils.getDatabaseReference().child("conversations")
                .child(mUserId).child(conversationId).child("blockUser").updateChildren(hashMap);

        BaseUtils.hideSoftKeyBoard(ChatActivity.this, etEnterMessage);
    }

    /**
     * Unblock user
     */
    private void doUnBlockUser() {
        User myProfile = BaseDataManager.getInstance().getUser();
        String conversationId = FirebaseUtils.getConversationId(mUserId);

        String blockIds = "";
        if (mBlockUser != null && !TextUtils.isEmpty(mBlockUser.Ids)) {
            if (mBlockUser.Ids.contains(":")) {
                String[] ids = mBlockUser.Ids.split(":");
                if (ids[0].equals(myProfile.key))
                    blockIds = ids[1];
                else
                    blockIds = ids[0];
            }
        } else
            return;

        if (TextUtils.isEmpty(blockIds)) {
            FirebaseUtils.getDatabaseReference().child("conversations")
                    .child(myProfile.key).child(conversationId).child("blockUser").removeValue();

            FirebaseUtils.getDatabaseReference().child("conversations")
                    .child(mUserId).child(conversationId).child("blockUser").removeValue();

        } else {
            Map<String, Object> hashMap = new HashMap<>();
            hashMap.put("Ids", blockIds);

            FirebaseUtils.getDatabaseReference().child("conversations")
                    .child(myProfile.key).child(conversationId).child("blockUser").updateChildren(hashMap);

            FirebaseUtils.getDatabaseReference().child("conversations")
                    .child(mUserId).child(conversationId).child("blockUser").updateChildren(hashMap);

        }
    }

    /**
     * Add user to contacts
     */
    private void addUserToContacts() {
        if (!BaseDataManager.isDuplicatedContact(mUserId)) {
            Map<String, Object> userContact = new HashMap<>();
            userContact.put("userId", mUserId);

            FirebaseUtils.getDatabaseReference().child("contacts")
                    .child(FirebaseUtils.getUserId()).push().updateChildren(userContact);

            FirebaseUtils.sendNotification(mUserId, AppConfig.Notification.NOTIFICATION_IMPORTED_CONTACT);

        } else {
            Toast.makeText(ChatActivity.this, R.string.duplicated_contact_message, Toast.LENGTH_SHORT).show();
        }
    }

    /////  Send Photo //////
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);
            else if (requestCode == REQUEST_CAMERA)
                onCaptureImageResult(data);
        }
    }

    /**
     * Get the result from camera
     * @param data intent data
     */
    private void onCaptureImageResult(Intent data) {
        if (mPhotoUri == null) {
            Toast.makeText(getApplicationContext(), R.string.failed_get_photo, Toast.LENGTH_LONG).show();
            return;
        }
        uploadImageData();
    }

    /**
     * Get the result from gallery
     * @param data intent data
     */
    private void onSelectFromGalleryResult(Intent data) {
        if (data != null) {
            Uri selectedImage = data.getData();
            mPhotoUri = data.getData();
            uploadImageData();
        }
    }


    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
    }

    private void cameraIntent() {
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (cameraIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = ImageUtils.createImageFile(ChatActivity.this);
            } catch (IOException ex) {
                Log.i(TAG, "IOException");
            }

            //For Android SDk 24+, 7.0
            if (photoFile != null) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    mPhotoUri = FileProvider.getUriForFile(this, getPackageName() + ".provider", photoFile);

                    List<ResolveInfo> resInfoList = getPackageManager().queryIntentActivities(cameraIntent, PackageManager.MATCH_DEFAULT_ONLY);
                    for (ResolveInfo resolveInfo : resInfoList) {
                        String packageName = resolveInfo.activityInfo.packageName;
                        grantUriPermission(packageName, mPhotoUri, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    }
                } else {
                    mPhotoUri = Uri.fromFile(photoFile);
                }

                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, mPhotoUri);
                startActivityForResult(cameraIntent, REQUEST_CAMERA);
            }
        }
    }

    /**
     * Get TextWatcher for input edit text.
     * @return TextWatcher
     */
    private TextWatcher onTextChangeListener() {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                if (etEnterMessage.getText().equals(""))
                    btnSend.setTextColor(getResources().getColor(ThemeManager.getInstance().getCurrentThemeColor()));
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(editable.length() == 0)
                    btnSend.setTextColor(getResources().getColor(R.color.normalTextGrayColor));
                else
                    btnSend.setTextColor(getResources().getColor(ThemeManager.getInstance().getCurrentThemeColor()));
            }
        };
    }

    /**
     * For resizing chat view when the keyboard would be shown.
     * @return ViewTreeObserver.OnGlobalLayoutListener
     */
    private ViewTreeObserver.OnGlobalLayoutListener onGlobalLayoutListener() {
        return new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                Rect r = new Rect();
                mRootView.getWindowVisibleDisplayFrame(r);
                int screenHeight = mRootView.getRootView().getHeight();
                // r.bottom is the position above soft keypad or device button.
                // if keypad is shown, the r.bottom is smaller than that before.
                int keypadHeight = screenHeight - r.bottom;
                if (keypadHeight > screenHeight * 0.15) { // 0.15 ratio is perhaps enough to determine keypad height.
                    // keyboard is opened
                    //mLinearLayoutManager.scrollToPosition(mMessageList.size() - 1);
                    lvChatView.smoothScrollToPosition(mMessageList.size() - 1);
                }
                else {
                    // keyboard is closed

                }
            }
        };
    }

    /**
     * Finish this activity.
     */
    private void finishActivity() {
        if (mShouldCheckUpdate) {
            Intent returnIntent = new Intent();
            returnIntent.putExtra("isUpdated", mIsUpdated);

            if (mIsUpdated) {
                returnIntent.putExtra("userKey", mCurrentUser.key);
                returnIntent.putExtra("photoUrl", mCurrentUser.userProfile.photoUrl);
                returnIntent.putExtra("profession", mCurrentUser.userProfile.profession);
                returnIntent.putExtra("username", mCurrentUser.userProfile.username);
            }

            setResult(Activity.RESULT_OK, returnIntent);
        }

        finish();
    }


    @OnClick(R.id.llUserProfileLayout)
    public void onUserProfileClicked() {
        UserProfileActivity.start(ChatActivity.this, true, /*tvStatus.getText().toString()*/"", mUserId);
    }


    @OnClick(R.id.btnBack)
    public void onBack() {
        finishActivity();
    }

    @OnClick(R.id.btnPlus)
    public void onPlus() {
        User myProfile = BaseDataManager.getInstance().getUser();

        MenuBuilder menuBuilder = new MenuBuilder(this);
        MenuInflater inflater = new MenuInflater(this);
        if (mBlockUser != null && mBlockUser.Ids.contains(myProfile.key))
            inflater.inflate(R.menu.chat_menu_unblock, menuBuilder);
        else
            inflater.inflate(R.menu.chat_menu, menuBuilder);

        MenuPopupHelper optionsMenu = new MenuPopupHelper(this, menuBuilder, btnPlus);
        optionsMenu.setForceShowIcon(true);

        // Set Item Click Listener
        menuBuilder.setCallback(new MenuBuilder.Callback() {
            @Override
            public boolean onMenuItemSelected(MenuBuilder menu, MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.go_to_map:
                        MainActivity.start(ChatActivity.this, AppConfig.RUNNING_GO_TO_MAP);
                        finish();
                        return true;
                    case R.id.add_contact:
                        addUserToContacts();
                        return true;
                    case R.id.block_user:
                        doBlockUser();
                        return true;
                    case R.id.unblock_user:
                        doUnBlockUser();
                        return true;
                    default:
                        return false;
                }
            }

            @Override
            public void onMenuModeChange(MenuBuilder menu) {}
        });

        // Display the menu
        optionsMenu.show();
    }

    @OnClick(R.id.btnSend)
    public void onSendMessage() {
        if (TextUtils.isEmpty(etEnterMessage.getText().toString()))
            return;

        sendMessage(Message.MessageType.TEXT, null);
    }

    @OnClick(R.id.famousme)
    public void onSendPhoto() {
        final CharSequence[] items = {getString(R.string.take_photo), getString(R.string.choose_from_libaray), getString(R.string.cancel)};
        AlertDialog.Builder builder = new AlertDialog.Builder(ChatActivity.this);
        builder.setTitle(getString(R.string.add_photo_title));
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals(getString(R.string.cancel))) {
                    dialog.dismiss();
                    return;
                }

                boolean result = Utility.checkPermission(ChatActivity.this);
                if (items[item].equals(getString(R.string.take_photo))) {
                    mUserChoosenTask = "Take Photo";
                    if (result)
                        cameraIntent();
                } else if (items[item].equals(getString(R.string.choose_from_libaray))) {
                    mUserChoosenTask = "Choose from Library";
                    if (result)
                        galleryIntent();
                }
            }
        });

        builder.show();
    }
}
