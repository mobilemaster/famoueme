package com.hadif.famousme.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.hadif.famousme.AppConfig;
import com.hadif.famousme.R;
import com.hadif.famousme.managers.basedata.BaseDataManager;
import com.hadif.famousme.managers.basedata.ThemeManager;
import com.hadif.famousme.models.Achievement;
import com.hadif.famousme.models.User;
import com.hadif.famousme.utils.DateUtils;
import com.hadif.famousme.utils.FirebaseUtils;
import com.views.imageview.CircularImageView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnLongClick;
import jp.wasabeef.glide.transformations.CropCircleTransformation;

public class UserProfileActivity extends BaseActivity {

    public static void start(Context context, boolean isFromChat, String status, String userId) {
        Intent intent = new Intent(context, UserProfileActivity.class);
        intent.putExtra("isFromChat", isFromChat);
        intent.putExtra("shouldCheckUpdate", false);
        intent.putExtra("status", status);
        intent.putExtra("userId", userId);
        context.startActivity(intent);
    }

    public static void start(Context context) {
        Intent intent = new Intent(context, UserProfileActivity.class);
        intent.putExtra("isFromChat", true);
        intent.putExtra("shouldCheckUpdate", false);
        context.startActivity(intent);
    }

    //UI Control
    @BindView(R.id.rlHeader)                RelativeLayout rlHeader;
    @BindView(R.id.tvTitle)                 TextView tvTitle;
    @BindView(R.id.ivProfilePhoto)          CircularImageView ivProfilePhoto;
    @BindView(R.id.tvAchievePercent)        TextView tvAchievePercent;
    @BindView(R.id.tvProfessionTitle)       TextView tvProfessionTitle;
    @BindView(R.id.tvDistanceStatus)        TextView tvDistanceStatus;
    @BindView(R.id.tvServiceTitle)          TextView tvServiceTitle;
    @BindView(R.id.tvServiceDetails)        TextView tvServiceDetails;
    @BindView(R.id.tvOnlineStatus)          TextView tvOnlineStatus;
    @BindView(R.id.tvProfessionValue)       TextView tvProfessionValue;
    @BindView(R.id.tvPhoneNumberValue)      TextView tvPhoneNumberValue;
    @BindView(R.id.tvEmailValue)            TextView tvEmailValue;
    @BindView(R.id.tvBirthValue)            TextView tvBirthValue;
    @BindView(R.id.tvNationalityValue)      TextView tvNationalityValue;
    @BindView(R.id.tvInterestValue)         TextView tvInterestValue;
    @BindView(R.id.btnMessage)              ViewGroup btnMessage;

    @BindView(R.id.llSocialLayout)          View llSocialLayout;
    @BindView(R.id.btnSnapChat)             View btnSnapChat;
    @BindView(R.id.btnFacebook)             View btnFacebook;
    @BindView(R.id.btnLinkedIn)             View btnLinkedIn;
    @BindView(R.id.btnTwitter)              View btnTwitter;

    //Private Members
    private boolean mIsFromChat;
    private User mCurrentUser;
    private boolean mShouldCheckUpdate;

    private String mOrgUsername = null;
    private String mOrgPhotoUrl = null;
    private String mOrgAvatarUrl = null;
    private String mOrgProfession = null;
    private boolean mIsUpdated = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(ThemeManager.getInstance().getCurrentThemeStyle());
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);

        ButterKnife.bind(this);

        applyTheme();

        mIsFromChat = getIntent().getBooleanExtra("isFromChat", true);
        String status = getIntent().getStringExtra("status");
        tvDistanceStatus.setText(status);

        mShouldCheckUpdate = getIntent().getBooleanExtra("shouldCheckUpdate", false);
        if (mShouldCheckUpdate) {
            mOrgUsername = getIntent().getStringExtra("username");
            mOrgPhotoUrl = getIntent().getStringExtra("photoUrl");
            mOrgAvatarUrl = getIntent().getStringExtra("avatarUrl");
            mOrgProfession = getIntent().getStringExtra("profession");
        }

        getUserInfo(getIntent().getStringExtra("userId"));
    }

    @Override
    public void onBackPressed() {
        finishActivity();
    }

    private void applyTheme() {
        ThemeManager.getInstance().setThemeWithChildViews(rlHeader);
        ThemeManager.getInstance().setThemeWithChildViews(btnMessage);
        //btnServiceEdit.setTextColor(getResources().getColor(ThemeManager.getInstance().getCurrentThemeColor()));
        //btnUpgradeAction.setTextColor(getResources().getColor(ThemeManager.getInstance().getCurrentThemeColor()));
    }

    /**
     * Set UI with current user info
     */
    private void setUI() {
        if (activityDestroyed)
            return;

        if (mCurrentUser == null || mCurrentUser.userProfile.photoUrl == null) {
            Toast.makeText(getApplicationContext(), R.string.msg_network_error, Toast.LENGTH_SHORT).show();
            return;
        }

        Glide.with(this).load(mCurrentUser.userProfile.photoUrl)
                .bitmapTransform(new CropCircleTransformation(this))
                .into(ivProfilePhoto);

        tvTitle.setText(mCurrentUser.userProfile.username);
        tvProfessionTitle.setText(mCurrentUser.userProfile.profession);
        tvProfessionValue.setText(mCurrentUser.userProfile.profession);
        tvPhoneNumberValue.setText(mCurrentUser.phoneNumber);
        tvEmailValue.setText(mCurrentUser.emailAddress);
        if (mCurrentUser.discoveryRules.canSeeMyAge)
            tvBirthValue.setText(mCurrentUser.dateOfBirth);
        else
            tvBirthValue.setText("");

        tvNationalityValue.setText(mCurrentUser.nationality);
        if (mCurrentUser.discoveryRules.showPhoneNumber)
            tvPhoneNumberValue.setText(mCurrentUser.phoneNumber);
        else
            tvPhoneNumberValue.setText("");

        if (mCurrentUser.status.isOnline) {
            tvOnlineStatus.setText(R.string.online_now);
            tvOnlineStatus.setTextColor(getResources().getColor(R.color.mainColor));
        } else {
            String onlineStatus = getString(R.string.online) + " " +
                    DateUtils.getGapOfTimesWithString(mCurrentUser.status.timestamp, UserProfileActivity.this);
            tvOnlineStatus.setText(onlineStatus);
            tvOnlineStatus.setTextColor(getResources().getColor(R.color.contentTextColor));
        }

        if (TextUtils.isEmpty(mCurrentUser.serviceTitle)) {
            tvServiceTitle.setText(getString(R.string.no_added_service));
            tvServiceDetails.setText("");
        } else {
            tvServiceTitle.setText(mCurrentUser.serviceTitle);
            tvServiceDetails.setText(mCurrentUser.serviceDetails);
        }

        String interestString = mCurrentUser.interests.replace("/", "\n");
        tvInterestValue.setText(interestString);

        if (mCurrentUser.isAddedSocial)
            llSocialLayout.setVisibility(View.VISIBLE);
        else
            llSocialLayout.setVisibility(View.GONE);
    }

    /**
     * Get full user info with key
     * @param userId
     */
    private void getUserInfo(String userId) {
        showProgressDialog();
        setProgressDialogMessage("Getting user's info...");
        FirebaseDatabase.getInstance().getReference().child("users").child(userId)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot snapshot) {
                        mCurrentUser = snapshot.getValue(User.class);
                        mCurrentUser.key = snapshot.getKey();
                        hideProgressDialog();
                        if (mCurrentUser != null) {
                            getAchievements();
                            setUI();

                            if (isUpdatedProfile()) {
                                mIsUpdated = true;
                                Toast.makeText(getApplicationContext(), R.string.profile_updated, Toast.LENGTH_LONG).show();
                            }
                        } else {
                            Toast.makeText(getApplicationContext(), "Can't get user data!", Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Toast.makeText(getApplicationContext(), "Can't get user data!", Toast.LENGTH_LONG).show();
                    }
                });
    }

    /**
     * Get Achievements
     */
    private void getAchievements() {
        FirebaseDatabase.getInstance().getReference().child("achievements").child(mCurrentUser.key)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot snapshot) {
                        if (snapshot != null) {
                            float achievementPercent = 0;
                            for (DataSnapshot dataSnapshot: snapshot.getChildren()) {
                                Achievement achievement = dataSnapshot.getValue(Achievement.class);
                                achievementPercent = achievementPercent + achievement.value;
                            }

                            tvAchievePercent.setText(String.valueOf(achievementPercent) + "%");
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Toast.makeText(getApplicationContext(), "Can't get data!", Toast.LENGTH_LONG).show();
                    }
                });
    }


    /**
     * Check whether the user's profile was updated or not.
     * @return result.
     */
    private boolean isUpdatedProfile() {
        if (!TextUtils.isEmpty(mOrgUsername) &&
                !mOrgUsername.equals(mCurrentUser.userProfile.username))
            return true;

        if (!TextUtils.isEmpty(mOrgAvatarUrl) &&
                !mOrgAvatarUrl.equals(mCurrentUser.userProfile.avatarUrl))
            return true;

        if (!TextUtils.isEmpty(mOrgPhotoUrl) &&
                !mOrgPhotoUrl.equals(mCurrentUser.userProfile.photoUrl))
            return true;

        if (!TextUtils.isEmpty(mOrgProfession) &&
                !mOrgProfession.equals(mCurrentUser.userProfile.profession))
            return true;

        return false;
    }

    /**
     * Finish this activity.
     */
    private void finishActivity() {
        if (mShouldCheckUpdate) {
            Intent returnIntent = new Intent();
            returnIntent.putExtra("isUpdated", mIsUpdated);

            if (mIsUpdated) {
                returnIntent.putExtra("userKey", mCurrentUser.key);
                returnIntent.putExtra("photoUrl", mCurrentUser.userProfile.photoUrl);
                returnIntent.putExtra("avatarUrl", mCurrentUser.userProfile.avatarUrl);
                returnIntent.putExtra("profession", mCurrentUser.userProfile.profession);
                returnIntent.putExtra("username", mCurrentUser.userProfile.username);
            }

            setResult(Activity.RESULT_OK, returnIntent);
        }

        finish();
    }

    @OnClick(R.id.btnBack)
    public void onBack() {
        finishActivity();
    }

    @OnLongClick(R.id.ivProfilePhoto)
    public boolean onViewPhotoFullScreen() {
        if (mCurrentUser == null || mCurrentUser.userProfile.photoUrl == null)
            return false;

        ImageViewerActivity.start(UserProfileActivity.this, mCurrentUser.userProfile.photoUrl);
        return true;
    }

    @OnClick(R.id.btnMessage)
    public void onMessageClicked() {
        if (mCurrentUser == null || mCurrentUser.userProfile.photoUrl == null)
            return;

        if (mIsFromChat)
            finish();
        else {
            ChatActivity.start(UserProfileActivity.this,
                                mCurrentUser.userProfile.username,
                                mCurrentUser.key,
                                mCurrentUser.userProfile.isValid);
        }
    }

    @OnClick(R.id.btnSnapChat)
    public void onSnapChatClicked() {

    }

    @OnClick(R.id.btnFacebook)
    public void onFacebookClicked() {

    }

    @OnClick(R.id.btnTwitter)
    public void onTwitterClicked() {

    }
}
