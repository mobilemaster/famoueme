package com.hadif.famousme.activities;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Path;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.firebase.geofire.GeoFire;
import com.firebase.geofire.GeoLocation;
import com.firebase.geofire.GeoQuery;
import com.firebase.geofire.GeoQueryEventListener;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.hadif.famousme.AppConfig;
import com.hadif.famousme.BuildConfig;
import com.hadif.famousme.R;
import com.hadif.famousme.adapters.BoardingPagerAdapter;
import com.hadif.famousme.managers.basedata.BaseDataManager;
import com.hadif.famousme.models.MapLocationItem;
import com.hadif.famousme.models.User;
import com.hadif.famousme.utils.FirebaseUtils;
import com.hadif.famousme.utils.permissions.PermissionsUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import jp.wasabeef.glide.transformations.CropCircleTransformation;

public class GettingStartedActivity extends BaseActivity {

    public static void start (Context context) {
        Intent intent = new Intent(context, GettingStartedActivity.class);
        context.startActivity(intent);
    }

    @BindView(R.id.vgOnboard)               View vgOnboard;
    @BindView(R.id.vgEnableLocation)        View vgEnableLocation;

    @BindView(R.id.vpBoardPager)            ViewPager vpBoardPager;
    @BindView(R.id.vgProfilesOfNearBy)      View vgProfilesOfNearBy;
    @BindView(R.id.vgNumberOfNearBy)        View vgNumberOfNearBy;
    @BindView(R.id.vgNearGroup)             View vgNearGroup;
    @BindView(R.id.vgLocationGroup)         View vgLocationGroup;
    @BindView(R.id.tvNumberOfUsers)         TextView tvNumberOfUsers;
    @BindView(R.id.tvLocation)              TextView tvLocation;
    @BindView(R.id.ivProfile1)              ImageView ivProfile1;
    @BindView(R.id.ivProfile2)              ImageView ivProfile2;
    @BindView(R.id.ivProfile3)              ImageView ivProfile3;
    @BindView(R.id.ivProfile4)              ImageView ivProfile4;
    @BindView(R.id.ivProfile5)              ImageView ivProfile5;
    @BindView(R.id.ivProfile6)              ImageView ivProfile6;
    @BindView(R.id.ivProfile7)              ImageView ivProfile7;
    @BindView(R.id.ivProfile8)              ImageView ivProfile8;

    //Private Members
    private static final String TAG = "GettingStartedActivity";
    BoardingPagerAdapter mBoardingPagerAdapter;
    ArrayList<ObjectAnimator> mAnimatorList = new ArrayList<>();
    ArrayList<User> mNearUserList = new ArrayList<>();
    ArrayList<ImageView> mImageViewArrayList = new ArrayList<>();
    boolean mIsStoppedAnimation = true;

    private FusedLocationProviderClient mFusedLocationClient;
    private SettingsClient mSettingsClient;
    private LocationRequest mLocationRequest;
    private LocationSettingsRequest mLocationSettingsRequest;
    private LocationCallback mLocationCallback;
    private Location mCurrentLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_getting_started);

        ButterKnife.bind(this);
        mBoardingPagerAdapter = new BoardingPagerAdapter(GettingStartedActivity.this);
        vpBoardPager.setAdapter(mBoardingPagerAdapter);
        vpBoardPager.setOffscreenPageLimit(4);
        vpBoardPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                setCurrentBoardingPage(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tbPageDots);
        tabLayout.setupWithViewPager(vpBoardPager, true);

        mImageViewArrayList.add(ivProfile1);
        mImageViewArrayList.add(ivProfile2);
        mImageViewArrayList.add(ivProfile3);
        mImageViewArrayList.add(ivProfile4);
        mImageViewArrayList.add(ivProfile5);
        mImageViewArrayList.add(ivProfile6);
        mImageViewArrayList.add(ivProfile7);
        mImageViewArrayList.add(ivProfile8);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            // Check for the integer request code originally supplied to startResolutionForResult().
            case PermissionsUtils.REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        Log.i(TAG, "User agreed to make required location settings changes.");
                        startLocationUpdates();
                        break;
                    case Activity.RESULT_CANCELED:
                        Log.i(TAG, "User chose not to make required location settings changes.");
                        break;
                }
                break;
        }
    }

    /**
     * Callback received when a permissions request has been completed.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {

        if (requestCode == PermissionsUtils.REQUEST_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.length <= 0) {
                // If user interaction was interrupted, the permission request is cancelled and you
                // receive empty arrays.
                Log.e(TAG, "User interaction was cancelled.");
            } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //if (mRequestingLocationUpdates) {
                Log.e(TAG, "Permission granted, updates requested, starting location updates");
                startLocationUpdates();
                //}
            } else {
                // Permission denied.

                // Notify the user via a SnackBar that they have rejected a core permission for the
                // app, which makes the Activity useless. In a real app, core permissions would
                // typically be best requested during a welcome-screen flow.

                // Additionally, it is important to remember that a permission might have been
                // rejected without asking the user for permission (device policy or "Never ask
                // again" prompts). Therefore, a user interface affordance is typically implemented
                // when permissions are denied. Otherwise, your app could appear unresponsive to
                // touches or interactions which have required permissions.
                PermissionsUtils.showSnackbar(R.string.permission_denied_explanation,
                        R.string.settings, GettingStartedActivity.this, new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                // Build intent that displays the App settings screen.
                                Intent intent = new Intent();
                                intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                Uri uri = Uri.fromParts("package", BuildConfig.APPLICATION_ID, null);
                                intent.setData(uri);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                            }
                        });
            }
        }
    }


    private void createLocationService() {
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mSettingsClient = LocationServices.getSettingsClient(this);

        // Creates a callback for receiving location events.
        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                mCurrentLocation = locationResult.getLastLocation();

                setFirstOnBoardingPage();

                stopLocationUpdates();
            }
        };

        //[START-- Create Location Request--]
        mLocationRequest = new LocationRequest();
        // Sets the desired interval for active location updates. This interval is
        // inexact. You may not receive updates at all if no location sources are available, or
        // you may receive them slower than requested. You may also receive updates faster than
        // requested if other applications are requesting location at a faster interval.
        mLocationRequest.setInterval(AppConfig.Location.UPDATE_INTERVAL_IN_MILLISECONDS);
        // Sets the fastest rate for active location updates. This interval is exact, and your
        // application will never receive updates faster than this value.
        mLocationRequest.setFastestInterval(AppConfig.Location.FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        //[END-- Create Location Request--]

        //Building Settings Request!
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        mLocationSettingsRequest = builder.build();
    }

    private void enableMyLocation() {
        createLocationService();
        if (PermissionsUtils.checkLocationPermissions(GettingStartedActivity.this)) {
            startLocationUpdates();
        } else {
            PermissionsUtils.requestPermissions(GettingStartedActivity.this);
        }
    }

    /**
     * Requests location updates from the FusedLocationApi. Note: we don't call this unless location
     * runtime permission has been granted.
     */
    private void startLocationUpdates() {

        // Begin by checking if the device has the necessary location settings.
        mSettingsClient.checkLocationSettings(mLocationSettingsRequest)
                .addOnSuccessListener(this, new OnSuccessListener<LocationSettingsResponse>() {
                    @Override
                    public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                        //noinspection MissingPermission
                        try {
                            mFusedLocationClient.requestLocationUpdates(mLocationRequest,
                                    mLocationCallback, Looper.myLooper()).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                }
                            });

                        } catch (SecurityException e) {
                            e.printStackTrace();
                        }
                    }
                })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        int statusCode = ((ApiException) e).getStatusCode();
                        switch (statusCode) {
                            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                                try {
                                    // Show the dialog by calling startResolutionForResult(), and check the
                                    // result in onActivityResult().
                                    ResolvableApiException rae = (ResolvableApiException) e;
                                    rae.startResolutionForResult(GettingStartedActivity.this, PermissionsUtils.REQUEST_CHECK_SETTINGS);
                                } catch (IntentSender.SendIntentException sie) {
                                    Log.i(TAG, "PendingIntent unable to execute request.");
                                }
                                break;
                            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                                String errorMessage = "Location settings are inadequate, and cannot be " +
                                        "fixed here. Fix in Settings.";
                                Log.e(TAG, errorMessage);
                                Toast.makeText(GettingStartedActivity.this, errorMessage, Toast.LENGTH_LONG).show();
                        }

                    }
                });
    }

    /**
     * Stop Location update
     */
    private void stopLocationUpdates() {
        mFusedLocationClient.removeLocationUpdates(mLocationCallback);
        // It is a good practice to remove location requests when the activity is in a paused or
        // stopped state. Doing so helps battery performance and is especially
        // recommended in applications that request frequent location updates.
        mFusedLocationClient.removeLocationUpdates(mLocationCallback)
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                    }
                });
    }

    /**
     * Get users near by.
     */
    private void getNearUsers() {
        GeoFire geoFire = FirebaseUtils.getGeoFire();
        GeoQuery geoQuery = geoFire.queryAtLocation(FirebaseUtils.getGeoLocation(mCurrentLocation),
                                                    AppConfig.Location.NEAR_FINDING_RADIUS);
        BaseDataManager.getInstance().setGeoQuery(geoQuery); //For remove all event listener.
        geoQuery.addGeoQueryEventListener(new GeoQueryEventListener() {
            @Override
            public void onKeyEntered(String key, GeoLocation location) {
                getUserWithKey(key, location);
            }

            @Override
            public void onKeyExited(String key) {
                //System.out.println(String.format("Key %s is no longer in the search area", key));
            }

            @Override
            public void onKeyMoved(String key, GeoLocation location) {
                //System.out.println(String.format("Key %s moved within the search area to [%f,%f]", key, location.latitude, location.longitude));
            }

            @Override
            public void onGeoQueryReady() {
                //System.out.println("All initial data has been loaded and events have been fired!");
            }

            @Override
            public void onGeoQueryError(DatabaseError error) {
                System.err.println("There was an error with this query: " + error);
            }
        });
    }

    /**
     * Get user with key
     * @param key
     */
    private void getUserWithKey(final String key, final GeoLocation location) {

        FirebaseDatabase.getInstance().getReference().child("users").child(key)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot snapshot) {
                        if (snapshot == null)
                            return;

                        User userData = snapshot.getValue(User.class);
                        userData.key = snapshot.getKey();
                        mNearUserList.add(userData);

                        tvNumberOfUsers.setText(Integer.toString(mNearUserList.size()));
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Toast.makeText(getActivity(), "Can't get user data!", Toast.LENGTH_LONG).show();
                    }
                });

    }

    private void setFirstOnBoardingPage() {
        getNearUsers();

        vgEnableLocation.setVisibility(View.GONE);
        vgOnboard.setVisibility(View.VISIBLE);
        Geocoder gcd = new Geocoder(this, Locale.getDefault());
        List<Address> addresses = null;
        try {
            addresses = gcd.getFromLocation(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude(), 1);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (addresses != null && addresses.size() > 0) {
            String locality = addresses.get(0).getLocality();
            vgLocationGroup.setVisibility(View.VISIBLE);
            tvLocation.setText(locality);
        }

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (mNearUserList.size() > 0)
                    setNumberOfNearUsers();

            }
        }, 6000); // Change to what you want
    }

    private void setNumberOfNearUsers() {
        vgLocationGroup.setVisibility(View.GONE);
        vgNearGroup.setVisibility(View.VISIBLE);

        mBoardingPagerAdapter.setHasAroundUsersFlage(true);
        vpBoardPager.setAdapter(mBoardingPagerAdapter);
        mBoardingPagerAdapter.notifyDataSetChanged();

    }

    /**
     * Show boarding page by index
     * @param pageIndex The index of page.
     */
    private void setCurrentBoardingPage(int pageIndex) {
        switch (pageIndex) {
            case 0:
                for (ObjectAnimator objectAnimator: mAnimatorList) {
                    objectAnimator.setRepeatCount(0);
                    objectAnimator.end();
                    objectAnimator.cancel();
                    objectAnimator.removeAllListeners();
                    objectAnimator.removeAllUpdateListeners();
                }

                mAnimatorList.clear();
                mIsStoppedAnimation = true;

                vgNumberOfNearBy.setVisibility(View.VISIBLE);
                vgProfilesOfNearBy.setVisibility(View.GONE);
                break;
            case 1:
                vgNumberOfNearBy.setVisibility(View.GONE);
                vgProfilesOfNearBy.setVisibility(View.VISIBLE);
                if (mIsStoppedAnimation)
                    moveImageViews();
                mIsStoppedAnimation = false;
                break;
            case 2:
                break;
            case 3:
                break;
        }
    }

    /**
     * Moving profile images.
     */
    private void moveImageViews() {
        int imageCount;
        if (mNearUserList.size() < mImageViewArrayList.size())
            imageCount = mNearUserList.size();
         else
            imageCount = mImageViewArrayList.size();

        for (int i = 0; i < imageCount; i++) {
            User user = mNearUserList.get(i);
            Glide.with(GettingStartedActivity.this).load(user.userProfile.photoUrl)
                    .bitmapTransform(new CropCircleTransformation(GettingStartedActivity.this))
                    .into(mImageViewArrayList.get(i));
            moveImage(i, mImageViewArrayList.get(i));
        }
    }

    /**
     * Move image according to path.
     * @param indexOfPosition The index of image
     * @param view  The image view.
     */
    private void moveImage(int indexOfPosition, View view) {
        if (mAnimatorList.size() >= mImageViewArrayList.size())
            return;

        Path path = new Path();
        float x = view.getX();
        float y = view.getY();
        switch (indexOfPosition) {
            case 0:
            case 5:
                path.moveTo(x + 0, y + 0);
                path.lineTo(x - 30, y + 100);
                path.lineTo(x + 20, y + 20);
                path.lineTo(x + 30, y - 30);
                path.lineTo(x + 0, y + 0);
                break;
            case 1:
            case 6:
                path.moveTo(x + 0, y + 0);
                path.lineTo(x - 10, y + 110);
                path.lineTo(x + 0, y + 100);
                path.lineTo(x + 20, y + 10);
                path.lineTo(x + 0, y + 0);
                break;
            case 2:
                path.moveTo(x + 0, y + 0);
                path.lineTo(x + 40, y + 70);
                path.lineTo(x + 0, y + 0);
                break;
            case 3:
                path.moveTo(x + 0, y + 0);
                path.lineTo(x - 30, y + 90);
                path.lineTo(x - 10, y + 50);
                path.lineTo(x + 0, y + 0);
                break;
            case 4:
                path.moveTo(x + 0, y + 0);
                path.lineTo(x + 30, y + 50);
                path.lineTo(x - 10, y + 50);
                path.lineTo(x + 0, y + 0);
                break;
            case 7:
                path.moveTo(x + 0, y + 0);
                path.lineTo(x - 50, y + 100);
                path.lineTo(x + 0, y + 0);
                break;
        }

        ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(view, View.X, View.Y, path);
        objectAnimator.setDuration(8000);
        objectAnimator.setRepeatCount(Animation.INFINITE);
        objectAnimator.start();

        mAnimatorList.add(objectAnimator);
    }

    @OnClick(R.id.btnBack)
    public void onBack() {
        onBackPressed();
    }

    @OnClick(R.id.btnEnableLocation)
    public void onEnableLocation() {
        enableMyLocation();
    }

    @OnClick(R.id.btnInviteFriends)
    public void onInviteFriends() {
        InviteFriendsActivity.start(GettingStartedActivity.this);
    }

    @OnClick(R.id.btnSetupProfile)
    public void onSetupProfile() {
        VerifyPhoneActivity.start(GettingStartedActivity.this);
    }
}
