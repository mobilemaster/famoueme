package com.hadif.famousme.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.google.firebase.auth.FirebaseUser;
import com.hadif.famousme.R;
import com.hadif.famousme.utils.PrefsUtils;

import butterknife.OnClick;

public class LaunchActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launch);
    }

    @Override
    public void onStart() {
        super.onStart();

        if (PrefsUtils.hasUserCompletedRegistration())
            MainActivity.start(LaunchActivity.this);
        else
            LogoActivity.start(LaunchActivity.this);
    }
}
