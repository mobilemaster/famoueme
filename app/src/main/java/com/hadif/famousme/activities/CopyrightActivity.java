package com.hadif.famousme.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.text.method.ScrollingMovementMethod;
import android.widget.TextView;

import com.hadif.famousme.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CopyrightActivity extends AppCompatActivity {

    public static void start (Context context, boolean isFirst) {
        Intent intent = new Intent(context, CopyrightActivity.class);
        intent.putExtra("isFirst", isFirst);
        context.startActivity(intent);
    }

    public static void start (Context context) {
        start(context, Boolean.TRUE);
    }

    //UI control
    @BindView(R.id.tvCopyrightContent)    TextView tvCopyrightContent;

    //Private Members
    private boolean mIsFirst = Boolean.TRUE;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_copyright);
        ButterKnife.bind(this);

        mIsFirst = getIntent().getBooleanExtra("isFirst", Boolean.TRUE);
        tvCopyrightContent.setMovementMethod(new ScrollingMovementMethod());
        tvCopyrightContent.setText(Html.fromHtml(getResources().getString(R.string.copyright_html)));
    }

    @OnClick(R.id.btnBack)
    public void onBack() {
        finish();
    }

    @OnClick(R.id.btnAccept)
    public void onAccept() {
        if (mIsFirst)
            VerifyPhoneActivity.start(CopyrightActivity.this);
        else
            finish();
    }
}
