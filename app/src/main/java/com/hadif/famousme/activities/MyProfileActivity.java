package com.hadif.famousme.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.PopupMenu;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.LoggingBehavior;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.share.ShareApi;
import com.facebook.share.Sharer;
import com.facebook.share.internal.ShareFeedContent;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.ShareDialog;
import com.firebase.geofire.GeoFire;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.auth.TwitterAuthProvider;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.hadif.famousme.AppConfig;
import com.hadif.famousme.BuildConfig;
import com.hadif.famousme.R;
import com.hadif.famousme.managers.basedata.BaseDataManager;
import com.hadif.famousme.managers.basedata.ThemeManager;
import com.hadif.famousme.models.Achievement;
import com.hadif.famousme.models.User;
import com.hadif.famousme.utils.FirebaseUtils;
import com.hadif.famousme.utils.PrefsUtils;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;
import com.twitter.sdk.android.tweetcomposer.TweetComposer;
import com.views.imageview.CircularImageView;
import com.views.actionsheet.ActionSheet;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnLongClick;
import jp.wasabeef.glide.transformations.CropCircleTransformation;

import static android.provider.UserDictionary.Words.APP_ID;

public class MyProfileActivity extends BaseActivity implements PopupMenu.OnMenuItemClickListener {

    public static void start (Context context) {
        Intent intent = new Intent(context, MyProfileActivity.class);
        context.startActivity(intent);
    }

    private static final String TAG = "MyProfileActivity";
    public final static int REQUEST_PROVIDE_SERVICE = 12345;

    //UI Control
    @BindView(R.id.rlHeader)                RelativeLayout rlHeader;
    @BindView(R.id.ivProfilePhoto)          CircularImageView ivProfilePhoto;
    @BindView(R.id.tvName)                  TextView tvName;
    @BindView(R.id.tvAchievePercent)        TextView tvAchievePercent;
    @BindView(R.id.tvCareerOrStatus)        TextView tvCareerOrStatus;
    @BindView(R.id.tvServiceTitle)          TextView tvServiceTitle;
    @BindView(R.id.tvServiceDetails)        TextView tvServiceDetails;
    @BindView(R.id.tvProfessionValue)       TextView tvProfessionValue;
    @BindView(R.id.tvPhoneNumberValue)      TextView tvPhoneNumberValue;
    @BindView(R.id.tvEmailValue)            TextView tvEmailValue;
    @BindView(R.id.tvBirthValue)            TextView tvBirthValue;
    @BindView(R.id.tvNationalityValue)      TextView tvNationalityValue;
    @BindView(R.id.tvInterestValue)         TextView tvInterestValue;
    @BindView(R.id.btnEditProfile)          ViewGroup btnEditProfile;
    @BindView(R.id.btnServiceEdit)          TextView btnServiceEdit;

    @BindView(R.id.llUpgradeLayout)         View llUpgradeLayout;
    @BindView(R.id.llSocialLayout)          View llSocialLayout;
    @BindView(R.id.btnUpgradeAction)        TextView btnUpgradeAction;
    @BindView(R.id.tvUpgradeTitle)          TextView tvUpgradeTitle;
    @BindView(R.id.tvStatus)                TextView tvStatus;
    @BindView(R.id.btnMenu)                 ImageView btnMenu;

    //Private Members
    private int mUpgradedState;
    private String mReVerificationId = null;
    private AlertDialog mConfirmSMSDialog = null;
    private EditText mEtSMSCode = null;
    private CallbackManager mCallbackManager = null;
    TwitterAuthClient mTwitterAuthClient;
    GoogleSignInClient mGoogleSignInClient;

    private static final int RC_SIGN_IN = 1001;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(ThemeManager.getInstance().getCurrentThemeStyle());
        super.onCreate(savedInstanceState);
        AppEventsLogger.activateApp(getApplication()); //Facebook

        setContentView(R.layout.activity_my_profile);

        mUpgradedState = 0;
        ButterKnife.bind(this);

        applyTheme();
        getAchievements();

        setUI();
    }

    @Override
    public void onResume() {
        super.onResume();
        setUI();
    }

    @Override
    protected void onActivityResult(int requestCode, int responseCode, Intent intent) {
        super.onActivityResult(requestCode, responseCode, intent);
        if(requestCode == TwitterAuthConfig.DEFAULT_AUTH_REQUEST_CODE){
            //  twitter related handling
            mTwitterAuthClient.onActivityResult(requestCode, responseCode, intent);
        } else if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(intent);
            try {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);
                firebaseAuthWithGoogle(account);
            } catch (ApiException e) {
                // Google Sign In failed, update UI appropriately
                Log.w(TAG, "Google sign in failed", e);
                // ...
            }
        } else {
            // facebook related handling
            if (mCallbackManager != null)
                mCallbackManager.onActivityResult(requestCode, responseCode, intent);
        }
    }

    private void applyTheme() {
        ThemeManager.getInstance().setThemeWithChildViews(rlHeader);
        ThemeManager.getInstance().setThemeWithChildViews(btnEditProfile);
        btnServiceEdit.setTextColor(getResources().getColor(ThemeManager.getInstance().getCurrentThemeColor()));
        btnUpgradeAction.setTextColor(getResources().getColor(ThemeManager.getInstance().getCurrentThemeColor()));
    }

    /**
     * Setting UI
     */
    private void setUI() {
        User user = BaseDataManager.getInstance().getUser();

        if (user == null || user.userProfile.photoUrl == null) {
            Toast.makeText(getApplicationContext(), R.string.msg_network_error, Toast.LENGTH_SHORT).show();
            return;
        }

        Glide.with(this).load(user.userProfile.photoUrl)
                .bitmapTransform(new CropCircleTransformation(this))
                .into(ivProfilePhoto);

        tvName.setText(user.userProfile.username);

        tvCareerOrStatus.setText(user.userProfile.profession);
        tvProfessionValue.setText(user.userProfile.profession);
        tvEmailValue.setText(user.emailAddress);
        tvBirthValue.setText(user.dateOfBirth);
        tvNationalityValue.setText(user.nationality);
        tvPhoneNumberValue.setText(getPhoneNumber());

        if (TextUtils.isEmpty(user.serviceTitle)) {
            tvServiceTitle.setText(getString(R.string.no_added_service));
            tvServiceDetails.setText("");
        } else {
            tvServiceTitle.setText(user.serviceTitle);
            tvServiceDetails.setText(user.serviceDetails);
        }

        if (user.isUpgraded) {
            mUpgradedState = 1;
            setUpdateStatus();
        }

        if (user.isAddedSocial) {
            llSocialLayout.setVisibility(View.VISIBLE);
        }

        String interestString = user.interests.replace("/", "\n");
        tvInterestValue.setText(interestString);
    }

    /**
     * Get Achievements
     */
    private void getAchievements() {
        FirebaseDatabase.getInstance().getReference().child("achievements").child(FirebaseUtils.getUserId())
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot snapshot) {
                        if (snapshot != null) {
                            float achievementPercent = 0;
                            for (DataSnapshot dataSnapshot: snapshot.getChildren()) {
                                Achievement achievement = dataSnapshot.getValue(Achievement.class);
                                achievementPercent = achievementPercent + achievement.value;
                            }

                            tvAchievePercent.setText(String.valueOf(achievementPercent) + "%");
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Toast.makeText(getApplicationContext(), "Can't get data!", Toast.LENGTH_LONG).show();
                    }
                });
    }

    /**
     * Delete my account from server
     */
    private void deleteAccountFromServer() {
        //Delete Geofire position.
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference("positions");
        GeoFire geoFire = new GeoFire(ref);
        String userId = FirebaseAuth.getInstance().getCurrentUser().getUid();
        geoFire.removeLocation(userId);

        final FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        // Prompt the user to re-provide their sign-in credentials
        currentUser.delete().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    saveUserAsDeletedAccount();
                } else {
                    Log.w(TAG,"Something is wrong!");
                    Toast.makeText(getApplicationContext(), getString(R.string.cannot_delete_account), Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    /**
     * The sms code has expired. Please re-send the verification code to try again.
     * Resend verification
     */
    private void resendPhoneVerification() {
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        PhoneAuthProvider.OnVerificationStateChangedCallbacks callbacks =
                new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
                    @Override
                    public void onVerificationCompleted(PhoneAuthCredential credential) {
                        // Update the UI and attempt sign in with the phone credential
                        if (mConfirmSMSDialog != null)
                            mConfirmSMSDialog.cancel();

                        signInWithPhoneAuthCredential(credential);
                    }

                    @Override
                    public void onVerificationFailed(FirebaseException e) {
                        if (e instanceof FirebaseAuthInvalidCredentialsException) {
                            // Invalid request
                            Snackbar.make(findViewById(android.R.id.content), e.getLocalizedMessage(),
                                    Snackbar.LENGTH_SHORT).show();
                        } else if (e instanceof FirebaseTooManyRequestsException) {
                            // The SMS quota for the project has been exceeded
                            Snackbar.make(findViewById(android.R.id.content), "Quota exceeded.",
                                    Snackbar.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onCodeSent(String verificationId,
                                           PhoneAuthProvider.ForceResendingToken token) {

                        mReVerificationId = verificationId;
                        verifyPhoneNumberWithCode();
                    }
                };

        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                currentUser.getPhoneNumber(),        // Phone number to verify
                60,                 // Timeout duration
                TimeUnit.SECONDS,   // Unit of timeout
                this,               // Activity (for callback binding)
                callbacks);        // OnVerificationStateChangedCallbacks
    }

    /**
     * Confirm sms code.
     */
    private void verifyPhoneNumberWithCode() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.enter_digit_code_label));

        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.layout_alert_confirm_sms_code, null);
        builder.setView(dialogView);
        mEtSMSCode = (EditText)dialogView.findViewById(R.id.etSMSCode);
        builder.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                mConfirmSMSDialog.cancel();
                mConfirmSMSDialog = null;
                mEtSMSCode = null;
            }
        });

        builder.setPositiveButton(getString(R.string.verify_button_title), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                String smsCode = mEtSMSCode.getText().toString();
                PhoneAuthCredential credential = PhoneAuthProvider.getCredential(mReVerificationId, smsCode);
                signInWithPhoneAuthCredential(credential);
            }
        });

        mConfirmSMSDialog = builder.create();
        mConfirmSMSDialog.show();
    }

    /**
     * Re sign in with new sms code.
     * @param credential
     */
    private void signInWithPhoneAuthCredential(final PhoneAuthCredential credential) {
        FirebaseAuth auth = FirebaseAuth.getInstance();
        auth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCredential:success");
                            PrefsUtils.saveSMSCode(credential.getSmsCode());
                            deleteAccountFromServer();
                        } else {
                            // Sign in failed, display a message and update the UI
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                // The verification code entered was invalid
                                if (mConfirmSMSDialog != null && mEtSMSCode != null)
                                    mEtSMSCode.setError("Invalid code.");
                                else
                                    Snackbar.make(findViewById(android.R.id.content), "Quota exceeded.",
                                            Snackbar.LENGTH_SHORT).show();
                            }
                        }
                    }
                });
    }


    private void reAuthenticateFacebook(){
        if (mCallbackManager == null) {
            mCallbackManager = CallbackManager.Factory.create();

            LoginManager.getInstance().registerCallback(mCallbackManager,
                    new FacebookCallback<LoginResult>() {
                        @Override
                        public void onSuccess(LoginResult loginResult) {
                            Log.d(TAG, "facebook:onSuccess:" + loginResult);
                            handleFacebookAccessToken(loginResult.getAccessToken());
                        }

                        @Override
                        public void onCancel() {
                            Toast.makeText(MyProfileActivity.this, "Login Cancel", Toast.LENGTH_LONG).show();
                        }

                        @Override
                        public void onError(FacebookException exception) {
                            Toast.makeText(MyProfileActivity.this, exception.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    });
        }

        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "email"));
    }

    private void handleFacebookAccessToken(AccessToken token) {
        Log.d(TAG, "handleFacebookAccessToken:" + token);
        FirebaseAuth auth = FirebaseAuth.getInstance();
        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        auth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCredential:success");
                            deleteAccountFromServer();

                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            Toast.makeText(MyProfileActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                            //updateUI(null);
                        }
                        // ...
                    }
                });
    }

    private void reAuthenticateTwitter(){
        mTwitterAuthClient = new TwitterAuthClient();
        mTwitterAuthClient.authorize(this, new com.twitter.sdk.android.core.Callback<TwitterSession>() {

            @Override
            public void success(Result<TwitterSession> twitterSessionResult) {
                // Success
                Log.d(TAG, "signUpWithTwitter:success");
                handleTwitterSession(twitterSessionResult.data);
            }

            @Override
            public void failure(TwitterException e) {
                e.printStackTrace();
            }
        });
    }

    private void handleTwitterSession(TwitterSession session) {
        Log.d(TAG, "handleTwitterSession:" + session);

        AuthCredential credential = TwitterAuthProvider.getCredential(
                session.getAuthToken().token,
                session.getAuthToken().secret);

        FirebaseAuth auth = FirebaseAuth.getInstance();
        auth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCredential:success");
                            deleteAccountFromServer();
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            Toast.makeText(MyProfileActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                            //updateUI(null);
                        }
                        // ...
                    }
                });
    }

    private void reAuthenticateGoogle() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    private void firebaseAuthWithGoogle(final GoogleSignInAccount acct) {
        Log.d(TAG, "firebaseAuthWithGoogle:" + acct.getId());
        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        FirebaseAuth auth = FirebaseAuth.getInstance();
        auth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCredential:success");
                            deleteAccountFromServer();

                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithCredential:failure", task.getException());

                            Toast.makeText(MyProfileActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                            //updateUI(null);
                        }
                    }
                });
    }

    /**
     * Save data that the user was deleted.
     */
    private void saveUserAsDeletedAccount() {
        User user = BaseDataManager.getInstance().getUser();

        Map<String, Object> serviceData = new HashMap<>();
        serviceData.put("isValid", Boolean.FALSE);
        serviceData.put("deletedTimestamp", ServerValue.TIMESTAMP);

        FirebaseDatabase.getInstance().getReference().child("users")
                .child(user.key).child("userProfile").updateChildren(serviceData);

        FirebaseDatabase.getInstance().getReference("positions").child(user.key).removeValue();

        //FirebaseAuth.getInstance().signOut();

        PrefsUtils.setUserNeedsToReauthorized();
        MainActivity.start(MyProfileActivity.this, AppConfig.RUNNING_EXIT);
        finish();
    }
    /**
     * Setting update status
     */
    private void setUpdateStatus() {
        if (mUpgradedState == 0) {

        } else if (mUpgradedState == 1) {
            tvUpgradeTitle.setText(getString(R.string.upgraded));
            tvStatus.setText(getString(R.string.add_your_social));
            btnUpgradeAction.setText(getString(R.string.add));

        } else {

        }
    }

    /**
     * Log out
     */
    private void logOut() {
        FirebaseAuth.getInstance().signOut();
        PrefsUtils.setUserNeedsToReauthorized();

        MainActivity.start(getApplicationContext(), AppConfig.RUNNING_EXIT);
        finish();
    }

    /**
     * Before delete my account, confirming delete code.
     */
    private void deleteMyAccount() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.confirm_delete_code_title));
        builder.setMessage(getString(R.string.confirm_delete_code_message));

        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.layout_alert_confirm_delete_code, null);
        builder.setView(dialogView);
        final EditText etDeleteCode = (EditText)dialogView.findViewById(R.id.etDeleteCode);
        builder.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        builder.setPositiveButton(getString(R.string.delete), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                String deleteCode = etDeleteCode.getText().toString();
                if (deleteCode.toLowerCase().equals("delete")) {
                    User me = BaseDataManager.getInstance().getUser();
                    if (!TextUtils.isEmpty(me.logInSocialWith)) {
                        if (me.logInSocialWith.equals("facebook"))
                            reAuthenticateFacebook();
                        else if (me.logInSocialWith.equals("twitter"))
                            reAuthenticateTwitter();
                        else if (me.logInSocialWith.equals("google"))
                            reAuthenticateGoogle();
                    } else
                        resendPhoneVerification();

                    dialog.cancel();
                }
                else
                    Toast.makeText(getApplicationContext(), getString(R.string.confirm_delete_code_message), Toast.LENGTH_LONG).show();
            }
        });

        builder.create().show();
    }

    /**
     * Upgrade more.
     */
    private void upgradeMore() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.layout_alert_upgrade_profile, null);

        builder.setView(dialogView);
        final AlertDialog dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.show();

        Button btnUpgrade = (Button)dialogView.findViewById(R.id.btnUpgrade);
        btnUpgrade.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();

                //Update runtime database with upgrade info.
                User user = BaseDataManager.getInstance().getUser();
                Map<String, Object> upgradeData = new HashMap<>();
                upgradeData.put("isUpgraded", Boolean.TRUE);
                user.isUpgraded = Boolean.TRUE;

                FirebaseDatabase.getInstance().getReference().child("users")
                        .child(user.key).updateChildren(upgradeData);

                mUpgradedState = 1;
                setUpdateStatus();
            }
        });
        TextView btnNotInterested = (TextView)dialogView.findViewById(R.id.btnNotInterested);
        btnNotInterested.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
            }
        });
    }

    @OnClick(R.id.btnBack)
    public void onBack() {
        finish();
    }

    @OnClick(R.id.btnMenu)
    public void onClickMenu() {
        PopupMenu popupMenu = new PopupMenu(MyProfileActivity.this, btnMenu);
        popupMenu.setOnMenuItemClickListener(MyProfileActivity.this);
        popupMenu.inflate(R.menu.my_profile_menu);
        popupMenu.show();
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.terms_policy:
                CopyrightActivity.start(MyProfileActivity.this, Boolean.FALSE);
                return true;
            case R.id.edit_your_service:
                ProvideServiceActivity.start(MyProfileActivity.this);
                return true;
            case R.id.edit_profile:
                EditProfileActivity.start(MyProfileActivity.this);
                return true;
            case R.id.logout:
                logOut();
                return true;
            case R.id.delete_my_account:
                final AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle(getString(R.string.delete_confirm_title));
                builder.setMessage(getString(R.string.delete_confirm_message));
                builder.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
                LayoutInflater inflater = this.getLayoutInflater();
                final View dialogView = inflater.inflate(R.layout.layout_alert_confirm_delete_account, null);
                builder.setView(dialogView);

                final AlertDialog dialog = builder.create();
                Button btnLogOut = (Button)dialogView.findViewById(R.id.btnLogOut);
                btnLogOut.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        logOut();
                        dialog.cancel();
                    }
                });

                Button btnDeleteAccount = (Button)dialogView.findViewById(R.id.btnDeleteAccount);
                btnDeleteAccount.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        deleteMyAccount();
                        dialog.cancel();
                    }
                });

                dialog.show();

                return true;
            case R.id.cancel:
                return true;
        }

        return false;
    }

    @OnLongClick(R.id.ivProfilePhoto)
    public boolean onViewPhotoFullScreen() {
        User user = BaseDataManager.getInstance().getUser();
        ImageViewerActivity.start(MyProfileActivity.this, user.userProfile.photoUrl);

        return true;
    }

    @OnClick(R.id.btnEditProfile)
    public void onEditProfile() {
        EditProfileActivity.start(MyProfileActivity.this);
    }

    @OnClick(R.id.rlAchievementGroup)
    public void onClickAchievement() {
        AchievementsActivity.start(MyProfileActivity.this);
    }

    @OnClick(R.id.btnServiceEdit)
    public void onClickDetailEdit() {
        ProvideServiceActivity.start(MyProfileActivity.this, REQUEST_PROVIDE_SERVICE);
    }

    @OnClick(R.id.btnUpgradeAction)
    public void onClickedUpgrade() {
        if (mUpgradedState == 0) {
            upgradeMore();
        } else if (mUpgradedState == 1) {
            AddMoreProfileActivity.start(MyProfileActivity.this);
        } else {

        }
    }

    @OnClick(R.id.btnSnapChat)
    public void onSnapChatClicked() {

    }

    void shareFacebookOnWall() {
        ShareDialog shareDialog = new ShareDialog(this);
        mCallbackManager = CallbackManager.Factory.create();
        shareDialog.registerCallback(mCallbackManager, new

                FacebookCallback<Sharer.Result>() {
                    @Override
                    public void onSuccess(Sharer.Result result) {
                        Log.d(TAG, "onSuccess: ");
                        Toast.makeText(MyProfileActivity.this, "onSuccess", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onCancel() {
                        Log.d(TAG, "onCancel: ");
                        Toast.makeText(MyProfileActivity.this, "onCancel", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onError(FacebookException error) {
                        Log.d(TAG, "onError: ");
                        Toast.makeText(MyProfileActivity.this, "onError" + error.toString(), Toast.LENGTH_SHORT).show();
                    }
                });

        if (ShareDialog.canShow(ShareLinkContent.class)) {
            ShareLinkContent linkContent = new ShareLinkContent.Builder()
                    .setContentTitle("I'm in use FamousMe!")
                    .setContentUrl(Uri.parse("http://developers.facebook.com/android"))
                    .build();
            shareDialog.show(linkContent);
        }
    }

    @OnClick(R.id.btnFacebook)
    public void onFacebookClicked() {
        shareFacebookOnWall();
    }

    @OnClick(R.id.btnTwitter)
    public void onTwitterClicked() {
        Uri imageUri = Uri.parse("android.resource://com.hadif.famousme/" + R.mipmap.ic_launcher);
        TweetComposer.Builder builder = new TweetComposer.Builder(this)
                .text(getString(R.string.twitter_message))
                .image(imageUri);
        builder.show();
    }
}
