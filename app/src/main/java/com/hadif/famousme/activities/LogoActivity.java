package com.hadif.famousme.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.hadif.famousme.R;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class LogoActivity extends AppCompatActivity {

    public static void start (Context context) {
        Intent intent = new Intent(context, LogoActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logo);

        ButterKnife.bind(this);
    }

    @Override
    public void onBackPressed() {
        // Your Code Here. Leave empty if you want nothing to happen on back press.
    }

    @OnClick(R.id.btnGetStarted)
    public void onGetStarted() {
        //CopyrightActivity.start(LogoActivity.this);
        GettingStartedActivity.start(LogoActivity.this);
    }

    @OnClick(R.id.btnHaveAccount)
    public void onHaveAccount() {
        VerifyPhoneActivity.start(LogoActivity.this, true);
    }
}
