package com.hadif.famousme.activities;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.support.design.widget.Snackbar;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseNetworkException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.auth.TwitterAuthProvider;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.hadif.famousme.AppConfig;
import com.hadif.famousme.R;
import com.hadif.famousme.managers.basedata.BaseDataManager;
import com.hadif.famousme.models.Achievement;
import com.hadif.famousme.models.DiscoveryRules;
import com.hadif.famousme.models.Social;
import com.hadif.famousme.models.User;
import com.hadif.famousme.utils.BaseUtils;
import com.hadif.famousme.utils.FirebaseUtils;
import com.hadif.famousme.utils.ImageUtils;
import com.hadif.famousme.utils.PrefsUtils;
import com.hadif.famousme.utils.pickers.Country;
import com.hadif.famousme.utils.pickers.CountryPicker;
import com.hadif.famousme.utils.pickers.CountryPickerListener;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.Twitter;
import com.twitter.sdk.android.core.TwitterApiClient;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class VerifyPhoneActivity extends BaseActivity {

    public static void start (Context context) {
        start(context, false);
    }

    public static void start (Context context, boolean isHaveAccount) {
        Intent intent = new Intent(context, VerifyPhoneActivity.class);
        intent.putExtra("isHaveAccount", isHaveAccount);
        context.startActivity(intent);
    }

    //UI control
    @BindView(R.id.etPhoneNumber)           EditText etPhoneNumber;
    @BindView(R.id.tvCountryName)           TextView tvCountryName;
    @BindView(R.id.rlSendVerifyCode)        RelativeLayout rlSendVerifyCode;
    @BindView(R.id.rlVerifyCode)            RelativeLayout rlVerifyCode;
    @BindView(R.id.tvPhoneNumber)           TextView tvPhoneNumber;
    @BindView(R.id.etDigitCode)             EditText etDigitCode;
    @BindView(R.id.btnBack)                 ImageView btnBack;
    @BindView(R.id.tvTitle)                 TextView tvTitle;

    //Social Log-in
    @BindView(R.id.lySocialGroup)           View lySocialGroup;
    @BindView(R.id.tvNext)                  TextView tvNext;
    @BindView(R.id.btnSendVerifyCode)       TextView btnSendVerifyCode;

    //Private members
    String mCountryName;
    String mCountryCode;
    String mPhoneNumber;
    Boolean mIsHaveAccount = false;

    private static final String TAG = "VerifyPhoneActivity";
    private static final String KEY_VERIFY_IN_PROGRESS = "key_verify_in_progress";

    private static final int STATE_INITIALIZED = 1;
    private static final int STATE_CODE_SENT = 2;
    private static final int STATE_VERIFY_FAILED = 3;
    private static final int STATE_VERIFY_SUCCESS = 4;
    private static final int STATE_SIGNIN_FAILED = 5;
    private static final int STATE_SIGNIN_SUCCESS = 6;

    private static final int SOCIAL_FACEBOOK = 1;
    private static final int SOCIAL_TWITTER = 2;
    private static final int SOCIAL_GOOGLE = 3;

    private static final int RC_SIGN_IN = 1001;

    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;
    private FirebaseAuth mAuth;
    private PhoneAuthProvider.ForceResendingToken mResendToken;
    private String mVerificationId;

    private boolean mVerificationInProgress = false;
    private CallbackManager mCallbackManager = null;
    TwitterAuthClient mTwitterAuthClient;
    GoogleSignInClient mGoogleSignInClient;
    GoogleSignInAccount mGoogleSignInAccount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_phone);
        ButterKnife.bind(this);

        setCountyText();

        mIsHaveAccount = getIntent().getBooleanExtra("isHaveAccount", false);

        //Show social buttons on have an account mode.
        lySocialGroup.setVisibility(View.VISIBLE);
        tvNext.setVisibility(View.VISIBLE);
        btnSendVerifyCode.setVisibility(View.GONE);

        // [START initialize_auth]
        mAuth = FirebaseAuth.getInstance();
        // [END initialize_auth]

        // Initialize phone auth callbacks
        // [START phone_auth_callbacks]
        mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

            @Override
            public void onVerificationCompleted(PhoneAuthCredential credential) {
                // This callback will be invoked in two situations:
                // 1 - Instant verification. In some cases the phone number can be instantly
                //     verified without needing to send or enter a verification code.
                // 2 - Auto-retrieval. On some devices Google Play services can automatically
                //     detect the incoming verification SMS and perform verification without
                //     user action.
                Log.d(TAG, "onVerificationCompleted:" + credential);
                mVerificationInProgress = false;

                // Update the UI and attempt sign in with the phone credential
                updateUI(STATE_VERIFY_SUCCESS, credential);
                signInWithPhoneAuthCredential(credential);
            }

            @Override
            public void onVerificationFailed(FirebaseException e) {
                // This callback is invoked in an invalid request for verification is made,
                // for instance if the the phone number format is not valid.
                Log.w(TAG, "onVerificationFailed", e);
                mVerificationInProgress = false;
                if (e instanceof FirebaseAuthInvalidCredentialsException) {
                    // Invalid request
                    etPhoneNumber.setError("Invalid phone number.");
                } else if (e instanceof FirebaseTooManyRequestsException) {
                    // The SMS quota for the project has been exceeded
                    Snackbar.make(findViewById(android.R.id.content), "Quota exceeded.",
                            Snackbar.LENGTH_SHORT).show();
                } else if (e instanceof FirebaseNetworkException)
                    Snackbar.make(findViewById(android.R.id.content), "A network Error. Please check your network or Google play service",
                            Snackbar.LENGTH_SHORT).show();

                // Show a message and update the UI
                updateUI(STATE_VERIFY_FAILED);
            }

            @Override
            public void onCodeSent(String verificationId,
                                   PhoneAuthProvider.ForceResendingToken token) {
                // The SMS verification code has been sent to the provided phone number, we
                // now need to ask the user to enter the code and then construct a credential
                // by combining the code with a verification ID.
                Log.d(TAG, "onCodeSent:" + verificationId);

                // Save verification ID and resending token so we can use them later
                mVerificationId = verificationId;
                mResendToken = token;

                // Update UI
                updateUI(STATE_CODE_SENT);
            }
        };
        // [END phone_auth_callbacks]
    }

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
        updateUI(currentUser);

        if (mVerificationInProgress && validatePhoneNumber()) {
            startPhoneNumberVerification(etPhoneNumber.getText().toString());
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(KEY_VERIFY_IN_PROGRESS, mVerificationInProgress);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mVerificationInProgress = savedInstanceState.getBoolean(KEY_VERIFY_IN_PROGRESS);
    }

    @Override
    protected void onActivityResult(int requestCode, int responseCode, Intent intent) {
        super.onActivityResult(requestCode, responseCode, intent);
        if(requestCode == TwitterAuthConfig.DEFAULT_AUTH_REQUEST_CODE){
            //  twitter related handling
            mTwitterAuthClient.onActivityResult(requestCode, responseCode, intent);
        } else if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(intent);
            try {
                // Google Sign In was successful, authenticate with Firebase
                mGoogleSignInAccount = task.getResult(ApiException.class);
                saveUserSocialAccount(SOCIAL_GOOGLE);

            } catch (ApiException e) {
                // Google Sign In failed, update UI appropriately
                Log.w(TAG, "Google sign in failed", e);
                // ...
            }
        } else {
            // facebook related handling
            mCallbackManager.onActivityResult(requestCode, responseCode, intent);

        }
    }

    /**
     * Start Phone Number verification.
     * @param phoneNumber
     */
    private void startPhoneNumberVerification(String phoneNumber) {
        mPhoneNumber = mCountryCode + " " + phoneNumber;
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                mPhoneNumber,        // Phone number to verify
                60,                 // Timeout duration
                TimeUnit.SECONDS,   // Unit of timeout
                this,               // Activity (for callback binding)
                mCallbacks);        // OnVerificationStateChangedCallbacks

        mVerificationInProgress = true;
    }

    /**
     * Check if the phone number is valid or not
     * @return
     */
    private boolean validatePhoneNumber() {
        String phoneNumber = etPhoneNumber.getText().toString();
        if (TextUtils.isEmpty(phoneNumber) || phoneNumber.length() < 7) {
            AlertDialog alertDialog = new AlertDialog.Builder(VerifyPhoneActivity.this).create();
            alertDialog.setTitle(R.string.verify_error_title);
            alertDialog.setMessage(getResources().getString(R.string.verify_error_message));
            alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            alertDialog.show();

            return false;
        }

        return true;
    }

    /**
     * Verify code.
     * @param verificationId
     * @param code
     */
    private void verifyPhoneNumberWithCode(String verificationId, String code) {
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationId, code);
        signInWithPhoneAuthCredential(credential);
    }

    /**
     * Resend verification code
     * @param phoneNumber
     * @param token
     */
    private void resendVerificationCode(String phoneNumber,
                                        PhoneAuthProvider.ForceResendingToken token) {
        mPhoneNumber = mCountryCode + " " + phoneNumber;
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                mPhoneNumber,        // Phone number to verify
                60,                 // Timeout duration
                TimeUnit.SECONDS,   // Unit of timeout
                this,               // Activity (for callback binding)
                mCallbacks,         // OnVerificationStateChangedCallbacks
                token);             // ForceResendingToken from callbacks
    }

    /**
     * Sign In after verification.
     * @param credential
     */
    private void signInWithPhoneAuthCredential(final PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCredential:success");

                            FirebaseUser user = task.getResult().getUser();
                            updateUI(STATE_SIGNIN_SUCCESS, user);

                        } else {
                            // Sign in failed, display a message and update the UI
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                // The verification code entered was invalid
                                etDigitCode.setError("Invalid code.");
                            }

                            // Update UI
                            updateUI(STATE_SIGNIN_FAILED);
                        }
                    }
                });
    }

    /**
     * Update UI
     * @param uiState
     */
    private void updateUI(int uiState) {
        updateUI(uiState, mAuth.getCurrentUser(), null);
    }

    private void updateUI(FirebaseUser user) {
        if (user != null) {
            updateUI(STATE_SIGNIN_SUCCESS, user);
        } else {
            updateUI(STATE_INITIALIZED);
        }
    }

    private void updateUI(int uiState, FirebaseUser user) {
        updateUI(uiState, user, null);
    }

    private void updateUI(int uiState, PhoneAuthCredential cred) {
        updateUI(uiState, null, cred);
    }

    private void updateUI(int uiState, FirebaseUser user, PhoneAuthCredential cred) {
        switch (uiState) {
            case STATE_INITIALIZED:
                // Initialized state, show only the phone number field and start button
                rlSendVerifyCode.setVisibility(View.VISIBLE);
                rlVerifyCode.setVisibility(View.GONE);
                break;
            case STATE_CODE_SENT:
                // Code sent state, show the verification field, the
                rlSendVerifyCode.setVisibility(View.GONE);
                tvNext.setVisibility(View.INVISIBLE);
                mPhoneNumber = mCountryCode + " " + etPhoneNumber.getText();
                tvTitle.setText(R.string.verify_phone_title);
                rlVerifyCode.setVisibility(View.VISIBLE);
                btnBack.setVisibility(View.GONE);
                tvPhoneNumber.setText(mPhoneNumber);
                break;
            case STATE_VERIFY_FAILED:
                // Verification has failed, show all options
                break;
            case STATE_VERIFY_SUCCESS:
                // Verification has succeeded, proceed to firebase sign in
                // Set the verification text based on the credential
                break;
            case STATE_SIGNIN_FAILED:
                // No-op, handled by sign-in check

                break;
            case STATE_SIGNIN_SUCCESS:
                // Np-op, handled by sign-in check
                gotoNextPage();
                break;
        }
    }

    private void gotoNextPage() {
        if (mIsHaveAccount) {
            FirebaseDatabase.getInstance().getReference().child("users")
                    .addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot snapshot) {
                            String userId = FirebaseUtils.getUserId();
                            if (snapshot.hasChild(userId)) {
                                PrefsUtils.userCompletedRegistered(true);
                                MainActivity.start(VerifyPhoneActivity.this, true);
                            } else
                                SetupProfileActivity.start(VerifyPhoneActivity.this, mCountryCode);
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            Toast.makeText(getApplicationContext(), "Can't get data!", Toast.LENGTH_LONG).show();
                        }
                    });
        }
        else
            SetupProfileActivity.start(VerifyPhoneActivity.this, mCountryCode);
    }

    private void gotoNextPageWithSocial(final int socialIndex) {
        if (mIsHaveAccount) {
            FirebaseDatabase.getInstance().getReference().child("users")
                    .addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot snapshot) {
                            String userId = FirebaseUtils.getUserId();
                            if (snapshot.hasChild(userId)) {
                                PrefsUtils.userCompletedRegistered(true);
                                MainActivity.start(VerifyPhoneActivity.this, true);
                            } else
                                saveUserSocialAccount(socialIndex);
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            Toast.makeText(getApplicationContext(), "Can't get data!", Toast.LENGTH_LONG).show();
                        }
                    });
        }
        else
            saveUserSocialAccount(socialIndex);
    }

    private void saveUserSocialAccount(int socialIndex) {
        if (socialIndex == SOCIAL_FACEBOOK)
            saveFacebookUserInfo();
        else if (socialIndex == SOCIAL_TWITTER)
            saveUserWithTwitter();
        else
            saveUserWithGoogle();
    }

    private void setCountry(String name, String code) {
        mCountryName = name;
        mCountryCode = code;

        tvCountryName.setText(mCountryName + "(" + mCountryCode + ")");
    }

    private void setCountyText() {
        String localeCode;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
            localeCode = getResources().getConfiguration().getLocales().get(0).getCountry();
        else
            localeCode = getResources().getConfiguration().locale.getCountry();
        Country country = getCountryByCode(localeCode);
        if (country == null) {
            mCountryName = "United Arab Emirates";
            mCountryCode = "+971";
        } else {
            mCountryName = country.getName();
            mCountryCode = country.getDialCode();
        }

        tvCountryName.setText(mCountryName + "(" + mCountryCode + ")");
    }

    private Country getCountryByCode(String localeCode) {
        try {
            String allCountriesCode = BaseDataManager.readEncodedJsonString(VerifyPhoneActivity.this);

            JSONArray countryArray = new JSONArray(allCountriesCode);

            for (int i = 0; i < countryArray.length(); i++) {
                JSONObject jsonObject = countryArray.getJSONObject(i);
                String countryName = jsonObject.getString("name");
                String countryDialCode = jsonObject.getString("dial_code");
                String countryCode = jsonObject.getString("code");
                if (localeCode.equals(countryCode)) {
                    Country country = new Country();
                    country.setCode(countryCode);
                    country.setName(countryName);
                    country.setDialCode(countryDialCode);

                    return country;
                }
            }

        }catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    private void signUpWithFacebook() {
        if (mCallbackManager == null) {
            mCallbackManager = CallbackManager.Factory.create();

            LoginManager.getInstance().registerCallback(mCallbackManager,
                    new FacebookCallback<LoginResult>() {
                        @Override
                        public void onSuccess(LoginResult loginResult) {
                            Log.d(TAG, "facebook:onSuccess:" + loginResult);
                            handleFacebookAccessToken(loginResult.getAccessToken());
                        }

                        @Override
                        public void onCancel() {
                            Toast.makeText(VerifyPhoneActivity.this, "Login Cancel", Toast.LENGTH_LONG).show();
                        }

                        @Override
                        public void onError(FacebookException exception) {
                            Toast.makeText(VerifyPhoneActivity.this, exception.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    });
        }

        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "email"));
    }

    private void handleFacebookAccessToken(AccessToken token) {
        Log.d(TAG, "handleFacebookAccessToken:" + token);

        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCredential:success");
                           gotoNextPageWithSocial(SOCIAL_FACEBOOK);

                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            Toast.makeText(VerifyPhoneActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                            //updateUI(null);
                        }
                        // ...
                    }
                });
    }

    private void saveFacebookUserInfo() {
        Bundle params = new Bundle();
        params.putString("fields", "id, email, name, gender, birthday, picture.type(large)");
        new GraphRequest(AccessToken.getCurrentAccessToken(), "me", params, HttpMethod.GET,
                new GraphRequest.Callback() {
                    @Override
                    public void onCompleted(GraphResponse response) {
                        if (response != null) {
                            User user = getNewUserForSocial();
                            try {
                                String photoUrl = "";
                                JSONObject data = response.getJSONObject();
                                if (data.has("picture"))
                                    photoUrl = data.getJSONObject("picture").getJSONObject("data").getString("url");

                                if (data.getString("gender").equals("male"))
                                    user.gender = 0;
                                else
                                    user.gender = 1;

                                user.logInSocialWith = "facebook";
                                user.userProfile.username = data.getString("name");
                                user.userProfile.photoUrl = photoUrl;
                                user.userProfile.avatarUrl = photoUrl;
                                user.emailAddress = data.getString("email");

                                //Save user
                                saveUserData(user);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }).executeAsync();
    }

    private void signUpWithTwitter() {
        mTwitterAuthClient = new TwitterAuthClient();
        mTwitterAuthClient.authorize(this, new com.twitter.sdk.android.core.Callback<TwitterSession>() {

            @Override
            public void success(Result<TwitterSession> twitterSessionResult) {
                // Success
                Log.d(TAG, "signUpWithTwitter:success");
                handleTwitterSession(twitterSessionResult.data);
            }

            @Override
            public void failure(TwitterException e) {
                e.printStackTrace();
            }
        });
    }

    private void handleTwitterSession(TwitterSession session) {
        Log.d(TAG, "handleTwitterSession:" + session);

        AuthCredential credential = TwitterAuthProvider.getCredential(
                session.getAuthToken().token,
                session.getAuthToken().secret);

        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCredential:success");
                            gotoNextPageWithSocial(SOCIAL_TWITTER);
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            Toast.makeText(VerifyPhoneActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                            //updateUI(null);
                        }
                        // ...
                    }
                });
    }

    private void saveUserWithTwitter() {
        TwitterApiClient twitterApiClient = TwitterCore.getInstance().getApiClient();
        twitterApiClient.getAccountService().verifyCredentials(true, true, false).enqueue(new Callback<com.twitter.sdk.android.core.models.User>() {
            @Override
            public void success(Result<com.twitter.sdk.android.core.models.User> result) {
                com.twitter.sdk.android.core.models.User user = result.data;

                User me = getNewUserForSocial();
                String photoUrl = user.profileImageUrl;

                me.gender = 0; //default
                me.logInSocialWith = "twitter";
                me.userProfile.username = user.screenName;
                me.userProfile.photoUrl = photoUrl;
                me.userProfile.avatarUrl = photoUrl;
                me.emailAddress = "";

                saveUserData(me);
            }

            @Override
            public void failure(TwitterException exception) {

            }
        });
    }

    private void signUpWithGoogle() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    private void saveUserWithGoogle() {
        if (mGoogleSignInAccount == null)
            return;

        Log.d(TAG, "firebaseAuthWithGoogle:" + mGoogleSignInAccount.getId());
        AuthCredential credential = GoogleAuthProvider.getCredential(mGoogleSignInAccount.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCredential:success");
                            User me = getNewUserForSocial();
                            String photoUrl =  mGoogleSignInAccount.getPhotoUrl().toString();

                            me.gender = 0; //default
                            me.logInSocialWith = "google";
                            me.userProfile.username = mGoogleSignInAccount.getDisplayName();
                            me.userProfile.photoUrl = photoUrl;
                            me.userProfile.avatarUrl = photoUrl;
                            me.emailAddress = mGoogleSignInAccount.getEmail();

                            saveUserData(me);

                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithCredential:failure", task.getException());

                            Toast.makeText(VerifyPhoneActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                            //updateUI(null);
                        }
                    }
                });
    }

    private User getNewUserForSocial() {
        User user = new User();

        user.userProfile.isValid = true;
        user.userProfile.deletedTimestamp = 0;
        user.userProfile.username = "";

        user.countryCode = mCountryCode;

        user.dateOfBirth = "";
        user.phoneNumber = "";

        user.userProfile.profession = "";
        user.nationality = "";
        user.isUpgraded = false;
        user.isAddedSocial = false;

        user.serviceTitle = "";
        user.serviceDetails = "";

        if (user.discoveryRules == null)
            user.discoveryRules = new DiscoveryRules();

        //Initialize Discovery rules
        user.discoveryRules.discoverNationality = "All";
        user.discoveryRules.discoveryInterest = "All";

        if (user.social == null)
            user.social = new Social();

        user.token = "";
        ArrayList<String> interestList =
                new ArrayList<String>(Arrays.asList(getResources().getStringArray(R.array.interests_list)));

        String interestData = "";
        for (String interest : interestList) {
            if (interestData.equals(""))
                interestData = interest;
            else
                interestData = interestData + "/" + interest;
        }

        user.interests = interestData;
        return user;
    }

    private void saveUserData(User user) {
        showProgressDialog();
        setProgressDialogMessage("Saving...");

        FirebaseDatabase.getInstance().getReference().child("users").child(getUid())
                .setValue(user).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                hideProgressDialog();
                if (task.isSuccessful()) {
                    PrefsUtils.userCompletedRegistered(true);

                    MainActivity.start(VerifyPhoneActivity.this);
                } else {
                    Toast.makeText(getApplicationContext(), "Saving failed.", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    @OnClick(R.id.btnBack)
    public void onBack() {
        onBackPressed();
    }

    @OnClick(R.id.btnSendVerifyCode)
    public void onSendVerifyCode() {
        if (!validatePhoneNumber()) {
            return;
        }

        startPhoneNumberVerification(etPhoneNumber.getText().toString());
    }

    @OnClick(R.id.btnVerify)
    public void onVerify() {
        String code = etDigitCode.getText().toString();
        if (TextUtils.isEmpty(code)) {
            etDigitCode.setError(getString(R.string.can_not_empty));
            return;
        }

        verifyPhoneNumberWithCode(mVerificationId, code);
    }

    @OnClick(R.id.btnNotGetCode)
    public void onNotGetCode() {
        resendVerificationCode(mPhoneNumber.toString(), mResendToken);
    }

    @OnClick(R.id.btnSendCode)
    public void onSendNewCodeAgain() {
        mResendToken = null;
        mVerificationId = null;

        updateUI(STATE_INITIALIZED);
    }

    @OnClick(R.id.btnFacebook)
    public void onClickFacebook() {
        signUpWithFacebook();
    }

    @OnClick(R.id.btnTwitter)
    public void onClickTwitter() {
        signUpWithTwitter();
    }

    @OnClick(R.id.btnGoogle)
    public void onClickGoogle() {
        signUpWithGoogle();
    }

    @OnClick(R.id.tvNext)
    public void onClickNext() {
        if (!validatePhoneNumber()) {
            return;
        }

        startPhoneNumberVerification(etPhoneNumber.getText().toString());
    }


    @OnClick(R.id.tvCountryName)
    public void onSelectCountry() {
        final CountryPicker picker = CountryPicker.newInstance("SelectCountry");
        picker.setListener(new CountryPickerListener() {
            @Override
            public void onSelectCountry(String name, String code, String dialCode) {
                picker.dismiss();
                setCountry(name, dialCode);
            }
        });
        picker.show(getSupportFragmentManager(), "COUNTRY_CODE_PICKER");
    }

}
