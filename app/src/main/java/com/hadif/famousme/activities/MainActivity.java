package com.hadif.famousme.activities;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.firebase.geofire.GeoFire;
import com.firebase.geofire.GeoLocation;
import com.firebase.geofire.GeoQuery;
import com.firebase.geofire.GeoQueryEventListener;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;

import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.hadif.famousme.AppConfig;
import com.hadif.famousme.BuildConfig;
import com.hadif.famousme.R;
import com.hadif.famousme.fragments.BasicFragment;
import com.hadif.famousme.fragments.ConversationsFragment;
import com.hadif.famousme.fragments.SettingsFragment;
import com.hadif.famousme.fragments.MapFragment;
import com.hadif.famousme.fragments.NotificationsFragment;
import com.hadif.famousme.managers.basedata.BaseDataManager;
import com.hadif.famousme.managers.basedata.ThemeManager;
import com.hadif.famousme.models.Contact;
import com.hadif.famousme.models.Discovery;
import com.hadif.famousme.models.DiscoveryRules;
import com.hadif.famousme.models.MapLocationItem;
import com.hadif.famousme.models.Notification;
import com.hadif.famousme.models.NudgeMessage;
import com.hadif.famousme.models.User;
import com.hadif.famousme.utils.BaseUtils;
import com.hadif.famousme.utils.DateUtils;
import com.hadif.famousme.utils.FirebaseUtils;
import com.hadif.famousme.utils.PrefsUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import jp.wasabeef.glide.transformations.CropCircleTransformation;

import static android.content.Intent.FLAG_ACTIVITY_CLEAR_TOP;
import static com.hadif.famousme.AppConfig.DiscoveryMode.COUNTRY_DISCOVERY_MODE;
import static com.hadif.famousme.AppConfig.DiscoveryMode.RANGE_DISCOVERY_MODE;
import static com.hadif.famousme.AppConfig.Location.NEAR_FINDING_RADIUS;
import static com.hadif.famousme.AppConfig.Location.UPDATE_INTERVAL_IN_MILLISECONDS;
import static com.hadif.famousme.AppConfig.MAX_GET_DISCOVERIES_SKIP_TIME;

public class MainActivity extends BaseActivity implements BasicFragment.OnCompleteListener {

    public static void start (Context context, int runningMode) {
        Intent intent = new Intent(context, MainActivity.class);
        intent.putExtra("runningMode", runningMode);
        if (runningMode == AppConfig.RUNNING_EXIT)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        context.startActivity(intent);
    }

    public static void start (Context context, boolean isHaveAccount) {
        Intent intent = new Intent(context, MainActivity.class);
        intent.putExtra("isHaveAccount", isHaveAccount);
        intent.putExtra("runningMode", AppConfig.RUNNING_NORMAL);

        context.startActivity(intent);
    }

    public static void start (Context context) {
        start(context, AppConfig.RUNNING_NORMAL);
    }

    //UI Control
    private static final int TAB_CONVERSATION = 0;
    private static final int TAB_MAP = 1;
    private static final int TAB_NOTIFICATION = 2;
    private static final int TAB_SETTINGS = 3;

    @BindView(R.id.vpViewPager)                 ViewPager vpViewPager;
    @BindView(R.id.ivChatsItem)                 ImageView ivChatsItem;
    @BindView(R.id.ivMapItem)                   ImageView ivMapItem;
    @BindView(R.id.ivNotificationsItem)         ImageView ivNotificationsItem;
    @BindView(R.id.ivSettingsItem)              ImageView ivSettingsItem;

    @BindView(R.id.tvChatsTitle)                TextView tvChatsTitle;
    @BindView(R.id.tvMapTitle)                  TextView tvMapTitle;
    @BindView(R.id.tvNotificationsTitle)        TextView tvNotificationsTitle;
    @BindView(R.id.tvSettingsTitle)             TextView tvSettingsTitle;

    @BindView(R.id.tvChatsBadge)                TextView tvChatsBadge;
    @BindView(R.id.tvMapBadge)                  TextView tvMapBadge;
    @BindView(R.id.tvNotificationsBadge)        TextView tvNotificationsBadge;

    //Private members
    private static final String TAG = "MainActivity";
    private ArrayList<TabItemHolder> mTabItems = new ArrayList<>();

    private ConversationsFragment mConversationsFragment = null;
    private MapFragment mMapFragment = null;
    private NotificationsFragment mNotificationsFragment = null;
    private SettingsFragment mSettingsFragment = null;
    private FragmentPagerAdapter mFragmentPagerAdapter;

    private boolean mIsFirstStarted = false;
    private Location mCurrentLocation;

    private int mCount = 0;


    /**
     * Code used in requesting runtime permissions.
     */
    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 34;

    /**
     * Constant used in the location settings dialog.
     */
    private static final int REQUEST_CHECK_SETTINGS = 0x1;

    /**
     * Provides access to the Fused Location Provider API.
     */
    private FusedLocationProviderClient mFusedLocationClient;

    /**
     * Provides access to the Location Settings API.
     */
    private SettingsClient mSettingsClient;

    private LocationRequest mLocationRequest;
    private LocationSettingsRequest mLocationSettingsRequest;
    private Boolean mRequestingLocationUpdates = false;
    private Boolean mIsGotUserInfo = false;
    private Boolean mIsChangedRules = false;

    private DatabaseReference mNotificationRef;
    private ChildEventListener mNotificationChildEventListener;

    private boolean mCompletedConversationFragment = false;
    private boolean mCompletedMapFragment = false;
    private boolean mCompletedNotificationFragment = false;
    private boolean mCompletedSettingsFragment = false;

    private int mCountCompletedFragment = 0;
    private int mGotDiscoveriesTimes = 0;
    private int mCountryFilterTimes = 0;

    /**
     * Callback for Location events.
     */
    private LocationCallback mLocationCallback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(ThemeManager.getInstance().getCurrentThemeStyle());
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        setFragments();

        if (!BaseDataManager.hasValidInterestList())
            getInterestList();

        if (!BaseDataManager.hasValidNationalityList())
            getNationalityList();

        if (PrefsUtils.isUsedAppAtFirst()){
            PrefsUtils.setUsedAppAtFirst();
        } else {
            if (DateUtils.getGapOfDate(PrefsUtils.getStartedUsingAppTime()) > 14)
                FirebaseUtils.updateAchievements(AppConfig.Achievement.ACHIEVEMENT_USE_APP_TWO_WEEKS, MainActivity.this);
        }

        if (FirebaseAuth.getInstance() != null &&
                FirebaseAuth.getInstance().getCurrentUser() != null) {
            //Update token for push notification.
            String token = FirebaseInstanceId.getInstance().getToken();
            String userId = FirebaseAuth.getInstance().getCurrentUser().getUid();
            Map<String, Object> tokenMap = new HashMap<>();
            tokenMap.put("token", token);

            FirebaseDatabase.getInstance().getReference()
                    .child("users").child(userId).updateChildren(tokenMap);
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if (mIsFirstStarted) {
            //When backing to main activity from chat or user profile,
            // it should stay where it is in discovery page.
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    startLocationUpdates();
                }

            }, UPDATE_INTERVAL_IN_MILLISECONDS); // Change to what you want
        }
    }

    @Override
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        int runningMode = intent.getIntExtra("runningMode", AppConfig.RUNNING_NORMAL);
        if (runningMode == AppConfig.RUNNING_GO_TO_MAP)
            gotoMapFragment();
        else if (runningMode == AppConfig.RUNNING_EXIT) { //Logout or Delete Account.
            deletedAccount();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        // Remove location updates to save battery.
        stopLocationUpdates();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onBackPressed() {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            // Check for the integer request code originally supplied to startResolutionForResult().
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        Log.i(TAG, "User agreed to make required location settings changes.");
                        startLocationUpdates();
                        break;
                    case Activity.RESULT_CANCELED:
                        Log.i(TAG, "User chose not to make required location settings changes.");
                        mRequestingLocationUpdates = false;
                        break;
                }
                break;
            case AppConfig.Request.REQUEST_USER_PROFILE:
            case AppConfig.Request.REQUEST_CHAT:
                if (resultCode == RESULT_OK) {
                    mConversationsFragment.resetUserProfileByActivityResult(data);
                    mNotificationsFragment.resetUserProfileByActivityResult(data);
                }

        }
    }

    /**
     * Set fragments
     */
    private void setFragments() {
        showProgressDialog();
        setProgressDialogMessage("Initializing...");
        mConversationsFragment = ConversationsFragment.newInstance();
        mMapFragment = MapFragment.newInstance();
        mNotificationsFragment = NotificationsFragment.newInstance();
        mSettingsFragment = SettingsFragment.newInstance();

        Boolean isHaveAccount = getIntent().getBooleanExtra("isHaveAccount", false);
        if (isHaveAccount)
            mConversationsFragment.setShowWelcomeMessage(true);

        vpViewPager.setOffscreenPageLimit(4);
        vpViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }
            @Override
            public void onPageSelected(int position) {
                setCurrentTab(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

        //Setup page adapter
        mFragmentPagerAdapter = new FragmentPagerAdapter(getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                switch (position) {
                    case 0:
                        return mConversationsFragment;
                    case 1:
                        return mMapFragment;
                    case 2:
                        return mNotificationsFragment;
                    case 3:
                        return mSettingsFragment;
                    default:
                        return mConversationsFragment;
                }
            }

            @Override
            public int getCount() {
                return 4;
            }

            @Override
            public CharSequence getPageTitle(int position) {
                switch (position) {
                    case 0:
                        return getString(R.string.conversations_title);
                    case 1:
                        return getString(R.string.map_title);
                    case 2:
                        return getString(R.string.notifications_title);
                    case 3:
                        return getString(R.string.settings_title);
                    default:
                        return getString(R.string.conversations_title);
                }
            }

        };

        vpViewPager.setAdapter(mFragmentPagerAdapter);
    }

    public void onComplete() {
        mCountCompletedFragment ++;
        Log.e("completed count", String.valueOf(mCountCompletedFragment));
        checkIfPreparedFragments();
    }


    private void deletedAccount() {
        BaseDataManager.getInstance().removeAllBaseData();
        stopNotifications();
        stopLocationUpdates();
        LogoActivity.start(MainActivity.this);
        finish();
    }

    /**
     * After completing loading fragments!!!
     * Sometimes, it's slow to load view via onCreateView.
     */
    private void checkIfPreparedFragments() {
        if (mCountCompletedFragment == 4) {
            getUserInfo();

            setTabItems();
            setCurrentTab(TAB_CONVERSATION);

            createLocationService();
            //getAllDiscoveries();

            monitoringNudgeMessages();
        }
    }
    /**
     * Set online status on my profile
     */
    private void updateUserStatus() {
        // since I can connect from multiple devices, we store each connection instance separately
        // any time that connectionsRef's value is null (i.e. has no children) I am offline
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        if (database == null)
            return;

        final DatabaseReference statusOnlineRef = database.getReference("users/" + getUid() + "/status/isOnline");

        // stores the timestamp of my last disconnect (the last time I was seen online)
        final DatabaseReference lastOnlineTimeRef = database.getReference("users/" + getUid() + "/status/timestamp");

        final DatabaseReference connectedRef = database.getReference(".info/connected");
        connectedRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                boolean connected = snapshot.getValue(Boolean.class);
                if (connected) {
                    // add this device to my connections list
                    // this value could contain info about the device or a timestamp too
                    statusOnlineRef.setValue(Boolean.TRUE);
                    statusOnlineRef.onDisconnect().setValue(Boolean.FALSE);

                    // when I disconnect, update the last time I was seen online
                    lastOnlineTimeRef.setValue(ServerValue.TIMESTAMP);
                    lastOnlineTimeRef.onDisconnect().setValue(ServerValue.TIMESTAMP);
                }
            }

            @Override
            public void onCancelled(DatabaseError error) {
                System.err.println("Listener was cancelled at .info/connected");
            }
        });
    }

    /**
     * Get user Info when the apps is launched.
     */
    protected void getUserInfo() {
        FirebaseDatabase database = FirebaseDatabase.getInstance();

        if (database == null || FirebaseUtils.getUserId() == null) {
            AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
            alertDialog.setTitle(R.string.sorry);
            alertDialog.setMessage(getResources().getString(R.string.check_network_try_again));
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            alertDialog.show();
            return;
        }

        DatabaseReference userReference = FirebaseUtils.getDatabaseReference().child("users").child(FirebaseUtils.getUserId());
        ValueEventListener userListener = userReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                User userData = snapshot.getValue(User.class);
                userData.key = snapshot.getKey();
                Log.e("user key", userData.key);
                if (userData != null) {
                    BaseDataManager.getInstance().setUser(userData);
                    mConversationsFragment.setUserProfile();
                }

                if (!userData.userProfile.isValid) {
                    deletedAccount();
                    return;
                }

                if (!mIsGotUserInfo) {
                    updateUserStatus();
                    mIsGotUserInfo = true;

                    if (checkPermissions()) {
                        startLocationUpdates();
                        mIsFirstStarted = true;
                    } else {
                        requestPermissions();
                    }

                    monitoringDiscoveryRules();

                    hideProgressDialog();
                }

                if (userData.discoveryRules.notifications)
                    getNotifications();
                else
                    stopNotifications();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Toast.makeText(getApplicationContext(), "Can't get user data!", Toast.LENGTH_LONG).show();
            }
        });

        BaseDataManager.putValueEventListener(userReference, userListener);
    }

    /**
     * Monitoring Nudge Messages.
     */
    private void monitoringNudgeMessages() {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        if (database == null)
            return;

        String userId = FirebaseUtils.getUserId();
        DatabaseReference databaseRef = FirebaseDatabase.getInstance().getReference();
        DatabaseReference nudgeRef = databaseRef.child("nudge").child(userId);
        ChildEventListener nudgeChildEventListener = nudgeRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                if (dataSnapshot.getValue() != null) {
                    NudgeMessage nudgeMessage = dataSnapshot.getValue(NudgeMessage.class);
                    nudgeMessage.key = dataSnapshot.getKey();
                    showNudgeToast(nudgeMessage);
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });

        BaseDataManager.putChildEventListener(nudgeRef, nudgeChildEventListener);
    }

    /**
     * Show Nudge Toast Message
     * @param nudgeMessage
     */
    private void showNudgeToast(NudgeMessage nudgeMessage) {
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.layout_map_message_notice,
                        (ViewGroup) findViewById(R.id.rootLayout));

        ImageView ivProfile = (ImageView) layout.findViewById(R.id.ivSenderPhoto);
        Glide.with(getApplicationContext())
                .load(nudgeMessage.photoUrl)
                .bitmapTransform(new CropCircleTransformation(getApplicationContext()))
                .into(ivProfile);

        TextView tvMessageTitle = (TextView) layout.findViewById(R.id.tvMessageTitle);
        tvMessageTitle.setText(nudgeMessage.title);

        TextView tvMessageText = (TextView) layout.findViewById(R.id.tvMessageText);
        tvMessageText.setText(nudgeMessage.message);

        //Set Theme
        layout.setBackgroundColor(getResources().getColor(ThemeManager.getInstance().getCurrentThemeColor()));

        final Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.TOP, 0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        toast.show();

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                toast.cancel();
            }
        }, 60000); // Change to what you want

        User me = BaseDataManager.getInstance().getUser();
        FirebaseDatabase.getInstance().getReference()
                .child("nudge").child(me.key).child(nudgeMessage.key).removeValue();
    }

    /**
     * Check if the conversation is blocked or not.
     */
    private void monitoringDiscoveryRules() {
        User myProfile = BaseDataManager.getInstance().getUser();
        DatabaseReference databaseRef = FirebaseUtils.getDatabaseReference();
        DatabaseReference rulesRef = databaseRef.child("users").child(myProfile.key).child("discoveryRules");
        ValueEventListener listener = rulesRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                if (snapshot != null) {
                    DiscoveryRules discoveryRules = snapshot.getValue(DiscoveryRules.class);
                    if (discoveryRules == null)
                        return;

                    if (mIsChangedRules) {
                        mGotDiscoveriesTimes = 0;
                        mCountryFilterTimes = 0;
                        if (mConversationsFragment != null)
                            mConversationsFragment.refreshDiscoveries();

                        if (mMapFragment != null)
                            mMapFragment.refreshMapLocation();

                        restartLocationUpdate();
                    }

                    mIsChangedRules = true;
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });

        BaseDataManager.putValueEventListener(databaseRef, listener);
    }

    /**
     * Get notifications
     */
    private void getNotifications() {
        if (mNotificationRef != null)
            return;

        String userId = FirebaseUtils.getUserId();
        DatabaseReference databaseRef = FirebaseDatabase.getInstance().getReference();
        mNotificationRef = databaseRef.child("notifications").child(userId);
        mNotificationChildEventListener = mNotificationRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                if (dataSnapshot.getValue() != null) {
                    Notification notification = dataSnapshot.getValue(Notification.class);
                    notification.key = dataSnapshot.getKey();
                    if (mNotificationsFragment != null)
                        mNotificationsFragment.addNotification(notification);

                    if (!notification.isRead) {
                        if (vpViewPager.getCurrentItem() != 2) {//Current is not Notifications page!
                            BaseDataManager.addNewNotification();
                            tvNotificationsBadge.setVisibility(View.VISIBLE);
                            tvNotificationsBadge.setText(String.valueOf(BaseDataManager.getInstance().getNewNotifications()));
                        } else {
                            tvNotificationsBadge.setVisibility(View.INVISIBLE);
                        }
                    }
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                //Because this part is called only when all notifications is changed as a read status!
                if (tvNotificationsBadge.getVisibility() == View.VISIBLE)
                    tvNotificationsBadge.setVisibility(View.GONE);
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }

    /**
     * Stop receiving notifications when you disable notification on settings page.
     */
    private void stopNotifications() {
        if (mNotificationRef != null && mNotificationChildEventListener != null) {
            mNotificationRef.removeEventListener(mNotificationChildEventListener);
            mNotificationRef = null;
            mNotificationChildEventListener = null;
        }

        tvNotificationsBadge.setVisibility(View.GONE);
        if (mNotificationsFragment != null)
            mNotificationsFragment.hideAllNotifications();
    }

    //[START - Building Location service]
    private void createLocationService() {
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mSettingsClient = LocationServices.getSettingsClient(this);

        // Creates a callback for receiving location events.
        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);

                mCurrentLocation = locationResult.getLastLocation();

                getNearUsers();

                User me = BaseDataManager.getInstance().getUser();
                if (me != null) {
                    if (me.discoveryRules.discoveryMode == COUNTRY_DISCOVERY_MODE)
                        getDiscoveriesByCountryFilter();
                }

                if (mMapFragment != null)
                    mMapFragment.updateUserLocation(locationResult.getLastLocation());
            }
        };

        //[START-- Create Location Request--]
        mLocationRequest = new LocationRequest();
        // Sets the desired interval for active location updates. This interval is
        // inexact. You may not receive updates at all if no location sources are available, or
        // you may receive them slower than requested. You may also receive updates faster than
        // requested if other applications are requesting location at a faster interval.
        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
        // Sets the fastest rate for active location updates. This interval is exact, and your
        // application will never receive updates faster than this value.
        mLocationRequest.setFastestInterval(AppConfig.Location.FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        //[END-- Create Location Request--]

        //Building Settings Request!
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        mLocationSettingsRequest = builder.build();
    }

    /**
     * Stop Location update
     */
    private void stopLocationUpdates() {
        if (!mRequestingLocationUpdates) {
            Log.d(TAG, "stopLocationUpdates: updates never requested, no-op.");
            return;
        }

        mFusedLocationClient.removeLocationUpdates(mLocationCallback);
        // It is a good practice to remove location requests when the activity is in a paused or
        // stopped state. Doing so helps battery performance and is especially
        // recommended in applications that request frequent location updates.
        mFusedLocationClient.removeLocationUpdates(mLocationCallback)
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        mRequestingLocationUpdates = false;
                    }
                });
    }

    /**
     * Restart Location update
     */
    private void restartLocationUpdate() {
        if (!mRequestingLocationUpdates) {
            Log.d(TAG, "stopLocationUpdates: updates never requested, no-op.");
            return;
        }

        //Now, I'm not sure why it works so, but it should be called twice.
        mFusedLocationClient.removeLocationUpdates(mLocationCallback);
        // It is a good practice to remove location requests when the activity is in a paused or
        // stopped state. Doing so helps battery performance and is especially
        // recommended in applications that request frequent location updates.
        mFusedLocationClient.removeLocationUpdates(mLocationCallback)
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        mRequestingLocationUpdates = true;

                        startLocationUpdates();
                    }

                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.e(TAG, e.getLocalizedMessage());
            }
        });
    }

    /**
     * Requests location updates from the FusedLocationApi. Note: we don't call this unless location
     * runtime permission has been granted.
     */
    private void startLocationUpdates() {

        // Begin by checking if the device has the necessary location settings.
        mSettingsClient.checkLocationSettings(mLocationSettingsRequest)
                .addOnSuccessListener(this, new OnSuccessListener<LocationSettingsResponse>() {
                    @Override
                    public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                        Log.i(TAG, "All location settings are satisfied.");

                        try {
                            //noinspection MissingPermission
                            mFusedLocationClient.requestLocationUpdates(mLocationRequest,
                                    mLocationCallback, Looper.myLooper()).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    mRequestingLocationUpdates = true;
                                }
                            });

                        } catch (SecurityException e) {
                            e.printStackTrace();
                        }
                    }
                })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        int statusCode = ((ApiException) e).getStatusCode();
                        switch (statusCode) {
                            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                                Log.i(TAG, "Location settings are not satisfied. Attempting to upgrade " +
                                        "location settings ");
                                try {
                                    // Show the dialog by calling startResolutionForResult(), and check the
                                    // result in onActivityResult().
                                    ResolvableApiException rae = (ResolvableApiException) e;
                                    rae.startResolutionForResult(MainActivity.this, REQUEST_CHECK_SETTINGS);
                                } catch (IntentSender.SendIntentException sie) {
                                    Log.i(TAG, "PendingIntent unable to execute request.");
                                }
                                break;
                            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                                String errorMessage = "Location settings are inadequate, and cannot be " +
                                        "fixed here. Fix in Settings.";
                                Log.e(TAG, errorMessage);
                                Toast.makeText(MainActivity.this, errorMessage, Toast.LENGTH_LONG).show();
                                mRequestingLocationUpdates = false;
                        }

                    }
                });
    }

    private boolean checkPermissions() {
        int permissionState = ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION);
        return permissionState == PackageManager.PERMISSION_GRANTED;

         //!!!!!The below code causes infinite loop in OnResume
        // return permissionState == PackageManager.PERMISSION_GRANTED &&
        //  ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) ;
    }

    private void requestPermissions() {
        boolean shouldProvideRationale =
                ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.ACCESS_FINE_LOCATION);

        // Provide an additional rationale to the user. This would happen if the user denied the
        // request previously, but didn't check the "Don't ask again" checkbox.
        if (shouldProvideRationale) {
            Log.i(TAG, "Displaying permission rationale to provide additional context.");
            showSnackbar(R.string.permission_rationale,
                    android.R.string.ok, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            // Request permission
                            ActivityCompat.requestPermissions(MainActivity.this,
                                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                    REQUEST_PERMISSIONS_REQUEST_CODE);
                        }
                    });
        } else {
            Log.i(TAG, "Requesting permission");
            // Request permission. It's possible this can be auto answered if device policy
            // sets the permission in a given state or the user denied the permission
            // previously and checked "Never ask again".
            ActivityCompat.requestPermissions(MainActivity.this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_PERMISSIONS_REQUEST_CODE);
        }
    }

    /**
     * Callback received when a permissions request has been completed.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {

        if (requestCode == REQUEST_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.length <= 0) {
                // If user interaction was interrupted, the permission request is cancelled and you
                // receive empty arrays.
                Log.e(TAG, "User interaction was cancelled.");
            } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //if (mRequestingLocationUpdates) {
                    Log.e(TAG, "Permission granted, updates requested, starting location updates");
                    startLocationUpdates();
                //}
            } else {
                // Permission denied.

                // Notify the user via a SnackBar that they have rejected a core permission for the
                // app, which makes the Activity useless. In a real app, core permissions would
                // typically be best requested during a welcome-screen flow.

                // Additionally, it is important to remember that a permission might have been
                // rejected without asking the user for permission (device policy or "Never ask
                // again" prompts). Therefore, a user interface affordance is typically implemented
                // when permissions are denied. Otherwise, your app could appear unresponsive to
                // touches or interactions which have required permissions.
                showSnackbar(R.string.permission_denied_explanation,
                        R.string.settings, new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                // Build intent that displays the App settings screen.
                                Intent intent = new Intent();
                                intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                Uri uri = Uri.fromParts("package", BuildConfig.APPLICATION_ID, null);
                                intent.setData(uri);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                            }
                        });
            }
        }
    }

    /**
     * Shows a {@link Snackbar}.
     *
     * @param mainTextStringId The id for the string resource for the Snackbar text.
     * @param actionStringId   The text of the action item.
     * @param listener         The listener associated with the Snackbar action.
     */
    private void showSnackbar(final int mainTextStringId, final int actionStringId,
                              View.OnClickListener listener) {
        Snackbar.make(
                findViewById(android.R.id.content),
                getString(mainTextStringId),
                Snackbar.LENGTH_INDEFINITE)
                .setAction(getString(actionStringId), listener).show();
    }
    //[END - Building Location service]


    /**
     * Get Discovery Range value
     * @return discovery range by km
     */
    private double getDiscoveryRangeValue() {
        User myProfile = BaseDataManager.getInstance().getUser();
        if (myProfile == null)
            return 0;

        String[] rangeArray = getResources().getStringArray(R.array.discovery_range_array);
        String range = rangeArray[myProfile.discoveryRules.discoveryRange];

        if (range.equals(getString(R.string.no_limit)))
            return AppConfig.Location.DISCOVERY_FINDING_RADIUS;

        range = range.replace("km", "");

        return Double.valueOf(range);
    }

    /**
     * Get users near by.
     */
    private void getNearUsers() {
        //Remove the old query.
        Log.e("1", "get --------Near------------- Users");
        mCount = 0;
        BaseDataManager.getInstance().removeGeoQuery();
        if (mMapFragment != null)
            mMapFragment.refreshMapLocation();

        if (mConversationsFragment != null && mGotDiscoveriesTimes == 0)
            mConversationsFragment.removeAllDiscoveries();

        if (mGotDiscoveriesTimes > MAX_GET_DISCOVERIES_SKIP_TIME)
            mGotDiscoveriesTimes = 0; //reset.

        mGotDiscoveriesTimes++;

        User me = BaseDataManager.getInstance().getUser();
        double rangeRadius = NEAR_FINDING_RADIUS;
        //First time, get all discoveries according to discovery rules.
        //However, it wouldn't observe anymore because of performance.
        //After then, getting near users on map within 1km.
        if (mGotDiscoveriesTimes == 1 && me.discoveryRules.discoveryMode == RANGE_DISCOVERY_MODE) {
            rangeRadius =  getDiscoveryRangeValue();
        }

        GeoFire geoFire = FirebaseUtils.getGeoFire();
        GeoQuery geoQuery = geoFire.queryAtLocation(BaseUtils.getGeoLocationFromLocation(mCurrentLocation),
                rangeRadius);


        BaseDataManager.getInstance().setGeoQuery(geoQuery); //For remove all event listener.
        geoQuery.addGeoQueryEventListener(new GeoQueryEventListener() {
            @Override
            public void onKeyEntered(String key, GeoLocation location) {
                //System.out.println(String.format("Key %s entered the search area at [%f,%f]", key, location.latitude, location.longitude));
                String userId = FirebaseUtils.getUserId();
                if (TextUtils.isEmpty(userId))
                    return;

                if (key.equals(userId))
                    return;

                mCount ++;
                Log.e("-Founded--", String.valueOf(mCount));

                getUserWithKey(key, location);
            }

            @Override
            public void onKeyExited(String key) {
                //System.out.println(String.format("Key %s is no longer in the search area", key));
            }

            @Override
            public void onKeyMoved(String key, GeoLocation location) {
                //System.out.println(String.format("Key %s moved within the search area to [%f,%f]", key, location.latitude, location.longitude));
            }

            @Override
            public void onGeoQueryReady() {
                //System.out.println("All initial data has been loaded and events have been fired!");
            }

            @Override
            public void onGeoQueryError(DatabaseError error) {
                System.err.println("There was an error with this query: " + error);
            }
        });
    }

    /**
     * Get users with Country filter
     */
    private void getDiscoveriesByCountryFilter() {
        if (mCountryFilterTimes > 0)
            return;

        if (mConversationsFragment != null)
            mConversationsFragment.removeAllDiscoveries();

        mCountryFilterTimes++;
        if (mCountryFilterTimes > MAX_GET_DISCOVERIES_SKIP_TIME)
            mCountryFilterTimes = 0; //reset.

        final User me = BaseDataManager.getInstance().getUser();

        DatabaseReference userRef = FirebaseUtils.getDatabaseReference().child("users");
        Query query = userRef.orderByChild("countryCode").equalTo(me.discoveryRules.countryFilter);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                if (snapshot != null) {
                    for (DataSnapshot userSnapshot:snapshot.getChildren()) {
                        User user = userSnapshot.getValue(User.class);
                        user.key = userSnapshot.getKey();
                        if (user == null)
                            continue;

                        if (!BaseDataManager.checkValidUser(user, MainActivity.this))
                            continue;

                        if (user.key.equals(me.key))
                            continue;

                        if (mConversationsFragment != null)
                            mConversationsFragment.addDiscovery(user, mCurrentLocation);
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }

    /**
     * Get user with key
     * @param key
     */
    private void getUserWithKey(final String key, final GeoLocation location) {

        FirebaseDatabase.getInstance().getReference().child("users").child(key)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot snapshot) {
                        if (snapshot == null)
                            return;

                        User userData = snapshot.getValue(User.class);
                        if (userData == null)
                            return;

                        userData.key = snapshot.getKey();

                        if (!BaseDataManager.checkValidUser(userData, MainActivity.this))
                            return;

                        User me = BaseDataManager.getInstance().getUser();
                        if (mConversationsFragment != null && mGotDiscoveriesTimes == 1 &&
                                me.discoveryRules.discoveryMode == RANGE_DISCOVERY_MODE ) {
                            Location userLocation = new Location("User Location");
                            userLocation.setLatitude(location.latitude);
                            userLocation.setLongitude(location.longitude);

                            mConversationsFragment.addDiscovery(userData, userLocation, mCurrentLocation);
                        }

                        if (mMapFragment != null)
                            mMapFragment.addNearUser(userData, location);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Toast.makeText(getActivity(), "Can't get user data!", Toast.LENGTH_LONG).show();
                    }
                });

    }

    /**
     * Go to Map screen
     */
    public void gotoMapFragment() {
        setCurrentTab(TAB_MAP);
    }

    @OnClick(R.id.llChatsLayout)
    public void onClickedChats() {
        setCurrentTab(TAB_CONVERSATION);
    }

    @OnClick(R.id.llMapLayout)
    public void onClickedMap() {
        setCurrentTab(TAB_MAP);
   }

    @OnClick(R.id.llNotificationsLayout)
    public void onClickedNotifications() {
        setCurrentTab(TAB_NOTIFICATION);
    }

    @OnClick(R.id.llSettingsLayout)
    public void onClickedSettings() {
        setCurrentTab(TAB_SETTINGS);
    }

    //[START - Control Tab]
    private void setCurrentTab(int index) {
        for (int i = 0; i < mTabItems.size(); i ++) {
            TabItemHolder tabItemHolder = mTabItems.get(i);
            if (i == index) {
                tabItemHolder.setActive(true);
            } else {
                tabItemHolder.setActive(false);
            }
        }

        vpViewPager.setCurrentItem(index);
        switch (index) {
            case TAB_CONVERSATION:
                break;
            case TAB_MAP:
                mMapFragment.setLoadingSplash();
                break;
            case TAB_NOTIFICATION:
                mNotificationsFragment.checkNewNotifications();
                tvNotificationsBadge.setVisibility(View.INVISIBLE);
                break;
            case TAB_SETTINGS:
                mSettingsFragment.setUI();
                break;
        }
    }

    /**
     * Set Tab layout
     */
    private void setTabItems() {
        TabItemHolder chatItem = new TabItemHolder();
        chatItem.mTitleTextView = tvChatsTitle;
        chatItem.mIconImageView = ivChatsItem;
        chatItem.mActiveIconResId = ThemeManager.getConversationIcon();
        chatItem.mInactiveIconResId = R.drawable.chats_inactive;
        mTabItems.add(chatItem);

        TabItemHolder mapItem = new TabItemHolder();
        mapItem.mTitleTextView = tvMapTitle;
        mapItem.mIconImageView = ivMapItem;
        mapItem.mActiveIconResId = ThemeManager.getMapIcon();
        mapItem.mInactiveIconResId = R.drawable.map_inactive;
        mTabItems.add(mapItem);

        TabItemHolder notiItem = new TabItemHolder();
        notiItem.mTitleTextView = tvNotificationsTitle;
        notiItem.mIconImageView = ivNotificationsItem;
        notiItem.mActiveIconResId = ThemeManager.getNotificationIcon();
        notiItem.mInactiveIconResId = R.drawable.notifications_inactive;
        mTabItems.add(notiItem);

        TabItemHolder settingsItem = new TabItemHolder();
        settingsItem.mTitleTextView = tvSettingsTitle;
        settingsItem.mIconImageView = ivSettingsItem;
        settingsItem.mActiveIconResId = ThemeManager.getSettingsIcon();
        settingsItem.mInactiveIconResId = R.drawable.settings_inactive;
        mTabItems.add(settingsItem);
    }

    class TabItemHolder {
        ImageView mIconImageView;
        TextView  mTitleTextView;
        int mActiveIconResId;
        int mInactiveIconResId;
        boolean mIsActive;

        TabItemHolder() {
        }

        void setActive(boolean isActive) {
            mIsActive = isActive;
            if (mIsActive) {
                mIconImageView.setImageResource(mActiveIconResId);
                mTitleTextView.setTextColor(getResources().getColor(ThemeManager.getInstance().getCurrentThemeColor()));
            }  else {
                mIconImageView.setImageResource(mInactiveIconResId);
                mTitleTextView.setTextColor(getResources().getColor(R.color.normalTextGrayColor));
            }
        }
    }
    //[END - Control Tab]
}
