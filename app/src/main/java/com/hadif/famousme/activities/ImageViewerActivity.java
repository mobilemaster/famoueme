package com.hadif.famousme.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.hadif.famousme.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ImageViewerActivity extends AppCompatActivity {

    public static void start(Context context, String photoUrl) {
        Intent intent = new Intent(context, ImageViewerActivity.class);
        intent.putExtra("photoUrl", photoUrl);

        context.startActivity(intent);
    }


    @BindView(R.id.ivFullImagePreview)      ImageView ivFullImagePreview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_image_viewer);
        ButterKnife.bind(this);

        String photoUrl = getIntent().getStringExtra("photoUrl");
        Glide.with(ImageViewerActivity.this).load(photoUrl)
                .fitCenter()
                .into(ivFullImagePreview);
    }

    @OnClick(R.id.ivFullImagePreview)
    public void OnImageClicked() {
        finish();
    }
}
