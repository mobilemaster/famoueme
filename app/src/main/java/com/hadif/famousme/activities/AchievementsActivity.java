package com.hadif.famousme.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.hadif.famousme.R;
import com.hadif.famousme.adapters.AchievementsAdapter;
import com.hadif.famousme.managers.basedata.ThemeManager;
import com.hadif.famousme.models.Achievement;
import com.hadif.famousme.models.BaseData;
import com.hadif.famousme.utils.FirebaseUtils;
import com.views.progressbar.SMProgressBar;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AchievementsActivity extends BaseActivity {

    public static void start (Context context) {
        Intent intent = new Intent(context, AchievementsActivity.class);
        context.startActivity(intent);
    }

    //UI controls
    @BindView(R.id.rlHeader)                RelativeLayout rlHeader;
    @BindView(R.id.lvAchievementList)       ListView lvAchievementList;
    @BindView(R.id.prAchievementValue)      SMProgressBar prAchievementValue;

    //Private Members
    AchievementsAdapter mAchievementsAdapter;
    ArrayList<Achievement> mAchievementsItems = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(ThemeManager.getInstance().getCurrentThemeStyle());
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_achievements);

        ButterKnife.bind(this);

        applyTheme();

        getAchievements();
    }

    private void applyTheme() {
        ThemeManager.getInstance().setThemeWithChildViews(rlHeader);
    }

    /**
     * Set UI
     */
    private void setUI() {
        mAchievementsAdapter = new AchievementsAdapter(AchievementsActivity.this);
        lvAchievementList.setAdapter(mAchievementsAdapter);
        mAchievementsAdapter.setData(mAchievementsItems);

            int achievementValue = 0;
            for (Achievement achievement:mAchievementsItems) {
                achievementValue = achievementValue + achievement.value;
            }

            prAchievementValue.setValue(achievementValue);
    }

    /**
     * Get Achievements
     */
    private void getAchievements() {
        FirebaseDatabase.getInstance().getReference().child("achievements").child(FirebaseUtils.getUserId())
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot snapshot) {
                        if (snapshot != null) {
                            for (DataSnapshot dataSnapshot: snapshot.getChildren()) {
                                Achievement achievement = dataSnapshot.getValue(Achievement.class);
                                mAchievementsItems.add(achievement);
                            }

                            setUI();
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Toast.makeText(getApplicationContext(), "Can't get data!", Toast.LENGTH_LONG).show();
                    }
                });
    }


    @OnClick(R.id.btnBack)
    public void onBack() {
        finish();
    }
}
