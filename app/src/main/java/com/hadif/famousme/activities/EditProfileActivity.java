package com.hadif.famousme.activities;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.content.FileProvider;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.bumptech.glide.signature.StringSignature;
import com.filters.FilterHelper;
import com.filters.IFInkwellFilter;
import com.filters.InstaFilter;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.hadif.famousme.AppConfig;
import com.hadif.famousme.R;
import com.hadif.famousme.adapters.InterestsAdapter;
import com.hadif.famousme.managers.basedata.BaseDataManager;
import com.hadif.famousme.managers.basedata.ThemeManager;
import com.hadif.famousme.models.InterestItem;
import com.hadif.famousme.models.User;
import com.hadif.famousme.utils.BaseUtils;
import com.hadif.famousme.utils.FirebaseUtils;
import com.hadif.famousme.utils.ImageUtils;
import com.hadif.famousme.utils.PrefsUtils;
import com.hadif.famousme.utils.Utility;
import com.squareup.picasso.Picasso;
import com.views.imageview.CircleImageView;
import com.views.imageview.CircularImageView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import jp.co.cyberagent.android.gpuimage.GPUImageFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageView;
import jp.wasabeef.glide.transformations.CropCircleTransformation;
import jp.wasabeef.glide.transformations.GrayscaleTransformation;
import jp.wasabeef.glide.transformations.gpu.BrightnessFilterTransformation;
import jp.wasabeef.glide.transformations.gpu.ContrastFilterTransformation;
import jp.wasabeef.glide.transformations.gpu.SepiaFilterTransformation;

import static com.hadif.famousme.AppConfig.GPU_IMAGE_FOLDER;
import static com.hadif.famousme.utils.BaseUtils.deleteRecursive;

public class EditProfileActivity extends BaseActivity implements GPUImageView.OnPictureSavedListener {

    public static void start (Context context) {
        Intent intent = new Intent(context, EditProfileActivity.class);
        context.startActivity(intent);
    }

    //UI control
    @BindView(R.id.rlHeader)                RelativeLayout rlHeader;
    @BindView(R.id.btnBack)                 ImageView btnBack;
    @BindView(R.id.tvTitle)                 TextView tvTitle;
    @BindView(R.id.btnControl)              TextView btnControl;

    /** Step 1 */
    @BindView(R.id.rlFirstStep)             RelativeLayout rlFirstStep;
    @BindView(R.id.etFullName)              EditText etFullName;
    @BindView(R.id.ivProfile)               CircularImageView ivProfile;
    @BindView(R.id.ivCloseImage)            ImageView ivCloseImage;

    //Filtering
    @BindView(R.id.vgAddFilterGroup)        View vgAddFilterGroup;
    @BindView(R.id.ivFilter1)               ImageView ivFilter1;
    @BindView(R.id.ivFilter2)               ImageView ivFilter2;
    @BindView(R.id.ivFilter3)               ImageView ivFilter3;
    @BindView(R.id.ivFilter4)               ImageView ivFilter4;
    @BindView(R.id.ivFilter5)               ImageView ivFilter5;
    @BindView(R.id.ivFilter6)               ImageView ivFilter6;
    @BindView(R.id.ivFilter7)               ImageView ivFilter7;
    @BindView(R.id.ivFilter8)               ImageView ivFilter8;
    @BindView(R.id.tvAddFilter)             TextView tvAddFilter;
    @BindView(R.id.tvFilterCancel)          TextView tvFilterCancel;
    @BindView(R.id.gpuImageView)            GPUImageView gpuImageView;

    /** Step 2 */
    @BindView(R.id.rlSecondStep)            LinearLayout rlSecondStep;
    @BindView(R.id.spSelectGender)          Spinner spSelectGender;
    @BindView(R.id.tvGender)                TextView tvGender;
    @BindView(R.id.spSelectNationality)     Spinner spSelectNationality;
    @BindView(R.id.tvSelectBirthDate)       TextView tvSelectBirthDate;
    @BindView(R.id.etProfession)            EditText etProfession;
    @BindView(R.id.etEmailAddress)          EditText etEmailAddress;

    /** Step 3 */
    @BindView(R.id.rlThirdStep)             LinearLayout rlThirdStep;
    @BindView(R.id.lvInterestList)          ListView lvInterestList;

    InterestsAdapter mInterestsAdapter = null;
    ArrayList<InterestItem> mInterestItems = new ArrayList<>();

    //Private Member
    private static final String TAG = "EditProfileActivity";
    private int mCurrentIndex = 1;
    private int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    private String mUserChoosenTask;
    private Uri mPhotoUri = null;

    private User mUser = null;
    private ArrayList<String> mCurrentInterestList = new ArrayList<>();

    //Filtering
    private ArrayList<Uri> mFilteredImageList = new ArrayList<>();
    private ArrayList<ImageView> mFilterViewList = new ArrayList<>();
    private int mFilterIndex = -1;
    private Bitmap mOrgBitMap = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(ThemeManager.getInstance().getCurrentThemeStyle());
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setup_profile);

        ButterKnife.bind(this);

        applyTheme();

        setCurrentStep();

        mUser = new User();

        mFilterViewList.add(ivFilter1);
        mFilterViewList.add(ivFilter2);
        mFilterViewList.add(ivFilter3);
        mFilterViewList.add(ivFilter4);
        mFilterViewList.add(ivFilter5);
        mFilterViewList.add(ivFilter6);
        mFilterViewList.add(ivFilter7);
        mFilterViewList.add(ivFilter8);
        for (int i = 0; i < mFilterViewList.size(); i++) {
            ImageView imageView = mFilterViewList.get(i);
            imageView.setTag(imageView.getId(), String.valueOf(i));
            imageView.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    onSelectFilter(Integer.parseInt((String) v.getTag(v.getId())));
                }
            });
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case Utility.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if(mUserChoosenTask.equals("Take Photo"))
                        cameraIntent();
                    else if(mUserChoosenTask.equals("Choose from Library"))
                        galleryIntent();
                } else {
                    //code for deny
                }
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);
            else if (requestCode == REQUEST_CAMERA)
                onCaptureImageResult(data);
        }
    }

    private void applyTheme() {
        ThemeManager.getInstance().setThemeWithChildViews(rlHeader);
        ThemeManager.getInstance().setTheme(btnControl);
    }

    //---------------------- Step 1 Start ------------------------/

    /**
     * Set first step UI with the old user's data
     */
    private void setFirstStepUI() {
        User currentUser = BaseDataManager.getInstance().getUser();
        if (currentUser == null)
            return;

        Glide.with(this).load(currentUser.userProfile.photoUrl)
                .bitmapTransform(new CropCircleTransformation(this))
                .into(ivProfile);

        etFullName.setText(currentUser.userProfile.username);
    }

    /**
     * Get gallery Intent
     */
    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
    }

    /**
     * Get Camera Intent
     */
    private void cameraIntent() {
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (cameraIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = ImageUtils.createImageFile(EditProfileActivity.this);
            } catch (IOException ex) {
                Log.i(TAG, "IOException");
            }

            //For Android SDk 24+, 7.0
            if (photoFile != null) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    mPhotoUri = FileProvider.getUriForFile(this, getPackageName() + ".provider", photoFile);

                    List<ResolveInfo> resInfoList = getPackageManager().queryIntentActivities(cameraIntent, PackageManager.MATCH_DEFAULT_ONLY);
                    for (ResolveInfo resolveInfo : resInfoList) {
                        String packageName = resolveInfo.activityInfo.packageName;
                        grantUriPermission(packageName, mPhotoUri, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    }
                } else {
                    mPhotoUri = Uri.fromFile(photoFile);
                }

                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, mPhotoUri);
                startActivityForResult(cameraIntent, REQUEST_CAMERA);
            }
        }
    }

    /**
     * Get the result from camera
     * @param data intent data
     */
    private void onCaptureImageResult(Intent data) {
        if (mPhotoUri == null) {
            Toast.makeText(getApplicationContext(), R.string.failed_get_photo, Toast.LENGTH_LONG).show();
            return;
        }
        Glide.with(this).load(mPhotoUri)
                .bitmapTransform(new CropCircleTransformation(EditProfileActivity.this))
                .signature(new StringSignature(String.valueOf(System.currentTimeMillis())))
                .into(ivProfile);

        prepareFiltering(true);
    }


    /**
     * Get the result from gallery
     * @param data intent data
     */
    private void onSelectFromGalleryResult(Intent data) {
        if (data != null) {
            Uri selectedImage = data.getData();
            Glide.with(this).load(selectedImage)
                    .bitmapTransform(new CropCircleTransformation(EditProfileActivity.this))
                    .signature(new StringSignature(String.valueOf(System.currentTimeMillis())))
                    .into(ivProfile);

            mPhotoUri = data.getData();

            prepareFiltering(false);
        }
    }

    /**
     * Set filter to GPUImageView in sequence.
     */
    private void setFilter() {
        try {
            InstaFilter filter = FilterHelper.getFilter(EditProfileActivity.this, mFilteredImageList.size());
            if (filter != null) {
                gpuImageView.setFilter(filter);
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    /**
     * Initializing for filtering.
     * @param isCamera  From Camera or Gallery
     */
    private void prepareFiltering(boolean isCamera) {
        vgAddFilterGroup.setVisibility(View.VISIBLE);
        etFullName.setVisibility(View.INVISIBLE);
        FilterHelper.destroyFilters();
        mFilteredImageList.clear();
        try {
            mOrgBitMap = ImageUtils.decodeUri(mPhotoUri, isCamera, AppConfig.IMAGE_REQUIRED_SIZE,EditProfileActivity.this);
            getFilteredImages(mOrgBitMap);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * Get a filtered image on GPUImageView from original Bitmap
     * @param bitmap original bitmap
     */
    private void getFilteredImages(Bitmap bitmap) {
        if (bitmap == null)
            return;

        if (mFilteredImageList.size() == 0)
            deleteFilteredImages();

        if (mFilteredImageList.size() >= mFilterViewList.size()) {
            //Initializing GPUImage to prevent overriding images.
            FilterHelper.destroyFilters();
            gpuImageView.getGPUImage().deleteImage();
            return;
        }

        gpuImageView.getLayoutParams().width = bitmap.getWidth();
        gpuImageView.getLayoutParams().height = bitmap.getHeight();
        gpuImageView.requestLayout();

        setFilter();

        gpuImageView.setImage(bitmap);

        try {
            String fileName = String.valueOf(mFilteredImageList.size()) + ".jpg";
            gpuImageView.saveToPictures(GPU_IMAGE_FOLDER, fileName, bitmap.getWidth(), bitmap.getHeight(),this);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Callback after saving captured image on GPUImageView.
     * @param uri the uri of the filtered image that was saved.
     */
    public void onPictureSaved(Uri uri) {
        try {
            mFilteredImageList.add(uri);
            ImageView imageView = mFilterViewList.get(mFilteredImageList.size() - 1);
            Glide.with(this).load(uri)
                    .bitmapTransform(new CropCircleTransformation(this))
                    .signature(new StringSignature(String.valueOf(System.currentTimeMillis())))
                    .into(imageView);

            getFilteredImages(mOrgBitMap);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Deleting temporary files for filtering.
     */
    private void deleteFilteredImages() {
        File path = Environment
                .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);

        File dir = new File(path, GPU_IMAGE_FOLDER);
        deleteRecursive(dir);
    }

    /**
     * Select filtered preview image.
     * @param index the index of filtering.
     */
    private void onSelectFilter(int index) {
        if (index > mFilteredImageList.size() - 1)
            return;

        mFilterIndex = index;
        for (int i = 0; i < mFilterViewList.size(); i++) {
            CircleImageView imageView = (CircleImageView) mFilterViewList.get(i);
            if (index == i) {
                imageView.setBorderWidth(8);
                imageView.setBorderColor(getResources().getColor(R.color.mainColor));
            } else {
                imageView.setBorderWidth(0);
            }
        }

        Uri filterUri = mFilteredImageList.get(index);
        Glide.with(this).load(filterUri)
                .bitmapTransform(new CropCircleTransformation(this))
                .signature(new StringSignature(String.valueOf(System.currentTimeMillis())))
                .into(ivProfile);
    }

    /**
     * Apply filter.
     */
    private void addFilter() {
        if (mFilterIndex < 0 || mFilterIndex >= mFilteredImageList.size()) {
            return;
        }

        vgAddFilterGroup.setVisibility(View.GONE);
        etFullName.setVisibility(View.VISIBLE);
        clearFilterImageBorder();

        mPhotoUri = mFilteredImageList.get(mFilterIndex);
        mFilterIndex = -1;
    }
    /**
     * Cancel filter.
     */
    private void cancelFilter() {
        vgAddFilterGroup.setVisibility(View.GONE);
        etFullName.setVisibility(View.VISIBLE);
        clearFilterImageBorder();

        Glide.with(this).load(mPhotoUri)
                .bitmapTransform(new CropCircleTransformation(this))
                .signature(new StringSignature(String.valueOf(System.currentTimeMillis())))
                .into(ivProfile);

        mFilterIndex = -1;
    }

    /**
     * Remove border preview filtering images.
     */
    private void clearFilterImageBorder() {
        for (int i = 0; i < mFilterViewList.size(); i++) {
            CircleImageView imageView = (CircleImageView) mFilterViewList.get(i);
            imageView.setBorderWidth(0);
        }
    }

    @OnClick(R.id.tvAddFilter)
    public void onAddFilter() {
        //addFilter();
    }

    @OnClick(R.id.tvFilterCancel)
    public void onCancelFilter() {
        cancelFilter();
    }

    /**
     * Check if the first step has completed or not?
     * @return
     */
    private boolean isValidationFirstStep() {
        if (!BaseUtils.isValidUserString(etFullName.getText().toString())) {
            etFullName.setError(getString(R.string.can_not_empty));
            return false;
        }

        addFilter();

        if (mPhotoUri == null) {
            //Toast.makeText(getApplicationContext(), "You should add an image", Toast.LENGTH_LONG).show();
            User currentUser = BaseDataManager.getInstance().getUser();
            mUser.userProfile.photoUrl = currentUser.userProfile.photoUrl;
            mUser.userProfile.avatarUrl = currentUser.userProfile.avatarUrl;
            return true;
        }

        return true;
    }

    /**
     * Select where you would get the photo from.
     */
    private void selectImageSource() {
        //deleteFilteredImages();
        final CharSequence[] items = { getString(R.string.take_photo), getString(R.string.choose_from_libaray), getString(R.string.cancel)};
        AlertDialog.Builder builder = new AlertDialog.Builder(EditProfileActivity.this);
        builder.setTitle(getString(R.string.add_photo_title));
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals(getString(R.string.cancel))) {
                    dialog.dismiss();
                    return;
                }

                boolean result = Utility.checkPermission(EditProfileActivity.this);
                if (items[item].equals(getString(R.string.take_photo))) {
                    mUserChoosenTask = "Take Photo";
                    if (result)
                        cameraIntent();
                } else if (items[item].equals(getString(R.string.choose_from_libaray))) {
                    mUserChoosenTask = "Choose from Library";
                    if (result)
                        galleryIntent();
                }
            }
        });
        builder.show();
    }

    @OnClick(R.id.ivCloseImage)
    public void onClosePhotoClicked() {
        selectImageSource();
    }

    @OnClick(R.id.rlAddImageGroup)
    public void onAddImage() {
        selectImageSource();
    }

    //---------------------- Step 1 End ------------------------/

    //---------------------- Step 2 Start ------------------------/
    private void setSecondStepUI() {
        User currentUser = BaseDataManager.getInstance().getUser();

        //Set Gender
        if (currentUser.gender == 0)
            tvGender.setText(R.string.male);
        else
            tvGender.setText(R.string.female);

        //Set Birthday
        tvSelectBirthDate.setText(currentUser.dateOfBirth);

        //Set Nationality
        ArrayList<String> nationalityList =  new ArrayList<String>(BaseDataManager.getInstance().getNationalityList());
        ArrayAdapter<String> nationalityAdapter = new ArrayAdapter<String>(
                this, android.R.layout.simple_spinner_dropdown_item, nationalityList);
        spSelectNationality.setAdapter(nationalityAdapter);
        int nationalityIndex = BaseDataManager.getNationalityIndex(currentUser.nationality);
        if (nationalityIndex >= 0)
            spSelectNationality.setSelection(nationalityIndex); //All item is added.

        //Profession
        etProfession.setText(currentUser.userProfile.profession);

        //Email Address
        etEmailAddress.setText(currentUser.emailAddress);
    }

    @OnClick(R.id.tvSelectBirthDate)
    public void onSelectDateOfBirth() {
        Calendar calendar = Calendar.getInstance();
        DatePickerDialog datePicker = new DatePickerDialog(this, datePickerListener,
                calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH));

        datePicker.getDatePicker().getTouchables().get(0).performClick();
        datePicker.show();
    }

    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {
            tvSelectBirthDate.setText(selectedDay + " / " + (selectedMonth + 1) + " / "
                    + selectedYear);
        }
    };

    /**
     * Check the second's data.
     * @return boolean
     */
    private boolean isValidationSecondStep() {
        if (TextUtils.isEmpty(tvSelectBirthDate.getText().toString())) {
            tvSelectBirthDate.setError("The date of birth is needed!");
            return false;
        }

        if (TextUtils.isEmpty(etProfession.getText())) {
            etProfession.setError(getString(R.string.can_not_empty));
            return false;
        }

        if (!BaseUtils.isValidEmail(etEmailAddress.getText())) {
            etEmailAddress.setError(getString(R.string.can_not_empty));
            return false;
        }

        return true;
    }
    //---------------------- Step 2 End --------------------------/

    //---------------------- Step 3 Start ------------------------/

    /**
     * Set Third Step UI with old user's data.
     * Add/remove to/from Interests list when it would be selected or deselected
     */
    private void setThirdStepUI() {
        User currentUser = BaseDataManager.getInstance().getUser();
        String[] currentInterests = currentUser.interests.split("/");

        if (mInterestsAdapter == null) {
            mInterestsAdapter = new InterestsAdapter(this);
            lvInterestList.setAdapter(mInterestsAdapter);
        }

        mInterestItems.clear();
        mCurrentInterestList.clear();
        ArrayList<String> interestsList = BaseDataManager.getInstance().getInterestList();
        for (String interestName: interestsList) {
            boolean isChecked = false;
            for (String oldInterest: currentInterests) {
                if (interestName.equals(oldInterest)) {
                    isChecked = true;
                    mCurrentInterestList.add(interestName);
                    break;
                }
            }

            InterestItem item = new InterestItem(interestName, isChecked);
            mInterestItems.add(item);
        }

        mInterestsAdapter.setData(mInterestItems);
        mInterestsAdapter.setOnItemSelectedListener(new InterestsAdapter.OnItemSelectedListener() {
            @Override
            public void onItemSelected(int position, boolean isChecked) {
                String interestName = mInterestItems.get(position).getName();
                if (isChecked) {
                    mCurrentInterestList.add(interestName);
                } else {
                    mCurrentInterestList.remove(interestName);
                }
            }
        });
    }

    /**
     * Check if the third step is valid or not.
     * @return
     */
    private boolean isValidationThirdStep() {
        if (mCurrentInterestList.size() < 3) {
            Toast.makeText(getApplicationContext(), R.string.set_interests_message, Toast.LENGTH_LONG).show();
            return false;
        }

        return true;
    }


    /**
     * Update Data
     */
    private void updateData() {
        if (mPhotoUri == null) {
            updateUserOnServer();
            return;
        }

        String photoName = FirebaseUtils.getUserId() + String.valueOf(System.currentTimeMillis()) + ".jpg";
        final StorageReference photoRef = FirebaseStorage.getInstance().getReference().child("images").child(photoName);

        Bitmap imageData = null;
        try {
            imageData = ImageUtils.decodeUri(mPhotoUri, true, AppConfig.IMAGE_REQUIRED_SIZE, this);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        if (imageData == null) {
            Toast.makeText(getApplicationContext(), "Can't get image.", Toast.LENGTH_LONG).show();
            return;
        }

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        imageData.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        imageData.recycle();
        byte[] byteData = baos.toByteArray();

        // Upload file to Firebase Storage
        UploadTask uploadTask = photoRef.putBytes(byteData);
        uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
            @Override
            public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                if (!task.isSuccessful()) {
                    throw task.getException();
                }

                // Continue with the task to get the download URL
                return photoRef.getDownloadUrl();
            }
        }).addOnCompleteListener(new OnCompleteListener<Uri>() {
            @Override
            public void onComplete(@NonNull Task<Uri> task) {
                hideProgressDialog();
                if (task.isSuccessful()) {
                    Uri downloadUri = task.getResult();
                    mUser.userProfile.photoUrl = downloadUri.toString();
                    uploadAvatar();
                } else {
                    // Handle failures
                }
            }
        });
    }

    /**
     * Upload an avatar image.
     */
    public void uploadAvatar() {
        String photoName = FirebaseUtils.getUserId() + String.valueOf(System.currentTimeMillis()) + ".jpg";
        final StorageReference photoRef = FirebaseStorage.getInstance().getReference().child("avatars").child(photoName);

        Bitmap imageData = null;
        try {
            imageData = ImageUtils.decodeUri(mPhotoUri, true, AppConfig.AVATAR_REQUIRED_SIZE, this);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        if (imageData == null) {
            Toast.makeText(getApplicationContext(), "Can't get image.", Toast.LENGTH_LONG).show();
            return;
        }

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        imageData.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        imageData.recycle();
        byte[] byteData = baos.toByteArray();

        // Upload file to Firebase Storage
        UploadTask uploadTask = photoRef.putBytes(byteData);
        uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
            @Override
            public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                if (!task.isSuccessful()) {
                    throw task.getException();
                }

                // Continue with the task to get the download URL
                return photoRef.getDownloadUrl();
            }
        }).addOnCompleteListener(new OnCompleteListener<Uri>() {
            @Override
            public void onComplete(@NonNull Task<Uri> task) {
                hideProgressDialog();
                if (task.isSuccessful()) {
                    Uri downloadUri = task.getResult();
                    mUser.userProfile.avatarUrl = downloadUri.toString();
                    updateUserOnServer();
                } else {
                    // Handle failures
                    Toast.makeText(getApplicationContext(), "Uploading failed.", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    /**
     * Update actually on the server.
     */
    private void updateUserOnServer() {
        Map<String, Object> userData = new HashMap<>();
        userData.put("dateOfBirth", mUser.dateOfBirth);
        userData.put("nationality", mUser.nationality);
        userData.put("emailAddress", mUser.emailAddress);
        userData.put("interests", mUser.interests);

        Map<String, Object> profileData = new HashMap<>();
        profileData.put("username", mUser.userProfile.username);
        profileData.put("photoUrl", mUser.userProfile.photoUrl);
        profileData.put("avatarUrl", mUser.userProfile.avatarUrl);
        profileData.put("profession", mUser.userProfile.profession);

        FirebaseDatabase.getInstance().getReference().child("users")
                .child(FirebaseUtils.getUserId()).updateChildren(userData);

        FirebaseDatabase.getInstance().getReference().child("users")
                .child(FirebaseUtils.getUserId()).child("userProfile").updateChildren(profileData);

        Toast.makeText(getApplicationContext(), getString(R.string.save_success_message), Toast.LENGTH_LONG).show();
        finish();
    }

    //---------------------- Step 3 End --------------------------/
    @OnClick(R.id.btnBack)
    public void onBack() {
        if (mCurrentIndex == 1) {
            finish();
        } else if (mCurrentIndex == 2) {
            mCurrentIndex = 1;
            setCurrentStep();
        } else {
            mCurrentIndex = 2;
            setCurrentStep();
        }
    }

    @OnClick(R.id.btnControl)
    public void onControl() {
        if (mCurrentIndex == 1) {
            //Checking Validation
            if (isValidationFirstStep()) {
                mCurrentIndex = 2;
                setCurrentStep();
            }

        } else if (mCurrentIndex == 2) {
            //Checking Validation
            if (isValidationSecondStep()) {
                mCurrentIndex = 3;
                setCurrentStep();
            }
        } else {
            //Checking Validation.
            if (isValidationThirdStep()) {
                String interestData = "";
                for (String interest: mCurrentInterestList){
                    if (interestData.equals(""))
                        interestData = interest;
                    else
                        interestData = interestData + "/" + interest;
                }

                mUser.interests = interestData;
                updateData();
            }
        }
    }

    private void setCurrentStep() {
        tvTitle.setText(getString(R.string.setup_profile_title) + "(" +
                Integer.toString(mCurrentIndex) + "/" + "3" +")");

        if (mCurrentIndex == 1) {
            btnControl.setText(R.string.continue_button_title);
            enableViews(rlFirstStep, ivCloseImage);
            disableViews(rlSecondStep, rlThirdStep);
            setFirstStepUI();
        }
        else if (mCurrentIndex == 2) {
            btnControl.setText(R.string.continue_button_title);
            mUser.userProfile.username = BaseUtils.removeGarbageFromString(etFullName.getText().toString());
            enableViews(rlSecondStep, btnBack, tvGender);
            disableViews(rlFirstStep, rlThirdStep, spSelectGender);

            setSecondStepUI();
        }
        else {
            btnControl.setText(R.string.submit_button_title);
            mUser.gender = spSelectGender.getSelectedItemPosition();
            mUser.dateOfBirth = tvSelectBirthDate.getText().toString();
            mUser.phoneNumber = getPhoneNumber();
            mUser.emailAddress = etEmailAddress.getText().toString();
            mUser.userProfile.profession = BaseUtils.removeGarbageFromString(etProfession.getText().toString());
            mUser.nationality = spSelectNationality.getSelectedItem().toString();

            enableViews(rlThirdStep, btnBack);
            disableViews(rlFirstStep, rlSecondStep);
            btnControl.setText(R.string.done);
            setThirdStepUI();
        }
    }
}
