package com.hadif.famousme.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.hadif.famousme.R;
import com.hadif.famousme.managers.basedata.BaseDataManager;
import com.hadif.famousme.managers.basedata.ThemeManager;
import com.hadif.famousme.models.BaseData;
import com.hadif.famousme.models.User;
import com.hadif.famousme.utils.FirebaseUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ProvideServiceActivity extends BaseActivity {

    public static void start (Context context) {
        Intent intent = new Intent(context, ProvideServiceActivity.class);
        context.startActivity(intent);
    }

    public static void start(AppCompatActivity activity, int requestCode) {
        Intent intent = new Intent(activity, ProvideServiceActivity.class);
        activity.startActivityForResult(intent, requestCode);
    }

    //UI Control
    @BindView(R.id.rlHeader)    RelativeLayout rlHeader;
    @BindView(R.id.spSelectService)   Spinner spSelectService;

    @BindView(R.id.etOffering)    EditText etOffering;
    @BindView(R.id.btnSubmit)     TextView btnSubmit;

    //Private members
    private ArrayList<String> mServiceList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(ThemeManager.getInstance().getCurrentThemeStyle());
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service);

        ButterKnife.bind(this);

        applyTheme();

        getServicesList();
    }


    private void applyTheme() {
        ThemeManager.getInstance().setThemeWithChildViews(rlHeader);
        ThemeManager.getInstance().setTheme(btnSubmit);
    }

    /**
     * Get services list
     */
    private void getServicesList() {
        mServiceList = BaseDataManager.getServicesList(ProvideServiceActivity.this);
        setUI();
    }

    /**
     * Set UI
     */
    private void setUI() {
        User user = BaseDataManager.getInstance().getUser();
        if (user == null)
            return;

        ArrayAdapter<String> nationalityAdapter = new ArrayAdapter<String>(
                this, android.R.layout.simple_spinner_dropdown_item, mServiceList);
        spSelectService.setAdapter(nationalityAdapter);

        int searchIndex = 0;
        if (!TextUtils.isEmpty(user.serviceTitle)) {
            for (int i = 0; i < mServiceList.size(); i++) {
                String serviceName = mServiceList.get(i);
                if (serviceName.equals(user.serviceTitle))
                    searchIndex = i;
            }
        }

        spSelectService.setSelection(searchIndex);
        if (!TextUtils.isEmpty(user.serviceDetails))
            etOffering.setText(user.serviceDetails);
    }

    /**
     * Save service data
     */
    private void submitServiceData() {
        User user = BaseDataManager.getInstance().getUser();

        user.serviceTitle = spSelectService.getSelectedItem().toString();
        user.serviceDetails = etOffering.getText().toString();

        Map<String, Object> serviceData = new HashMap<>();

        serviceData.put("serviceTitle", spSelectService.getSelectedItem().toString());
        serviceData.put("serviceDetails", etOffering.getText().toString());

        FirebaseDatabase.getInstance().getReference().child("users")
                .child(user.key).updateChildren(serviceData);
    }

    @OnClick(R.id.btnBack)
    public void onBack() {
        finish();
    }

    @OnClick(R.id.btnSubmit)
    public void onSubmit() {
        submitServiceData();
        //setResult(RESULT_OK);
        finish();
    }
}
