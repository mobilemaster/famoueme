package com.hadif.famousme.utils.pickers;

public interface CountryPickerListener {
	public void onSelectCountry(String name, String code, String dialCode);
}
