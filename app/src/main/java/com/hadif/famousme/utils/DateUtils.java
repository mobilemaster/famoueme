package com.hadif.famousme.utils;

import android.content.Context;
import android.text.TextUtils;

import com.hadif.famousme.R;
import com.hadif.famousme.models.TimeGap;

import org.w3c.dom.Text;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtils {

    /**
     * Get Age with string.
     * @param dateString the string that should be converted.
     * @return int
     */
    public static int getAge(String dateString){
        if (TextUtils.isEmpty(dateString))
            return 0;

        dateString = dateString.replace(" ", ""); //remove all space.
        String strYear = dateString.split("/")[2];
        String strMonth = dateString.split("/")[1];
        String strDay = dateString.split("/")[0];
        if (TextUtils.isEmpty(strYear) || TextUtils.isEmpty(strMonth) || TextUtils.isEmpty(strDay))
            return 0;

        int year, month, day;
        try {
            year = Integer.parseInt(strYear);
            month = Integer.parseInt(strMonth);
            day = Integer.parseInt(strDay);
        }
        catch (NumberFormatException nfe) {
            System.out.println("NumberFormatException: " + nfe.getMessage());
            return 0;
        }

        Calendar dob = Calendar.getInstance();
        Calendar today = Calendar.getInstance();

        dob.set(year, month, day);

        int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);
        if (today.get(Calendar.DAY_OF_YEAR) < dob.get(Calendar.DAY_OF_YEAR)){
            age--;
        }

        Integer ageInt = new Integer(age);
        //String ageS = ageInt.toString();

        return ageInt;
    }

    /**
     * Get the gap between times with int value
     * @param timeStamp
     * @return int
     */
    public static TimeGap getGapOfTimesWithInt(long timeStamp) {
        // Calculate difference in milliseconds
        long diff = Math.abs(System.currentTimeMillis() - timeStamp);

        TimeGap timeGap = new TimeGap();
        if (diff < 24 * 60 * 60 * 1000) {
            if (diff < 60 * 60 * 1000) { // minutes
                timeGap.gapType = TimeGap.GAP_TIME_HOURS;
                timeGap.gapValue = (int)(diff / (60 * 1000));
            } else { //hours
                timeGap.gapType = TimeGap.GAP_TIME_HOURS;
                timeGap.gapValue = (int)(diff / (60 * 60 * 1000));
            }
        } else { //Day
            timeGap.gapType = TimeGap.GAP_TIME_DAYS;
            timeGap.gapValue = (int)(diff / (24 * 60 * 60 * 1000));
        }

        return timeGap;
    }

    /**
     * Get gap of between twe times with string.
     * @param timeStamp
     * @param context
     * @return String
     */
    public static String getGapOfTimesWithString(long timeStamp, Context context) {
        // Calculate difference in milliseconds
        long diff = Math.abs(System.currentTimeMillis() - timeStamp);

        String gapString = "";
        if (diff < 24 * 60 * 60 * 1000) {
            if (diff < 60 * 60 * 1000) {// minutes
                if (diff < 60 * 1000)
                    gapString = context.getString(R.string.now);
                else
                    gapString = String.valueOf((int) (diff / (60 * 1000))) + " " + context.getString(R.string.minutes_ago);
            }
            else //hours
                gapString = String.valueOf((int)(diff / (60 * 60 * 1000))) + " " + context.getString(R.string.hours_ago);
        } else { //Day
            gapString = String.valueOf((int)(diff / (24 * 60 * 60 * 1000))) + " " + context.getString(R.string.days_ago);
        }

        return gapString;
    }

    /**
     * Get gap of date with string
     * @param timestamp
     * @param context
     * @return String
     */
    public static String getGapOfDateWithString(long timestamp, Context context) {
        long diff = Math.abs(System.currentTimeMillis() - timestamp);

        String gapString = "";
        if (diff < 24 * 60 * 60 * 1000) {
            gapString = context.getString(R.string.today);
        } else {
            int days = (int)(diff / (24 * 60 * 60 * 1000));
            if (days == 1)
                gapString = context.getString(R.string.yesterday);
            else if(days > 1 && days < 5)
                gapString = days + " " + context.getString(R.string.days_ago);
            else
                gapString = getDateFromTimestamp(timestamp);
        }

        return gapString;
    }

    /**
     * Get gap of date
     * @param timestamp
     * @return int
     */
    public static int getGapOfDate(long timestamp) {
        long diff = Math.abs(System.currentTimeMillis() - timestamp);

        int gap = 0;
        if (diff > 24 * 60 * 60 * 1000)
            gap = (int)(diff / (24 * 60 * 60 * 1000));

        return gap;
    }


    /**
     * Timestamp to String.
     * @param timestamp
     * @return String
     */
    public static String getDateFromTimestamp(long timestamp) {
        try{
            DateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
            Date netDate = (new Date(timestamp));
            return sdf.format(netDate);
        }
        catch(Exception ex){
            return "xx";
        }
    }

    /**
     * Get Time string from timestamp
     * @param timestamp
     * @return String
     */
    public static String getTimeFromTimestamp(long timestamp) {
        try{
            DateFormat sdf = new SimpleDateFormat("hh:mm");
            Date netDate = (new Date(timestamp));
            return sdf.format(netDate);
        }
        catch(Exception ex){
            return "xx";
        }
    }

    /**
     * Get date string from timestamp like Aug 29
     * @param timestamp
     * @return
     */
    public static String getMonthAndDayFromTimestamp(long timestamp) {
        try{
            DateFormat sdf = new SimpleDateFormat("MMM dd");
            Date netDate = (new Date(timestamp));
            return sdf.format(netDate);
        }
        catch(Exception ex){
            return "xx";
        }
    }

}
