package com.hadif.famousme.utils;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.firebase.geofire.GeoLocation;
import com.google.android.gms.maps.model.Circle;
import com.hadif.famousme.AppConfig;
import com.hadif.famousme.R;

import java.io.File;

import static android.content.Context.INPUT_METHOD_SERVICE;


public class BaseUtils {
    public static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 123;

    /**
     * Get Distance string.
     * @param distance the value of distance
     * @param context the current context.
     * @return String
     */
    public static String getDistanceString(int distance, Context context) {
        if (distance < 150)
            return context.getResources().getString(R.string.near_you);
        else {
            if (distance >= 1000) {
                float fValue = ((float) distance) / 1000;
                return String.valueOf(String.format("%.2f", fValue)) + " " +
                        context.getResources().getString(R.string.km_away);
            }

            return String.valueOf(distance) + " " +
                    context.getResources().getString(R.string.meters_away);
        }
    }

    /**
     * Get zoom level by circle radius.
     * @param circle a circle with radius
     * @return zoom level
     */
    public static int getZoomLevel(Circle circle) {
        int zoomLevel = 0;
        if (circle != null){
            double radius = getRadiusInMetres();
            double scale = radius / 500;
            zoomLevel =(int) (16 - Math.log(scale) / Math.log(2));
        }

        return zoomLevel;
    }

    /**
     * Get radius in metres.
     * @return radius
     */
    public static double getRadiusInMetres() {
        return AppConfig.Location.NEAR_FINDING_RADIUS * 1000;
    }

    /**
     * Get GeoLocation instance from the location.
     * @param location Location
     * @return GeoLocation
     */
    public static GeoLocation getGeoLocationFromLocation(Location location) {
        return new GeoLocation(location.getLatitude(), location.getLongitude());
    }

    /**
     * Check if the format of email is correct or not.
     * @param target email string which is entered
     * @return true or false
     */
    public final static boolean isValidEmail(CharSequence target) {
        if (TextUtils.isEmpty(target)) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    /**
     * Check if the format of user string like name or profession is correct or not.
     * Sometimes, a user clicks Enter key more than 2.
     * @param target email string which is entered
     * @return true or false
     */
    public final static boolean isValidUserString(String target) {
        if (TextUtils.isEmpty(target)) {
            return false;
        }

        return true;
    }

    /**
     * Remove \n characters and space.
     * @param str the string to be processed
     * @return the result.
     */
    public final static String removeGarbageFromString(String str) {
        String subString = str.replace("\n", " ");
        while (subString != null && subString.length() > 0 && subString.charAt(subString.length() - 1) == ' ') {
            subString = subString.substring(0, subString.length() - 1);
        }

        return subString;
    }


    /**
     * Hide keyboard programmatically
     * @param context the current context
     * @param editText the edit text view which is focused on.
     */
    public static void hideSoftKeyBoard(Context context, EditText editText) {
        InputMethodManager imm = (InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
    }

    /**
     * Delete Directory
     * @param fileOrDirectory the file or directory that should be deleted.
     */
    public static void deleteRecursive(File fileOrDirectory) {
        if (!fileOrDirectory.exists())
            return;

        if (fileOrDirectory.isDirectory()) {
            for (File child : fileOrDirectory.listFiles()) {
                deleteRecursive(child);
            }
        }

        fileOrDirectory.delete();
    }

}
