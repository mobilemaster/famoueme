package com.hadif.famousme.utils;

import android.content.Context;
import android.location.Location;

import com.firebase.geofire.GeoFire;
import com.firebase.geofire.GeoLocation;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.hadif.famousme.AppConfig;
import com.hadif.famousme.R;
import com.hadif.famousme.managers.basedata.BaseDataManager;
import com.hadif.famousme.models.TimeGap;
import com.hadif.famousme.models.User;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class FirebaseUtils {

    public static DatabaseReference getDatabaseReference() {
        return FirebaseDatabase.getInstance().getReference();
    }

    public static String getUserId() {
        if (FirebaseAuth.getInstance().getCurrentUser() == null)
            return null;
        
        return FirebaseAuth.getInstance().getCurrentUser().getUid();
    }

    public static String getConversationId(String firstUserId, String secondUserId) {
        return firstUserId + "_" + secondUserId;
    }

    /**
     * Get conversation id.
     * @param secondUserId
     * @return
     */
    public static String getConversationId(String secondUserId) {
        return getUserId().compareTo(secondUserId) > 0 ?
                getUserId() + "_" + secondUserId : secondUserId + "_" + getUserId();
    }

    /**
     * Update achievement!
     * @param achievementType
     * @param context
     */
    public static void updateAchievements(String achievementType, Context context) {
        String myId = FirebaseUtils.getUserId();
        Map<String, Object> upgradeData = new HashMap<>();
        String childKey = "1";
        switch (achievementType) {
            case AppConfig.Achievement.ACHIEVEMENT_START_CONVERSATION:
                upgradeData.put("type", AppConfig.Achievement.ACHIEVEMENT_START_CONVERSATION);
                upgradeData.put("name", context.getString(R.string.start_conversation));
                upgradeData.put("value", 25);
                childKey = "1";
                break;
            case AppConfig.Achievement.ACHIEVEMENT_USE_APP_TWO_WEEKS:
                upgradeData.put("type", AppConfig.Achievement.ACHIEVEMENT_USE_APP_TWO_WEEKS);
                upgradeData.put("name", context.getString(R.string.use_the_app));
                upgradeData.put("value", 25);
                childKey = "2";
                break;
            case AppConfig.Achievement.ACHIEVEMENT_IMPORT_CONTACTS:
                upgradeData.put("type", AppConfig.Achievement.ACHIEVEMENT_IMPORT_CONTACTS);
                upgradeData.put("name", context.getString(R.string.import_your_contacts));
                upgradeData.put("value", 25);
                childKey = "3";
                break;
        }

        upgradeData.put("timestamp", System.currentTimeMillis());

        FirebaseDatabase.getInstance().getReference().child("achievements")
                .child(myId).child(childKey).updateChildren(upgradeData);

    }

    /**
     * Send Notification
     * @param userId Receiver Id
     * @param notificationType the kind of notification
     */
    public static void sendNotification(String userId, int notificationType) {
        User mine = BaseDataManager.getInstance().getUser();
        Map<String, Object> notification = new HashMap<>();
        notification.put("userId", mine.key);
        notification.put("timestamp", ServerValue.TIMESTAMP);
        notification.put("isRead", Boolean.FALSE);
        notification.put("notificationType", notificationType);

        FirebaseUtils.getDatabaseReference().child("notifications").child(userId).push().setValue(notification);
    }

    /**
     * Get geo fire instance.
     * @return GeoFire
     */
    public static GeoFire getGeoFire() {
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference("positions");
        GeoFire geoFire = new GeoFire(ref);

        return geoFire;
    }

    /**
     * Get GeoLocation instance from current location.
     * @return GeoLocation
     */
    public static GeoLocation getGeoLocation(Location location) {
        return new GeoLocation(location.getLatitude(), location.getLongitude());
    }

}
