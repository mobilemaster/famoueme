package com.hadif.famousme.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hadif.famousme.R;
import com.hadif.famousme.adapters.ConversationsAdapter;

/**
 * A simple {@link Fragment} subclass.
 */
public class BasicFragment extends Fragment {

    public static interface OnCompleteListener {
        public abstract void onComplete();
    }

    protected OnCompleteListener mListener;
    public void setCompletedListener(OnCompleteListener listener) {
        mListener = listener;
    }

    public BasicFragment() {
        // Required empty public constructor
    }

    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            this.mListener = (OnCompleteListener)context;
        }
        catch (final ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement OnCompleteListener");
        }
    }
}
