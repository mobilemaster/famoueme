package com.hadif.famousme.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.hadif.famousme.AppConfig;
import com.hadif.famousme.R;
import com.hadif.famousme.activities.ChatActivity;
import com.hadif.famousme.activities.ImageViewerActivity;
import com.hadif.famousme.activities.MyProfileActivity;
import com.hadif.famousme.adapters.NotificationsAdapter;
import com.hadif.famousme.managers.basedata.BaseDataManager;
import com.hadif.famousme.managers.basedata.ThemeManager;
import com.hadif.famousme.models.Contact;
import com.hadif.famousme.models.Conversation;
import com.hadif.famousme.models.Discovery;
import com.hadif.famousme.models.Notification;
import com.hadif.famousme.models.User;
import com.hadif.famousme.models.UserProfile;
import com.hadif.famousme.utils.FirebaseUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class NotificationsFragment extends BasicFragment {

    public static NotificationsFragment newInstance() {
        return new NotificationsFragment();
    }

    //UI controls
    private View mRootView = null;
    @BindView(R.id.rlHeader)                    RelativeLayout rlHeader;
    @BindView(R.id.lvNotificationsList)         ListView lvNotificationsList;

    //Private Members
    NotificationsAdapter mNotificationsAdapter;
    ArrayList<Notification> mNotificationsList = new ArrayList<>();

    public NotificationsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (mRootView == null) {
            // Inflate the layout for this fragment
            mRootView = inflater.inflate(R.layout.fragment_notifications, container, false);
            ButterKnife.bind(this, mRootView);

            mNotificationsAdapter = new NotificationsAdapter(getContext());
            lvNotificationsList.setAdapter(mNotificationsAdapter);
            mNotificationsAdapter.setData(mNotificationsList);
            mNotificationsAdapter.setOnProfileLongPressListener(new NotificationsAdapter.OnProfileLongPressListener() {
                @Override
                public void onProfileLongPressed(int position) {
                    Notification notification = mNotificationsList.get(position);
                    ImageViewerActivity.start(getContext(), notification.userProfile.photoUrl);
                }
            });

            lvNotificationsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
                    Notification notification = (Notification) lvNotificationsList.getItemAtPosition(position);
                    Intent intent = new Intent(getActivity(), ChatActivity.class);
                    intent.putExtra("shouldCheckUpdate", true);
                    intent.putExtra("isValid", notification.userProfile.isValid);
                    intent.putExtra("userId", notification.userId);
                    intent.putExtra("profession", notification.userProfile.profession);
                    intent.putExtra("username", notification.userProfile.username);
                    intent.putExtra("photoUrl", notification.userProfile.photoUrl);
                    getActivity().startActivityForResult(intent, AppConfig.Request.REQUEST_CHAT);

                }
            });

            mListener.onComplete();
        }

        applyTheme();

        return mRootView;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void applyTheme() {
        ThemeManager.getInstance().setThemeWithChildViews(rlHeader);
    }

    /**
     * Check if the notifications are new or not
     */
    public void checkNewNotifications() {
        String userId = FirebaseUtils.getUserId();
        for (Notification notification:mNotificationsList) {
            if (!notification.isRead) {
                Map<String, Object> updateMap = new HashMap<>();
                updateMap.put("isRead", Boolean.TRUE);

                FirebaseUtils.getDatabaseReference().child("notifications")
                        .child(userId).child(notification.key).updateChildren(updateMap);
            }
        }

        BaseDataManager.getInstance().setNewNotifications(0);
    }

    /**
     * Check the duplicated notification.
     * @param newNotification
     * @return
     */
    private boolean isDuplicatedNotification(Notification newNotification) {
        if (mNotificationsList == null)
            return false;

        for (Notification notification: mNotificationsList) {
            if (notification.notificationType == newNotification.notificationType &&
                    notification.userId.equals(newNotification.userId)) {
                notification.timestamp = newNotification.timestamp;
                return true;
            }

        }
        return false;
    }
    /**
     * Add a new notification
     * @param notification
     */
    public void addNotification(Notification notification) {
        if (!isDuplicatedNotification(notification)) {
            setUserProfilesToNotification(notification);
        } else {
            //Remove the duplicated notification.
            String userId = FirebaseUtils.getUserId();
            FirebaseUtils.getDatabaseReference().child("notifications")
                    .child(userId).child(notification.key).removeValue();
        }
    }

    /**
     * Set user profiles to the discovery which was added.
     * @param notification
     */
    private void setUserProfilesToNotification(final Notification notification) {
        FirebaseDatabase.getInstance().getReference().child("users").child(notification.userId).child("userProfile")
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot snapshot) {
                        if (snapshot == null)
                            return;

                        UserProfile userProfile = snapshot.getValue(UserProfile.class);
                        if (userProfile == null)
                            return;

                        notification.userProfile = userProfile;
                        mNotificationsList.add(0, notification);
                        reloadNotificationList();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Toast.makeText(getContext(), "Can't get user profile!", Toast.LENGTH_LONG).show();
                    }
                });
    }

    /**
     * If the user profile is updated, reset the user profile with the data.
     * @param data changed data.
     */
    public void resetUserProfileByActivityResult(Intent data) {
        Boolean isUpdated = data.getBooleanExtra("isUpdated", false);
        if (!isUpdated)
            return;

        String userKey = data.getStringExtra("userKey");
        String username = data.getStringExtra("username");
        String photoUrl = data.getStringExtra("photoUrl");
        String profession = data.getStringExtra("profession");

        for (Notification notification: mNotificationsList) {
            if (notification.userId.equals(userKey)) {
                notification.userProfile.username = username;
                notification.userProfile.photoUrl = photoUrl;
                notification.userProfile.profession = profession;
                break;
            }
        }

        reloadNotificationList();
    }

    /**
     * Reload notification list
     */
    private void reloadNotificationList() {
        if (mNotificationsAdapter == null || mNotificationsList == null)
            return;

        mNotificationsAdapter.setData(mNotificationsList);
        mNotificationsAdapter.notifyDataSetChanged();
    }

    /**
     * Remove notification list
     */

    private void removeAllNotifications() {
        if (mNotificationsList == null)
            return;

        mNotificationsList.clear();
        User user = BaseDataManager.getInstance().getUser();
        FirebaseUtils.getDatabaseReference().child("notifications").child(user.key).removeValue();
        reloadNotificationList();
    }

    /**
     * Hide notification list
     */
    public void hideAllNotifications() {
        if (mNotificationsList == null)
            return;

        mNotificationsList.clear();
        reloadNotificationList();
    }


    @OnClick(R.id.btnClearAll)
    public void onClearAll() {
        removeAllNotifications();
    }
}