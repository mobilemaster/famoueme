package com.hadif.famousme.fragments;


import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.signature.StringSignature;
import com.filters.instagram.InstagramActivity;
import com.firebase.geofire.GeoLocation;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.hadif.famousme.AppConfig;
import com.hadif.famousme.R;
import com.hadif.famousme.activities.ChatActivity;
import com.hadif.famousme.activities.ImageViewerActivity;
import com.hadif.famousme.activities.MainActivity;
import com.hadif.famousme.activities.MyProfileActivity;
import com.hadif.famousme.activities.UserProfileActivity;
import com.hadif.famousme.adapters.ContactsAdapter;
import com.hadif.famousme.adapters.ConversationsAdapter;
import com.hadif.famousme.adapters.DiscoveriesAdapter;
import com.hadif.famousme.adapters.NotificationsAdapter;
import com.hadif.famousme.managers.basedata.BaseDataManager;
import com.hadif.famousme.managers.basedata.ThemeManager;
import com.hadif.famousme.models.Contact;
import com.hadif.famousme.models.Conversation;
import com.hadif.famousme.models.Discovery;
import com.hadif.famousme.models.Notification;
import com.hadif.famousme.models.User;
import com.hadif.famousme.models.UserProfile;
import com.hadif.famousme.utils.BaseUtils;
import com.hadif.famousme.utils.DateUtils;
import com.hadif.famousme.utils.FirebaseUtils;
import com.views.listview.ScrollingListView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import jp.wasabeef.glide.transformations.CropCircleTransformation;

/**
 * A simple {@link Fragment} subclass.
 */
public class ConversationsFragment extends BasicFragment {

    public static ConversationsFragment newInstance() {
        return new ConversationsFragment();
    }

    //UI Controls
    private static int MODE_CONVERSATIONS = 0;
    private static int MODE_DISCOVERIES = 1;
    private static int MODE_CONTACTS = 2;

    private static final int LIST_SCROLL_UP_DETECT_OFFSET = 20;

    //[START Header]
    @BindView(R.id.ivProfile)                   ImageView ivProfile;
    @BindView(R.id.tvName)                      TextView tvName;
    //[End Header]

    private View mRootView = null;
    @BindView(R.id.rlHeader)                    RelativeLayout rlHeader;
    @BindView(R.id.llChatsLayout)               View llChatsLayout;
    @BindView(R.id.rlDiscoveriesLayout)         View rlDiscoveriesLayout;
    @BindView(R.id.llContactsLayout)            View llContactsLayout;
    @BindView(R.id.llChatsTabLayout)            View llChatsTabLayout;
    @BindView(R.id.llDiscoveriesTabLayout)      View llDiscoveriesTabLayout;
    @BindView(R.id.llContactsTabLayout)         View llContactsTabLayout;

    @BindView(R.id.tvConversationsCount)        TextView tvConversationsCount;
    @BindView(R.id.tvConversationsTitle)        TextView tvConversationsTitle;
    @BindView(R.id.llChatsSearchLayout)         View llChatsSearchLayout;
    @BindView(R.id.etChatsSearchField)          EditText etChatsSearchField;

    @BindView(R.id.tvDiscoveriesCount)          TextView tvDiscoveriesCount;
    @BindView(R.id.tvDiscoveriesTitle)          TextView tvDiscoveriesTitle;
    @BindView(R.id.llDiscoveriesSearchLayout)   View llDiscoveriesSearchLayout;
    @BindView(R.id.etDiscoveriesSearchField)    EditText etDiscoveriesSearchField;

    @BindView(R.id.tvContactsCount)             TextView tvContactsCount;
    @BindView(R.id.tvContactsTitle)             TextView tvContactsTitle;
    @BindView(R.id.llContactsSearchLayout)      View llContactsSearchLayout;
    @BindView(R.id.etContactsSearchField)       EditText etContactsSearchField;

    @BindView(R.id.lvConversationsList)         ListView lvConversationsList;
    @BindView(R.id.lvDiscoveriesList)           ListView lvDiscoveriesList;
    @BindView(R.id.lvContactsList)              ListView lvContactsList;

    @BindView(R.id.lyNoConversationNotice)      View lyNoConversationNotice;

    @BindView(R.id.vgWelcomeBack)               View vgWelcomeBack;
    @BindView(R.id.tvMessageTitle)              TextView tvMessageTitle;
    @BindView(R.id.tvMessageText)               TextView tvMessageText;

    @BindView(R.id.btnGotoMap)                  TextView btnGotoMap;

    //Private Members
    ArrayList<TabItemHolder> mTabItems = new ArrayList<>();
    ConversationsAdapter mConversationsAdapter;
    ArrayList<Conversation> mConversationsItems = new ArrayList<>();
    ArrayList<Conversation> mSearchConversationsItems = new ArrayList<>();

    DiscoveriesAdapter mDiscoveriesAdapter;
    ArrayList<Discovery> mDiscoveriesItems = new ArrayList<>();
    ArrayList<Discovery> mSearchDiscoveriesItems = new ArrayList<>();

    ContactsAdapter mContactsAdapter;
    ArrayList<Contact> mContactsItems = new ArrayList<>();
    ArrayList<Contact> mSearchContactsItems = new ArrayList<>();

    private int mLastFirstVisibleItem; //For hide/show search var
    private boolean mIsConversationSearchMode;
    private boolean mIsDiscoverySearchMode;
    private boolean mIsContactSearchMode;
    private boolean mShowWelcomeMessage = false;
    private int mCountOfUnreadMessages = 0;

    public ConversationsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (container == null) {
            // We have different layouts, and in one of them this
            // fragment's containing frame doesn't exist.  The fragment
            // may still be created from its saved state, but there is
            // no reason to try to create its view hierarchy because it
            // won't be displayed.  Note this is not needed -- we could
            // just run the code below, where we would create and return
            // the view hierarchy; it would just never be used.
            return null;
        }

        if (mRootView == null) {
            // Inflate the layout for this fragment
            mRootView = inflater.inflate(R.layout.fragment_conversations, container, false);
            ButterKnife.bind(this, mRootView);

            setTabItems();
            setCurrentMode(MODE_CONVERSATIONS);

            //[START - Conversations]
            mConversationsAdapter = new ConversationsAdapter(getContext());
            lvConversationsList.setAdapter(mConversationsAdapter);
            mConversationsAdapter.setData(mConversationsItems);
            mConversationsAdapter.setOnProfileLongPressListener(new ConversationsAdapter.OnProfileLongPressListener() {
                @Override
                public void onProfileLongPressed(int position) {
                    Conversation conversation = mConversationsItems.get(position);
                    ImageViewerActivity.start(getContext(), conversation.userProfile.photoUrl);
                }
            });

            lvConversationsList.setClickable(true);
            lvConversationsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
                    Conversation conversation = (Conversation)lvConversationsList.getItemAtPosition(position);
                    Intent intent = new Intent(getActivity(), ChatActivity.class);
                    intent.putExtra("shouldCheckUpdate", true);
                    intent.putExtra("isValid", conversation.userProfile.isValid);
                    intent.putExtra("userId", conversation.userId);
                    intent.putExtra("profession", conversation.userProfile.profession);
                    intent.putExtra("username", conversation.userProfile.username);
                    intent.putExtra("photoUrl", conversation.userProfile.photoUrl);
                    intent.putExtra("avatarUrl", conversation.userProfile.avatarUrl);
                    getActivity().startActivityForResult(intent, AppConfig.Request.REQUEST_CHAT);
                }
            });

            lvConversationsList.setOnTouchListener(OnConversationsTouchUpListener());
            getConversations();
            //[END - Conversations]

            //[START - Discoveries]
            mDiscoveriesAdapter = new DiscoveriesAdapter(getContext());
            lvDiscoveriesList.setAdapter(mDiscoveriesAdapter);
            mDiscoveriesAdapter.setData(mDiscoveriesItems);
            mDiscoveriesAdapter.setOnProfileLongPressListener(new DiscoveriesAdapter.OnProfileLongPressListener() {
                @Override
                public void onProfileLongPressed(int position) {
                    Discovery discovery = mDiscoveriesItems.get(position);
                    ImageViewerActivity.start(getContext(), discovery.userProfile.photoUrl);
                }
            });

            lvDiscoveriesList.setClickable(true);
            lvDiscoveriesList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
                    Discovery discovery = (Discovery)lvDiscoveriesList.getItemAtPosition(position);
                    //String timeGap = DateUtils.getGapOfTimesWithString(discovery.timestamp, getContext());
                    String distance = BaseUtils.getDistanceString((int)discovery.distance, getContext());

                    FirebaseUtils.sendNotification(discovery.userKey, AppConfig.Notification.NOTIFICATION_VIEWED_PROFILE);

                    //Call user profile activity
                    Intent intent = new Intent(getActivity(), UserProfileActivity.class);
                    intent.putExtra("isFromChat", false);
                    intent.putExtra("shouldCheckUpdate", true);
                    intent.putExtra("status", /*timeGap + " - " +*/ distance);
                    intent.putExtra("userId", discovery.userKey);
                    intent.putExtra("profession", discovery.userProfile.profession);
                    intent.putExtra("username", discovery.userProfile.username);
                    intent.putExtra("photoUrl", discovery.userProfile.photoUrl);
                    intent.putExtra("avatarUrl", discovery.userProfile.avatarUrl);
                    getActivity().startActivityForResult(intent, AppConfig.Request.REQUEST_USER_PROFILE);
                }
            });

            lvDiscoveriesList.setOnTouchListener(OnDiscoveriesTouchUpListener());
            //getDiscoveries();
            //[END - Discoveries]

            //[START - Contacts]
            mContactsAdapter = new ContactsAdapter(getContext());
            lvContactsList.setAdapter(mContactsAdapter);
            mContactsAdapter.setData(mContactsItems);
            lvContactsList.setClickable(true);
            lvContactsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
                    Contact contact = (Contact) lvContactsList.getItemAtPosition(position);

                    Intent intent = new Intent(getActivity(), ChatActivity.class);
                    intent.putExtra("shouldCheckUpdate", true);
                    intent.putExtra("isValid", contact.userProfile.isValid);
                    intent.putExtra("userId", contact.userId);
                    intent.putExtra("profession", contact.userProfile.profession);
                    intent.putExtra("username", contact.userProfile.username);
                    intent.putExtra("photoUrl", contact.userProfile.photoUrl);
                    intent.putExtra("avatarUrl", contact.userProfile.avatarUrl);
                    intent.putExtra("startFrom", AppConfig.ChartFrom.FROM_CONTACT);
                    getActivity().startActivityForResult(intent, AppConfig.Request.REQUEST_CHAT);
                }
            });
            mContactsAdapter.setOnProfileLongPressListener(new ContactsAdapter.OnProfileLongPressListener() {
                @Override
                public void onProfileLongPressed(int position) {
                    Contact contact = mContactsItems.get(position);
                    ImageViewerActivity.start(getContext(), contact.userProfile.photoUrl);
                }
            });

            lvContactsList.setOnTouchListener(OnContactsTouchUpListener());
            getContacts();
            //[END - Contacts]

            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    showWelcomeBackMessage();
                }
            }, 3000); // Change to what you want

            mListener.onComplete();
        }

        applyTheme();

        return mRootView;
    }

    public void setShowWelcomeMessage(boolean showMessage) {
        mShowWelcomeMessage = showMessage;
    }

    private void showWelcomeBackMessage() {
        User user = BaseDataManager.getInstance().getUser();
        if (!mShowWelcomeMessage || user == null || user.userProfile == null)
            return;

        if (mCountOfUnreadMessages == 0)
            tvMessageText.setVisibility(View.GONE);
        else
            tvMessageText.setText(Integer.toString(mCountOfUnreadMessages) + " " + getString(R.string.new_messages));

        tvMessageTitle.setText(getString(R.string.welcome_back) + " " + user.userProfile.username);
        vgWelcomeBack.setVisibility(View.VISIBLE);
        int themeColor = getContext().getResources().getColor(ThemeManager.getInstance().getCurrentThemeColor());
        vgWelcomeBack.setBackgroundColor(themeColor);
        vgWelcomeBack.animate()
                .translationYBy(vgWelcomeBack.getHeight())
                .setDuration(800)
                .setStartDelay(0)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        dismissWelcomeBackMessage();
                    }
                })
                .start();
    }

    private void dismissWelcomeBackMessage() {
        vgWelcomeBack.clearAnimation();
        vgWelcomeBack.animate()
                .translationYBy(-vgWelcomeBack.getHeight())
                .setDuration(800)
                .setStartDelay(1000)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        vgWelcomeBack.setVisibility(View.GONE);
                    }
                })
                .start();
    }

    private void applyTheme() {
        ThemeManager.getInstance().setThemeWithChildViews(rlHeader);
        ThemeManager.getInstance().setMainColorButton(btnGotoMap);
    }

    /**
     * Set the photo of user profile.
     */
    public void setUserProfile() {
        if (getContext() == null)
            return;

        User user = BaseDataManager.getInstance().getUser();
        if (user != null && !TextUtils.isEmpty(user.userProfile.avatarUrl)) {

            Glide.with(getContext())
                    .load(user.userProfile.avatarUrl)
                    .bitmapTransform(new CropCircleTransformation(getContext()))
                    .into(ivProfile);

            ivProfile.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    User user = BaseDataManager.getInstance().getUser();
                    ImageViewerActivity.start(getContext(), user.userProfile.photoUrl);
                    return true;
                }
            });

            ivProfile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    MyProfileActivity.start(getContext());
                }
            });
        }

        tvName.setText(user.userProfile.username);
    }

    /**
     * If the user profile is updated, reset the user profile with the data.
     * @param data changed data.
     */
    public void resetUserProfileByActivityResult(Intent data) {
        Boolean isUpdated = data.getBooleanExtra("isUpdated", false);
        if (!isUpdated)
            return;

        String userKey = data.getStringExtra("userKey");
        String username = data.getStringExtra("username");
        String photoUrl = data.getStringExtra("photoUrl");
        String avatarUrl = data.getStringExtra("avatarUrl");
        String profession = data.getStringExtra("profession");

        for (Discovery discovery: mDiscoveriesItems) {
            if (discovery.userKey.equals(userKey)) {
                discovery.userProfile.username = username;
                discovery.userProfile.photoUrl = photoUrl;
                discovery.userProfile.avatarUrl = avatarUrl;
                discovery.userProfile.profession = profession;
                break;
            }
        }

        reloadDiscoveryList();

        for (Conversation conversation: mConversationsItems) {
            if (conversation.userId.equals(userKey)) {
                conversation.userProfile.username = username;
                conversation.userProfile.photoUrl = photoUrl;
                conversation.userProfile.avatarUrl = avatarUrl;
                conversation.userProfile.profession = profession;
                break;
            }
        }

        reloadConversationsList();

        for (Contact contact: mContactsItems) {
            if (contact.userId.equals(userKey)) {
                contact.userProfile.username = username;
                contact.userProfile.photoUrl = photoUrl;
                contact.userProfile.avatarUrl = avatarUrl;
                contact.userProfile.profession = profession;
                break;
            }
        }

        reloadContactList();
    }

    /**
     * Get conversations
     */
    private void getConversations() {
        String myId = FirebaseUtils.getUserId();
        if (myId == null)
            return;

        DatabaseReference databaseRef = FirebaseDatabase.getInstance().getReference();
        DatabaseReference conversationRef = databaseRef.child("conversations").child(myId);

        ChildEventListener childEventListener = conversationRef.orderByChild("timestamp")
                .addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                        if (dataSnapshot.getValue() != null) {
                            Conversation conversation = dataSnapshot.getValue(Conversation.class);
                            conversation.conversationId = dataSnapshot.getKey();

                            if (!conversation.isRead &&
                                    !TextUtils.isEmpty(conversation.lastMessage) &&
                                    !conversation.lastMessage.contains("You:"))
                                mCountOfUnreadMessages++;

                            setUserProfilesToConversation(conversation);
                        }
                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                        if (dataSnapshot.getValue() != null) {
                            //The conversation which is changed should always be on top.
                            Conversation conversation = dataSnapshot.getValue(Conversation.class);
                            for (Conversation item: mConversationsItems) {
                                if (item.conversationId.equals(dataSnapshot.getKey())) {
                                    conversation.userProfile = item.userProfile;
                                    conversation.conversationId = dataSnapshot.getKey();
                                    mConversationsItems.remove(item);
                                    mConversationsItems.add(0, conversation);
                                    break;
                                }
                            }

                            reloadConversationsList();
                        }
                    }

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {
                    }

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                    }
                });

        BaseDataManager.putChildEventListener(conversationRef, childEventListener);
    }

    /**
     * Search Conversations
     */
    private void searchConversations() {
        mSearchConversationsItems.clear();
        String searchText = etChatsSearchField.getText().toString();
        if (!TextUtils.isEmpty(searchText)) {
            for (Conversation conversation: mConversationsItems) {
                if (conversation.userProfile.username.contains(searchText) ||
                        conversation.userProfile.profession.contains(searchText))
                    mSearchConversationsItems.add(conversation);
            }

            mIsConversationSearchMode = true;
            mConversationsAdapter.setData(mSearchConversationsItems);
        } else {
            mIsConversationSearchMode = false;
            mConversationsAdapter.setData(mConversationsItems);
        }

        BaseUtils.hideSoftKeyBoard(getContext(), etChatsSearchField);

        llChatsSearchLayout.setVisibility(View.INVISIBLE);
        mConversationsAdapter.notifyDataSetChanged();
    }

    /**
     * Set user profiles to the discovery which was added.
     * @param conversation
     */
    private void setUserProfilesToConversation(final Conversation conversation) {
        FirebaseDatabase.getInstance().getReference().child("users").child(conversation.userId).child("userProfile")
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot snapshot) {
                        UserProfile userProfile = snapshot.getValue(UserProfile.class);
                        conversation.userProfile = userProfile;
                        mConversationsItems.add(0, conversation);
                        reloadConversationsList();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Toast.makeText(getContext(), "Can't get user profile!", Toast.LENGTH_LONG).show();
                    }
                });
    }

    /**
     * Reload conversations
     */
    private void reloadConversationsList() {
        tvConversationsCount.setText(String.valueOf(mConversationsItems.size()));
        if (mConversationsItems.size() > 0)
            lyNoConversationNotice.setVisibility(View.GONE);
        else
            lyNoConversationNotice.setVisibility(View.VISIBLE);

        BaseDataManager.getInstance().setConversationCount(mConversationsItems.size());
        if (mIsConversationSearchMode)
            return;

        mConversationsAdapter.setData(mConversationsItems);
        mConversationsAdapter.notifyDataSetChanged();
    }

    public void refreshDiscoveries() {
        mDiscoveriesItems.clear();
        reloadDiscoveryList();
        //getDiscoveries();
    }

    /**
     * Add discovery with user that got from country filter.[26/03/2018]
     */
    public void addDiscovery(User user, Location currentLocation) {
        Location userLocation = null;
        if (user.position != null) {
            userLocation = new Location("User Location");
            userLocation.setLatitude(user.position.latitude);
            userLocation.setLongitude(user.position.longitude);
        }

        addDiscovery(user, userLocation, currentLocation);
    }

    /**
     * Add Discovery
     */
    public void addDiscovery(User user, Location userLocation, Location currentLocation) {
        if (getContext() == null)
            return;

        if (!BaseDataManager.checkValidUser(user, getContext()))
            return;

        Discovery discovery = new Discovery();

        discovery.userKey = user.key;
        if (userLocation != null)
            discovery.distance = currentLocation.distanceTo(userLocation);
        else
            discovery.distance = -1;

        discovery.timestamp = System.currentTimeMillis();

        if (user.userProfile.isValid) {
            discovery.userProfile = user.userProfile;
            discovery.status = user.status;
            mDiscoveriesItems.add(discovery);

            sortDiscoveryArray();
            reloadDiscoveryList();
        }
    }

    /**
     * Sort Discovery by distance.
     */
    private void sortDiscoveryArray() {
        Collections.sort(mDiscoveriesItems, new Comparator<Discovery>() {
            @Override
            public int compare(Discovery a, Discovery b) {
                return Double.valueOf(a.distance).compareTo(Double.valueOf(b.distance));
            }
        });
    }

    /**
     * Search Discoveries
     */
    private void searchDiscoveries() {
        mSearchDiscoveriesItems.clear();
        String searchText = etDiscoveriesSearchField.getText().toString();
        if (!TextUtils.isEmpty(searchText)) {
            for (Discovery discovery: mDiscoveriesItems) {
                if (discovery.userProfile.username.contains(searchText) ||
                        discovery.userProfile.profession.contains(searchText))
                    mSearchDiscoveriesItems.add(discovery);
            }

            mIsDiscoverySearchMode = true;
            mDiscoveriesAdapter.setData(mSearchDiscoveriesItems);
        } else {
            mIsDiscoverySearchMode = false;
            mDiscoveriesAdapter.setData(mDiscoveriesItems);
        }

        BaseUtils.hideSoftKeyBoard(getContext(), etDiscoveriesSearchField);

        llDiscoveriesSearchLayout.setVisibility(View.INVISIBLE);
        mDiscoveriesAdapter.notifyDataSetChanged();
    }

    public void removeAllDiscoveries() {
        mDiscoveriesItems.clear();
    }

    /**
     * Reload Discovery List
     */
    private void reloadDiscoveryList() {
        tvDiscoveriesCount.setText(String.valueOf(mDiscoveriesItems.size()));
        if (mIsDiscoverySearchMode)
            return;

        mDiscoveriesAdapter.setData(mDiscoveriesItems);
        mDiscoveriesAdapter.notifyDataSetChanged();
    }

    /**
     * Get Contacts
     */
    private void getContacts() {
        String userId = FirebaseUtils.getUserId();
        DatabaseReference databaseRef = FirebaseDatabase.getInstance().getReference();
        final DatabaseReference contactsRef = databaseRef.child("contacts").child(userId);
        ChildEventListener childEventListener = contactsRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                if (dataSnapshot.getValue() != null) {
                    Contact contact = dataSnapshot.getValue(Contact.class);
                    contact.contactKey = dataSnapshot.getKey();

                    //Delete duplicated contacts.
                    //Some impetuous users might import contacts from settings as soon as you launch app.
                    //At that time, before calling getAllContacts() on MainActivity,
                    // the contacts might be added without checking whether it's duplicated or not.
                    //So, if they added again, they must be deleted.
                    //And now, the client doesn't want to show waiting dialog...
                    if (BaseDataManager.isDuplicatedContact(contact.userId)) {
                        contactsRef.child(contact.contactKey).removeValue();
                        return;
                    }

                    BaseDataManager.getInstance().addContact(contact);

                    setUserProfilesToContact(contact);
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });

        BaseDataManager.putChildEventListener(contactsRef, childEventListener);
    }

    /**
     * Set user profiles to the contact which was added.
     * @param contact
     */
    private void setUserProfilesToContact(final Contact contact) {
        FirebaseDatabase.getInstance().getReference().child("users").child(contact.userId).child("userProfile")
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot snapshot) {
                        UserProfile userProfile = snapshot.getValue(UserProfile.class);
                        contact.userProfile = userProfile;
                        mContactsItems.add(0, contact);
//                        if (!BaseDataManager.isDuplicatedContact(contact.userId))
//                            BaseDataManager.getInstance().addContact(contact);

                        reloadContactList();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Toast.makeText(getContext(), "Can't get user profile!", Toast.LENGTH_LONG).show();
                    }
                });
    }


    /**
     * Search Contacts
     */
    private void searchContacts() {
        mSearchContactsItems.clear();
        String searchText = etContactsSearchField.getText().toString();
        if (!TextUtils.isEmpty(searchText)) {
            for (Contact contact: mContactsItems) {
                if (contact.userProfile.username.contains(searchText) ||
                        contact.userProfile.profession.contains(searchText))
                    mSearchContactsItems.add(contact);
            }

            mIsContactSearchMode = true;
            mContactsAdapter.setData(mSearchContactsItems);
        } else {
            mIsContactSearchMode = false;
            mContactsAdapter.setData(mContactsItems);
        }

        BaseUtils.hideSoftKeyBoard(getContext(), etChatsSearchField);

        llContactsSearchLayout.setVisibility(View.INVISIBLE);
        mContactsAdapter.notifyDataSetChanged();
    }

    /**
     * Reload Contact List
     */
    private void reloadContactList() {
        tvContactsCount.setText(String.valueOf(mContactsItems.size()));
        if (mIsContactSearchMode)
            return;

        mContactsAdapter.setData(mContactsItems);
        mContactsAdapter.notifyDataSetChanged();
    }

    /**
     * Get the onScrollListener of Discoveries list.
     * Show search bar only while pulling up.
     * @return View.OnTouchListener
     */
    private View.OnTouchListener OnDiscoveriesTouchUpListener() {
        return new View.OnTouchListener() {
            float height;
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                float height = event.getY();
                if(action == MotionEvent.ACTION_DOWN){
                    this.height = height;
                }else if(action == MotionEvent.ACTION_UP){
                    if(this.height < height){
                        llDiscoveriesSearchLayout.setVisibility(View.INVISIBLE);
                    }else if(this.height > height + LIST_SCROLL_UP_DETECT_OFFSET){
                        if (mDiscoveriesItems.size() > 0)
                            llDiscoveriesSearchLayout.setVisibility(View.VISIBLE);
                    }
                }
                return false;
            }
        };
    }

    /**
     * Get the onScrollListener of Conversation list.
     * Show search bar only while pulling up.
     * @return AbsListView.OnScrollListener
     */
    private View.OnTouchListener OnConversationsTouchUpListener() {
        return new View.OnTouchListener() {
            float height;
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                float height = event.getY();
                if(action == MotionEvent.ACTION_DOWN){
                    this.height = height;
                }else if(action == MotionEvent.ACTION_UP){
                    if(this.height < height){
                        llChatsSearchLayout.setVisibility(View.INVISIBLE);
                    }else if(this.height > height + LIST_SCROLL_UP_DETECT_OFFSET){
                        if (mConversationsItems.size() > 0)
                            llChatsSearchLayout.setVisibility(View.VISIBLE);
                    }
                }
                return false;
            }
        };
    }

    /**
     * Get the onScrollListener of Contact list.
     * Show search bar only while pulling up.
     * @return AbsListView.OnScrollListener
     */
    private View.OnTouchListener OnContactsTouchUpListener() {
        return new View.OnTouchListener() {
            float height;
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                float height = event.getY();
                if(action == MotionEvent.ACTION_DOWN){
                    this.height = height;
                }else if(action == MotionEvent.ACTION_UP){
                    if(this.height < height){
                        llContactsSearchLayout.setVisibility(View.INVISIBLE);
                    }else if(this.height > height + LIST_SCROLL_UP_DETECT_OFFSET){
                        if (mContactsItems.size() > 0)
                            llContactsSearchLayout.setVisibility(View.VISIBLE);
                    }
                }
                return false;
            }
        };
    }


    @OnClick(R.id.btnGotoMap)
    public void onGotoMapClicked() {
        ((MainActivity)getActivity()).gotoMapFragment();
    }

    @OnClick(R.id.llProfileLayout)
    public void onClickedProfile(){
        MyProfileActivity.start(getContext());
        //Intent intent = new Intent(getContext(), InstagramActivity.class);
        //getContext().startActivity(intent);
    }

    @OnClick(R.id.llChatsTabLayout)
    public void onClickedConversations() {
        setCurrentMode(MODE_CONVERSATIONS);
    }

    @OnClick(R.id.llDiscoveriesTabLayout)
    public void onClickedDiscoveries() {
        setCurrentMode(MODE_DISCOVERIES);
    }

    @OnClick(R.id.llContactsTabLayout)
    public void onClickedContacts() {
        setCurrentMode(MODE_CONTACTS);
    }

    @OnClick(R.id.btnChatsSearch)
    public void onSearchConveration(){
        searchConversations();
    }

    @OnClick(R.id.btnDiscoverySearch)
    public void onSearchDiscovery(){
        searchDiscoveries();
    }

    @OnClick(R.id.btnContactSearch)
    public void onSearchContact(){
        searchContacts();
    }


    /**
     * Set Tab View
     * @param index
     */
    private void setCurrentMode(int index) {
        mLastFirstVisibleItem = 0;
        for (int i = 0; i < mTabItems.size(); i ++) {
            TabItemHolder tabItemHolder = mTabItems.get(i);
            if (i == index) {
                tabItemHolder.setActive(true);
            } else {
                tabItemHolder.setActive(false);
            }
        }
    }

    /**
     * Building tab controls.
     */
    private void setTabItems() {
        if (mTabItems != null && mTabItems.size() == 3)
            return;

        TabItemHolder chatItem = new TabItemHolder();
        chatItem.mRootView = llChatsLayout;
        chatItem.mTabView = llChatsTabLayout;
        chatItem.mCountTextView = tvConversationsCount;
        chatItem.mTitleTextView = tvConversationsTitle;
        mTabItems.add(chatItem);

        TabItemHolder discoveryItem = new TabItemHolder();
        discoveryItem.mRootView = rlDiscoveriesLayout;
        discoveryItem.mTabView = llDiscoveriesTabLayout;
        discoveryItem.mCountTextView = tvDiscoveriesCount;
        discoveryItem.mTitleTextView = tvDiscoveriesTitle;
        mTabItems.add(discoveryItem);

        TabItemHolder contactItem = new TabItemHolder();
        contactItem.mRootView = llContactsLayout;
        contactItem.mTabView = llContactsTabLayout;
        contactItem.mCountTextView = tvContactsCount;
        contactItem.mTitleTextView = tvContactsTitle;
        mTabItems.add(contactItem);
    }

    /**
     * Tab item holder class.
     */
    class TabItemHolder {
        View mRootView;
        View mTabView;
        TextView mCountTextView;
        TextView mTitleTextView;

        void setActive(boolean isActive) {
            if (isActive) {
                mRootView.setVisibility(View.VISIBLE);

                int colorRid = mTabView.getContext().getResources().getColor(ThemeManager.getInstance().getCurrentThemeColor());
                mCountTextView.setTextColor(colorRid);
                mTitleTextView.setTextColor(colorRid);

                setTabColor();
            } else {
                mRootView.setVisibility(View.INVISIBLE);
                mTabView.setBackgroundDrawable(getResources().getDrawable(R.drawable.top_tab_bg));
                mCountTextView.setTextColor(getResources().getColor(R.color.normalTextGrayColor));
                mTitleTextView.setTextColor(getResources().getColor(R.color.normalTextGrayColor));
            }
        }

        void setTabColor() {
            LayerDrawable layerDrawable = (LayerDrawable)mTabView.getContext().getResources().getDrawable(R.drawable.top_tab_selected_bg);
            GradientDrawable shapeDrawable = (GradientDrawable) layerDrawable.findDrawableByLayerId(R.id.tab_shape_color);
            shapeDrawable.setColor(mTabView.getContext().getResources().getColor(ThemeManager.getInstance().getCurrentThemeColor()));
            mTabView.setBackgroundDrawable(getResources().getDrawable(R.drawable.top_tab_selected_bg));
        }
    }
}
