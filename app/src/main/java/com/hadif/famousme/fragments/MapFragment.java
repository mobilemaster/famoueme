package com.hadif.famousme.fragments;


import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.arsy.maps_library.MapRipple;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.firebase.geofire.GeoFire;
import com.firebase.geofire.GeoLocation;
import com.firebase.geofire.GeoQuery;
import com.firebase.geofire.GeoQueryEventListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterItem;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;
import com.google.maps.android.ui.IconGenerator;
import com.hadif.famousme.AppConfig;
import com.hadif.famousme.R;
import com.hadif.famousme.activities.ChatActivity;
import com.hadif.famousme.activities.GettingStartedActivity;
import com.hadif.famousme.activities.InviteFriendsActivity;
import com.hadif.famousme.activities.UserProfileActivity;
import com.hadif.famousme.adapters.MapLocationsAdapter;
import com.hadif.famousme.managers.basedata.BaseDataManager;
import com.hadif.famousme.managers.basedata.ThemeManager;
import com.hadif.famousme.models.BlockUser;
import com.hadif.famousme.models.Discovery;
import com.hadif.famousme.models.DiscoveryRules;
import com.hadif.famousme.models.MapLocationItem;
import com.hadif.famousme.models.MarkerItem;
import com.hadif.famousme.models.User;
import com.hadif.famousme.utils.BaseUtils;
import com.hadif.famousme.utils.DateUtils;
import com.hadif.famousme.utils.FirebaseUtils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import jp.wasabeef.glide.transformations.CropCircleTransformation;

/**
 * A simple {@link Fragment} subclass.
 */
public class MapFragment extends BasicFragment implements OnMapReadyCallback, ClusterManager.OnClusterClickListener<MarkerItem>{

    public static MapFragment newInstance() {
        return new MapFragment();
    }

    private static final String TAG = "MapFragment";

    private class ClusterRenderer extends DefaultClusterRenderer<MarkerItem> {
        private final IconGenerator mIconGenerator = new IconGenerator(getContext());
        private final IconGenerator mClusterIconGenerator = new IconGenerator(getContext());
        private final ImageView mImageView;
        private final ImageView mClusterImageView;
        private final int mDimension;

        public ClusterRenderer() {
            super(getContext(), mGoogleMap, mClusterManager);

            View multiProfile = getActivity().getLayoutInflater().inflate(R.layout.layout_cluster_marker, null);
            mClusterIconGenerator.setContentView(multiProfile);
            mClusterImageView = (ImageView) multiProfile.findViewById(R.id.image);

            mImageView = new ImageView(getContext());
            mDimension = (int) getResources().getDimension(R.dimen.custom_cluster_image);
            mImageView.setLayoutParams(new ViewGroup.LayoutParams(mDimension, mDimension));
            int padding = (int) getResources().getDimension(R.dimen.custom_cluster_padding);
            mImageView.setPadding(padding, padding, padding, padding);
            mIconGenerator.setContentView(mImageView);
        }

        @Override
        protected void onBeforeClusterItemRendered(MarkerItem item, MarkerOptions markerOptions) {
            markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.famousme)).title(item.getTitle());
        }

        @Override
        protected void onClusterItemRendered(MarkerItem markerItem, Marker marker) {
            super.onClusterItemRendered(markerItem, marker);
            marker.setTag(markerItem.getIndex());
        }

        @Override
        protected void onBeforeClusterRendered(Cluster<MarkerItem> cluster, MarkerOptions markerOptions) {
            mClusterIconGenerator.setBackground(null);
            Activity activity = getActivity();

            //For Crash Error, when the app is just turned into foreground mode.
            //Crashing below [30.04.2018]
            //java.lang.IllegalStateException: Fragment MapFragment{5a3aea2} not attached to Activity
            //at android.support.v4.app.Fragment.getResources(Fragment.java:608)
            if(activity != null && isAdded()) {
                mClusterImageView.setImageDrawable(getResources().getDrawable(R.drawable.famousme));
                Bitmap icon = mClusterIconGenerator.makeIcon(String.valueOf(cluster.getSize()));
                markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon));
            }
        }


        @Override
        protected boolean shouldRenderAsCluster(Cluster cluster) {
            // Always render clusters.
            return cluster.getSize() > 1;
        }

    }

    //UI Control
    private View mRootView = null;
    @BindView(R.id.rlHeader)                RelativeLayout rlHeader;
    @BindView(R.id.lyMainLayout)            View lyMainLayout;
    @BindView(R.id.lyLoadingSplash)         View lyLoadingSplash;
    @BindView(R.id.lyNoBody)                View lyNoBody;
    @BindView(R.id.mvMapView)               MapView mMapView;
    @BindView(R.id.lvMapLocationsList)      ListView lvMapLocationsList;
    @BindView(R.id.rlMapViewLayout)         View rlMapViewLayout;
    @BindView(R.id.btnLeft)                 ImageView btnLeft;
    @BindView(R.id.nobodyImageView)         ImageView nobodyImageView;
    @BindView(R.id.loadImageView)           ImageView loadImageView;
    @BindView(R.id.btnInviteFriends)        TextView btnInviteFriends;

    //[START - View User Profile]
    @BindView(R.id.lyViewProfileLayout)     View lyViewProfileLayout;
    @BindView(R.id.btnViewProfile)          TextView btnViewProfile;
    @BindView(R.id.ivProfilePhoto)          ImageView ivProfilePhoto;
    @BindView(R.id.tvName)                  TextView tvName;
    @BindView(R.id.tvProfession)            TextView tvProfession;
    @BindView(R.id.tvOnlineStatus)          TextView tvOnlineStatus;
    @BindView(R.id.tvDistanceStatus)        TextView tvDistanceStatus;
    //[END - View User Profile]
    //[START - Received message notice]
    @BindView(R.id.lyMessageNoticeLayout)   View lyMessageNoticeLayout;
    @BindView(R.id.ivSenderPhoto)           ImageView ivSenderPhoto;
    @BindView(R.id.tvMessageTitle)          TextView tvMessageTitle;
    @BindView(R.id.tvMessageText)           TextView tvMessageText;

    //Private Members
    private GoogleMap mGoogleMap;
    private MapRipple mMapRipple;

    private Location mCurrentLocation;
    private MapLocationsAdapter mMapLocationsAdapter;

    //Check If the map view is showed or not at present.
    boolean mIsCurrentMapView;

    //User's key which is got by GeoFire querying.
    private ArrayList<MapLocationItem> mNearValidUserList = new ArrayList<>();
    private int mCurrentUserIndex = 0;
    private final int SPLASH_DISPLAY_LENGTH = 5000;
    private int mMinZoom = 15;

    private ClusterManager<MarkerItem> mClusterManager;

    public MapFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (mRootView == null) {
            // Inflate the layout for this fragment
            mRootView = inflater.inflate(R.layout.fragment_map, container, false);
            ButterKnife.bind(this, mRootView);

            mMapView.onCreate(savedInstanceState);
            mMapView.onResume();

            try {
                MapsInitializer.initialize(getActivity().getApplicationContext());
            } catch (Exception e) {
                e.printStackTrace();
            }

            mMapLocationsAdapter = new MapLocationsAdapter(getContext());
            lvMapLocationsList.setAdapter(mMapLocationsAdapter);
            lvMapLocationsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
                    MapLocationItem locationItem = (MapLocationItem)lvMapLocationsList.getItemAtPosition(position);
                    ChatActivity.start(getActivity(),
                            locationItem.user.userProfile.username,
                            locationItem.user.key,
                            locationItem.user.userProfile.isValid);
                }
            });

            mMapLocationsAdapter.setData(mNearValidUserList);
            mMapLocationsAdapter.setOnItemSelectedListener(new MapLocationsAdapter.OnItemSelectedListener() {
                @Override
                public void onItemSelected(MapLocationItem mapLocationItem) {

                }
            });

            mIsCurrentMapView = true;
            mMapView.getMapAsync(this);

            mListener.onComplete();
        }

        applyTheme();
        return mRootView;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mMapView.onSaveInstanceState(outState);
    }

    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mMapRipple != null && mMapRipple.isAnimationRunning()) {
            mMapRipple.stopRippleMapAnimation();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

    //For Theme
    private void applyTheme() {
        ThemeManager.getInstance().setThemeWithChildViews(rlHeader);
        loadImageView.setImageResource(ThemeManager.getLoadingIcon());
        nobodyImageView.setImageResource(ThemeManager.getLoadingIcon());

        ThemeManager.getInstance().setMainColorButton(btnInviteFriends);
        ThemeManager.getInstance().setMainColorButton(btnViewProfile);
    }

    /**
     * Show splash for loading map
     */
    public void setLoadingSplash() {
        if (lyLoadingSplash == null)
            return;

        lyLoadingSplash.setVisibility(View.VISIBLE);

        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {
                /* Create an Intent that will start the Menu-Activity. */
                lyLoadingSplash.setVisibility(View.GONE);
                if (mNearValidUserList.size() == 0)
                    lyNoBody.setVisibility(View.VISIBLE);
                else
                    lyNoBody.setVisibility(View.GONE);

            }
        }, SPLASH_DISPLAY_LENGTH);
    }

    /**
     * Get GeoLocation instance from current location.
     * @return GeoLocation
     */
    private GeoLocation getGeoLocation() {
        return new GeoLocation(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude());
    }

    /**
     * Update user location every 5 minutes.
     * The interval can be controlled by UPDATE_INTERVAL_IN_MILLISECONDS.
     */
    public void updateUserLocation(Location location) {
        if (mGoogleMap == null)
            return;

        //Update my location
        mCurrentLocation = location;

        User user = BaseDataManager.getInstance().getUser();
        if (user != null && user.discoveryRules.discoverableMap) {
            //Update position on database.
            GeoFire geoFire = FirebaseUtils.getGeoFire();
            String userId = FirebaseAuth.getInstance().getCurrentUser().getUid();
            geoFire.setLocation(userId, getGeoLocation(), new GeoFire.CompletionListener() {
                @Override
                public void onComplete(String key, DatabaseError error) {

                }
            });

            //Add position to user data [29/03/2018]
            Map<String, Object> children = new HashMap<>();
            children.put("latitude", location.getLatitude());
            children.put("longitude", location.getLongitude());

            FirebaseDatabase.getInstance().getReference().child("users")
                    .child(userId).child("position").updateChildren(children);
        }

        Log.e(TAG, "Location---Updated------- ");

        mGoogleMap.clear();
        if (mMapRipple != null) {
            mMapRipple.stopRippleMapAnimation();
            mMapRipple = null;

        }

        mClusterManager.clearItems();
        lyViewProfileLayout.setVisibility(View.GONE);

        final LatLng myLocation = new LatLng(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude());

        if (getContext() == null || ((Activity)getContext()).isFinishing())
            return;

        Glide.with(getContext())
                .load(user.userProfile.avatarUrl)//URL From Serverside
                .asBitmap()
                .override(200, 200)
                .fitCenter()
                .into(new SimpleTarget<Bitmap>() {

                    @Override
                    public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                        MarkerOptions options = new MarkerOptions()
                                .anchor(0.5f, 0.5f)
                                .position(myLocation)
                                .draggable(false)
                                .icon(BitmapDescriptorFactory.fromBitmap(getRoundBorderBitmap(resource)));

                        mGoogleMap.addMarker(options);
                    }
                });


        //mGoogleMap.addMarker(new MarkerOptions().position(myLocation).title("My location"));

        // For zooming automatically to the location of the marker
        CameraPosition cameraPosition = new CameraPosition.Builder().target(myLocation).zoom(15).build();
        mGoogleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

        Circle circle = mGoogleMap.addCircle(new CircleOptions().center(myLocation).
                                        radius(BaseUtils.getRadiusInMetres()).strokeColor(0xFF12b1ab));
        circle.setFillColor(0x2612b1ab);
        circle.setVisible(true);

        int minZoom = BaseUtils.getZoomLevel(circle);
        //it makes so that user can zoom by limit to radius.
        mGoogleMap.setMinZoomPreference(minZoom);

        //Adding Pulse.
        mMapRipple =  new MapRipple(mGoogleMap, myLocation, getContext());
        mMapRipple.withStrokewidth(0);
        mMapRipple.withDistance(BaseUtils.getRadiusInMetres()* 2);
        mMapRipple.withRippleDuration(5000);
        mMapRipple.withFillColor(0x7312dcbe);
        mMapRipple.startRippleMapAnimation();      //in onMapReadyCallBack
        //Get near by users
        //getNearUsers();
    }

    private Bitmap getRoundBorderBitmap(Bitmap bitmap) {
        int w = bitmap.getWidth();
        int h = bitmap.getHeight();

        int radius = Math.min(h / 2, w / 2);
        Bitmap output = Bitmap.createBitmap(w + 8, h + 8, Bitmap.Config.ARGB_8888);

        Paint p = new Paint();
        p.setAntiAlias(true);

        Canvas c = new Canvas(output);
        c.drawARGB(0, 0, 0, 0);
        p.setStyle(Paint.Style.FILL);

        c.drawCircle((w / 2) + 4, (h / 2) + 4, radius, p);

        p.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));

        c.drawBitmap(bitmap, 4, 4, p);
        p.setXfermode(null);
        p.setStyle(Paint.Style.STROKE);
        p.setColor(0xFF12b1ab);
        p.setStrokeWidth(3);
        c.drawCircle((w / 2) + 4, (h / 2) + 4, radius, p);

        return output;
    }

    @Override
    public void onMapReady(GoogleMap mMap) {
        mGoogleMap = mMap;
        mGoogleMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener(){
            @Override
            public void onInfoWindowClick(Marker marker){
                if (marker.getTag() != null) {
                    int indexOfUser = (int)(marker.getTag());
                    setUserProfileView(indexOfUser);
                }
            }
        });

        // For showing a move to my location button
        mClusterManager = new ClusterManager<MarkerItem>(getContext(), mGoogleMap);
        mClusterManager.setRenderer(new ClusterRenderer());
        mGoogleMap.setOnCameraIdleListener(mClusterManager);
        mGoogleMap.setOnMarkerClickListener(mClusterManager);
        mClusterManager.setOnClusterClickListener(this);
        //mClusterManager.setOnClusterInfoWindowClickListener(this);
        //mClusterManager.setOnClusterItemClickListener(this);
        //mClusterManager.setOnClusterItemInfoWindowClickListener(this);

        mClusterManager.cluster();
    }

    @Override
    public boolean onClusterClick(Cluster<MarkerItem> cluster) {
        // Zoom in the cluster. Need to create LatLngBounds and including all the cluster items
        // inside of bounds, then animate to center of the bounds.

        // Create the builder to collect all essential cluster items for the bounds.
        LatLngBounds.Builder builder = LatLngBounds.builder();
        for (ClusterItem item : cluster.getItems()) {
            builder.include(item.getPosition());
        }
        // Get the LatLngBounds
        final LatLngBounds bounds = builder.build();

        // Animate camera to the bounds
        try {
            mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 100));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return true;
    }

    /**
     * Add markers
     * @param latitude
     * @param longitude
     * @param indexOfItem
     * @param username
     */
    private void addMarkers(double latitude, double longitude, int indexOfItem, String username) {
        if (mGoogleMap == null)
            return;

        Log.e("index--------", String.valueOf(indexOfItem));
        mClusterManager.addItem(new MarkerItem(latitude, longitude, username, indexOfItem));
        mClusterManager.cluster();
    }

    /**
     * Get users near by.
     */
    private void getNearUsers() {
        //Remove the old query.
        BaseDataManager.getInstance().removeGeoQuery();

        GeoFire geoFire = FirebaseUtils.getGeoFire();
        GeoQuery geoQuery = geoFire.queryAtLocation(getGeoLocation(), AppConfig.Location.NEAR_FINDING_RADIUS);
        BaseDataManager.getInstance().setGeoQuery(geoQuery); //For remove all event listener.
        geoQuery.addGeoQueryEventListener(new GeoQueryEventListener() {
            @Override
            public void onKeyEntered(String key, GeoLocation location) {
                //System.out.println(String.format("Key %s entered the search area at [%f,%f]", key, location.latitude, location.longitude));
                String userId = FirebaseAuth.getInstance().getCurrentUser().getUid();
                if (key.equals(userId))
                    return;

                getUserWithKey(key, location);
            }

            @Override
            public void onKeyExited(String key) {
                //System.out.println(String.format("Key %s is no longer in the search area", key));
            }

            @Override
            public void onKeyMoved(String key, GeoLocation location) {
                //System.out.println(String.format("Key %s moved within the search area to [%f,%f]", key, location.latitude, location.longitude));
            }

            @Override
            public void onGeoQueryReady() {
                //System.out.println("All initial data has been loaded and events have been fired!");
            }

            @Override
            public void onGeoQueryError(DatabaseError error) {
                System.err.println("There was an error with this query: " + error);
            }
        });

    }

    /**
     * Check if the user is already added in the list or not.
     * @param key
     * @return boolean
     */
    private boolean isAlreadyAdded(String key) {
        if (getIndexOfNearUser(key) > -1)
            return true;

        return false;
    }

    /**
     * Get the index of the current user
     * @param key
     * @return
     */
    private int getIndexOfNearUser(String key) {
        for (int i = 0; i < mNearValidUserList.size(); i++) {
            MapLocationItem item = mNearValidUserList.get(i);
            if (key.equals(item.user.key))
                return i;
        }

        return -1;
    }

    /**
     * Refresh map location list after changing discovery rules.
     */
    public void refreshMapLocation() {
        if (mNearValidUserList != null) {
            mNearValidUserList.clear();
            reloadNearByUsersList();
        }

        if (mGoogleMap != null)
            mGoogleMap.clear();

        if (mClusterManager != null)
            mClusterManager.clearItems();
    }

    /**
     * Get user with key
     * @param key
     */
    private void getUserWithKey(final String key, final GeoLocation location) {

        FirebaseDatabase.getInstance().getReference().child("users").child(key)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot snapshot) {
                        if (snapshot == null)
                            return;

                        User userData = snapshot.getValue(User.class);
                        userData.key = snapshot.getKey();

                        if (!BaseDataManager.checkValidUser(userData, getContext()))
                            return;

                        if (lyNoBody.getVisibility() == View.VISIBLE)
                            lyNoBody.setVisibility(View.GONE);

                        int index = getIndexOfNearUser(userData.key);
                        if (index >= 0) {
                            MapLocationItem item = mNearValidUserList.get(index);
                            item.setData(userData, location, mCurrentLocation);

                            reloadNearByUsersList();
                            updateDiscoveriesData(item);

                            if (!item.user.discoveryRules.isAnonymousMode)
                                addMarkers(item.latitude, item.longitude, index, item.user.userProfile.username);

                        } else {
                            MapLocationItem item = new MapLocationItem();
                            item.setData(userData, location, mCurrentLocation);
                            mNearValidUserList.add(0, item);

                            reloadNearByUsersList();
                            updateDiscoveriesData(item);
                            if (!item.user.discoveryRules.isAnonymousMode)
                                addMarkers(item.latitude, item.longitude,
                                        mNearValidUserList.size() - 1,
                                        item.user.userProfile.username);
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Toast.makeText(getActivity(), "Can't get user data!", Toast.LENGTH_LONG).show();
                    }
                });

    }

    public void addNearUser(User user, final GeoLocation location) {
        if (lyNoBody != null && lyNoBody.getVisibility() == View.VISIBLE)
            lyNoBody.setVisibility(View.GONE);

        if (location == null || mCurrentLocation == null || user == null) //[Crash] Xiaomi
            return;

        MapLocationItem item = new MapLocationItem();
        item.setData(user, location, mCurrentLocation);

        //Only the users who located in 1km radius should be shown.
        if (item.distance > 1000)
            return;

        mNearValidUserList.add(item);

        reloadNearByUsersList();
        if (!item.user.discoveryRules.isAnonymousMode)
            addMarkers(item.latitude, item.longitude,
                    mNearValidUserList.size() - 1,
                    item.user.userProfile.username);
    }

    /**
     * Reload map location list view.
     */
    private void reloadNearByUsersList() {
        if (mMapLocationsAdapter == null || mNearValidUserList == null)
            return;

        mMapLocationsAdapter.setData(mNearValidUserList);
        mMapLocationsAdapter.notifyDataSetChanged();
    }

    /**
     * Update discoveries data
     * @param mapLocationItem
     */
    private void updateDiscoveriesData(MapLocationItem mapLocationItem) {
        Map<String, Object> children = new HashMap<>();
        children.put("distance", mapLocationItem.distance);
        if (!BaseDataManager.isDuplicatedDiscovery(mapLocationItem.user.key))
            children.put("timestamp", ServerValue.TIMESTAMP);

        String userId = FirebaseAuth.getInstance().getCurrentUser().getUid();
        FirebaseDatabase.getInstance().getReference()
                        .child("discoveries").child(userId)
                        .child(mapLocationItem.user.key).updateChildren(children);
    }


    /**
     * Set User profile when the marker is clicked.
     * @param indexOfUser
     */
    private void setUserProfileView(int indexOfUser) {
        MapLocationItem item = mNearValidUserList.get(indexOfUser);
        if (item == null)
            return;

        mCurrentUserIndex = indexOfUser;

        lyViewProfileLayout.setVisibility(View.VISIBLE);
        Glide.with(getContext()).load(item.user.userProfile.photoUrl)
                .bitmapTransform(new CropCircleTransformation(getContext()))
                .into(ivProfilePhoto);

        tvName.setText(item.user.userProfile.username);
        tvProfession.setText(item.user.userProfile.profession);

        if (item.user.status.isOnline) {
            tvOnlineStatus.setText(R.string.online_now);
            tvOnlineStatus.setTextColor(getContext().getResources().getColor(R.color.mainColor));
        } else {
            tvOnlineStatus.setText(DateUtils.getGapOfTimesWithString(item.user.status.timestamp, getContext()));
            tvOnlineStatus.setTextColor(getContext().getResources().getColor(R.color.contentTextColor));
        }

        tvDistanceStatus.setText(" - " + BaseUtils.getDistanceString((int)(item.distance), getContext()));
    }

    /**
     * Show user profile
     */
    private void showUserProfile() {
        MapLocationItem item = mNearValidUserList.get(mCurrentUserIndex);

        //Send a notification
        FirebaseUtils.sendNotification(item.user.key, AppConfig.Notification.NOTIFICATION_VIEWED_PROFILE);

        //Go to user profile page
        //String timeGap = DateUtils.getGapOfTimesWithString(item.user.status.timestamp, getContext());
        String distance = BaseUtils.getDistanceString((int)(item.distance), getContext());
        UserProfileActivity.start(getContext(), false, /*timeGap + " - " +*/ distance, item.user.key);

        lyViewProfileLayout.setVisibility(View.GONE);
    }

    @OnClick(R.id.btnLeft)
    public void onChangeViewMode() {
        mIsCurrentMapView = !mIsCurrentMapView;
        if (mIsCurrentMapView) {
            rlMapViewLayout.setVisibility(View.VISIBLE);
            lvMapLocationsList.setVisibility(View.INVISIBLE);
            btnLeft.setImageResource(R.drawable.list_simple_line_icon);
        } else {
            rlMapViewLayout.setVisibility(View.INVISIBLE);
            lvMapLocationsList.setVisibility(View.VISIBLE);
            btnLeft.setImageResource(R.drawable.map_active_icon);
        }
    }

    @OnClick(R.id.btnViewProfile)
    public void onUserProfileViewClicked() {
        showUserProfile();
    }

    @OnClick(R.id.btnInviteFriends)
    public void onInviteFriends() {
        InviteFriendsActivity.start(getActivity());
    }

}
