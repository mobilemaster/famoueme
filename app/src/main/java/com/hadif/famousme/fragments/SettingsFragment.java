package com.hadif.famousme.fragments;


import android.Manifest;
import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.geofire.GeoFire;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.hadif.famousme.AppConfig;
import com.hadif.famousme.R;
import com.hadif.famousme.activities.MainActivity;
import com.hadif.famousme.adapters.InterestsAdapter;
import com.hadif.famousme.managers.basedata.BaseDataManager;
import com.hadif.famousme.managers.basedata.ThemeManager;
import com.hadif.famousme.models.ContactObject;
import com.hadif.famousme.models.InterestItem;
import com.hadif.famousme.models.User;
import com.hadif.famousme.utils.FirebaseUtils;
import com.hadif.famousme.utils.pickers.CountryPicker;
import com.hadif.famousme.utils.pickers.CountryPickerListener;
import com.views.segmentedcontrol.SegmentedGroup;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.hadif.famousme.AppConfig.DiscoveryMode.COUNTRY_DISCOVERY_MODE;
import static com.hadif.famousme.AppConfig.DiscoveryMode.RANGE_DISCOVERY_MODE;

/**
 * A simple {@link Fragment} subclass.
 */
public class SettingsFragment extends BasicFragment implements RadioGroup.OnCheckedChangeListener{

    public static SettingsFragment newInstance() {
        return new SettingsFragment();
    }

    //UI Control
    @BindView(R.id.rlHeader)                      RelativeLayout rlHeader;
    @BindView(R.id.btnImportAddressValue)         TextView btnImportAddressValue;
    @BindView(R.id.swtAnonymousMode)              Switch swtAnonymousMode;
    @BindView(R.id.spSelectGender)                Spinner spSelectGender;
    @BindView(R.id.sgDiscoveryMode)               SegmentedGroup sgDiscoveryMode;
    @BindView(R.id.spDiscoveryRange)              Spinner spDiscoveryRange;
    @BindView(R.id.btnRange)                      RadioButton btnRange;
    @BindView(R.id.btnCountry)                    RadioButton btnCountry;
    @BindView(R.id.tvDiscoveryModeLabel)          TextView tvDiscoveryModeLabel;
    @BindView(R.id.tvCountryFilter)               TextView tvCountryFilter;
    @BindView(R.id.spOnlineFilter)                Spinner spOnlineFilter;
    @BindView(R.id.spServiceFilter)               Spinner spServiceFilter;
    @BindView(R.id.spAgeRange)                    Spinner spAgeRange;
    @BindView(R.id.spSelectNationality)           Spinner spSelectNationality;
    @BindView(R.id.spSelectInterests)             TextView spSelectInterests;
    @BindView(R.id.swtDiscoverableMap)            Switch swtDiscoverableMap;
    @BindView(R.id.swtPush)                       Switch swtPush;
    @BindView(R.id.swtNotifications)              Switch swtNotifications;
    @BindView(R.id.swtShowPhoneNumber)            Switch swtShowPhoneNumber;
    @BindView(R.id.swtCanSeeAge)                  Switch swtCanSeeAge;

    //Theme Color
    @BindView(R.id.showThemeView)                 View showThemeView;
    @BindView(R.id.btnSalmonColor)                Button btnSalmonColor;
    @BindView(R.id.btnMediumGreenColor)           Button btnMediumGreenColor;
    @BindView(R.id.btnBlueColor)                  Button btnBlueColor;
    @BindView(R.id.btnHotPinkColor)               Button btnHotPinkColor;
    @BindView(R.id.btnDarkIndigoColor)            Button btnDarkIndigoColor;
    @BindView(R.id.btnDarkSalmonColor)            Button btnDarkSalmonColor;
    @BindView(R.id.btnBlueVioletColor)            Button btnBlueVioletColor;
    @BindView(R.id.btnSlatGrayColor)              Button btnSlatGrayColor;
    @BindView(R.id.btnRoyalBlueColor)             Button btnRoyalBlueColor;

    //Privates Members
    public final static int REQUEST_READ_CONTACTS = 1234;
    private ArrayList<String> mInterestList = new ArrayList<>();

    private String mCountryFilter;
    private int mDiscoveryMode;
    private int mCurrentThemeType;

    public SettingsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_discovery_rules, container, false);
        ButterKnife.bind(this, rootView);

        mListener.onComplete();

        sgDiscoveryMode.setOnCheckedChangeListener(this);

        mCurrentThemeType = R.color.salmon_color;

        showThemeView.setBackgroundColor(getContext().getResources().getColor(ThemeManager.getInstance().getCurrentThemeColor()));
        applyTheme();
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();

        setUI();
    }

    private void applyTheme() {
        ThemeManager.getInstance().setThemeWithChildViews(rlHeader);
        sgDiscoveryMode.setTintColor(getContext().getResources().getColor(ThemeManager.getInstance().getCurrentThemeColor()));

    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId) {
            case R.id.btnRange:
                setDiscoveryMode(RANGE_DISCOVERY_MODE);
                break;
            case R.id.btnCountry:
                setDiscoveryMode(COUNTRY_DISCOVERY_MODE);

                break;
            default:
                // Nothing to do
        }
    }

    /**
     * Set UI controls with user's value
     */
    public void setUI() {
        User user = BaseDataManager.getInstance().getUser();
        if (user == null || user.discoveryRules == null)
            return;

        swtAnonymousMode.setChecked(user.discoveryRules.isAnonymousMode);
        spSelectGender.setSelection(user.discoveryRules.discoverGender);
        spAgeRange.setSelection(user.discoveryRules.ageRange);

        //Discovery Mode
        if (user.discoveryRules.discoveryMode == RANGE_DISCOVERY_MODE)
            btnRange.setChecked(true);
        else if (user.discoveryRules.discoveryMode == COUNTRY_DISCOVERY_MODE)
            btnCountry.setChecked(true);

        spDiscoveryRange.setSelection(user.discoveryRules.discoveryRange);

        //Set Nationality
        ArrayList<String> nationalityList =  new ArrayList<String>(BaseDataManager.getInstance().getNationalityList());
        nationalityList.add(0, getString(R.string.all));

        ArrayAdapter<String> nationalityAdapter = new ArrayAdapter<String>(
                getContext(), android.R.layout.simple_spinner_dropdown_item, nationalityList);
        spSelectNationality.setAdapter(nationalityAdapter);
        int nationalityIndex = BaseDataManager.getNationalityIndex(user.discoveryRules.discoverNationality);
        if (nationalityIndex >= 0)
            spSelectNationality.setSelection(nationalityIndex + 1); //All item is added.

        User currentUser = BaseDataManager.getInstance().getUser();
        ArrayList<String> interestsList = BaseDataManager.getInstance().getInterestList();
        if (mInterestList == null || mInterestList.size() == 0) {
            if (currentUser.discoveryRules.discoveryInterest.equals("All"))
                mInterestList = new ArrayList<>(interestsList);
            else
                mInterestList = new ArrayList<>(Arrays.asList(currentUser.discoveryRules.discoveryInterest.split("/")));
        }

        swtDiscoverableMap.setChecked(user.discoveryRules.discoverableMap);
        swtPush.setChecked(user.discoveryRules.pushNotification);
        swtNotifications.setChecked(user.discoveryRules.notifications);
        swtShowPhoneNumber.setChecked(user.discoveryRules.showPhoneNumber);
        swtCanSeeAge.setChecked(user.discoveryRules.canSeeMyAge);
        if (!TextUtils.isEmpty(user.discoveryRules.countryFilter))
            mCountryFilter = user.discoveryRules.countryFilter;
        else if (!TextUtils.isEmpty(user.countryCode))
            mCountryFilter = user.countryCode;
        else
            mCountryFilter = "+1";
    }

    private void setDiscoveryMode(int mode) {
        mDiscoveryMode = mode;
        User user = BaseDataManager.getInstance().getUser();
        if (mode == RANGE_DISCOVERY_MODE) {
            tvDiscoveryModeLabel.setText(R.string.range);
            tvCountryFilter.setVisibility(View.GONE);
            spDiscoveryRange.setVisibility(View.VISIBLE);

        } else if (mode == COUNTRY_DISCOVERY_MODE) {
            tvDiscoveryModeLabel.setText(R.string.country);
            tvCountryFilter.setVisibility(View.VISIBLE);
            spDiscoveryRange.setVisibility(View.GONE);

            if (user.discoveryRules == null) //[Crash]
                return;

            if (!TextUtils.isEmpty(user.discoveryRules.countryFilter)) {
                tvCountryFilter.setText(BaseDataManager.getCountryNameByCode(user.discoveryRules.countryFilter, getContext()));
            } else {
                if (!TextUtils.isEmpty(user.countryCode))
                    tvCountryFilter.setText(BaseDataManager.getCountryNameByCode(user.countryCode, getContext()));
                else
                    tvCountryFilter.setText("");
            }
        }
    }

    private void setCountry(String name, String code) {
        mCountryFilter = code;
        tvCountryFilter.setText(name);
    }

    /**
     * Select discovery interests
     */
    private void selectInterests() {
        //[START - Prepare Dialog]
        final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(getString(R.string.select_interests));
        builder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });

        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.layout_dialog_select_interests, null);
        builder.setView(dialogView);
        final AlertDialog dialog = builder.create();
        ListView lvInterestList = (ListView) dialogView.findViewById(R.id.lvInterestList);
        //[END - Prepare Dialog]

        //[START - Set Data]
        ArrayList<String> interestsList = BaseDataManager.getInstance().getInterestList();

        final ArrayList<InterestItem> interestItems = new ArrayList<>();
        for (String interestName: interestsList) {
            boolean isChecked = false;
            for (String oldInterest: mInterestList) {
                if (interestName.equals(oldInterest)) {
                    isChecked = true;
                    break;
                }
            }
            InterestItem item = new InterestItem(interestName, isChecked);
            interestItems.add(item);
        }

        final InterestsAdapter interestsAdapter = new InterestsAdapter(getContext());
        interestsAdapter.setData(interestItems);
        interestsAdapter.setOnItemSelectedListener(new InterestsAdapter.OnItemSelectedListener() {
            @Override
            public void onItemSelected(int position, boolean isChecked) {
                InterestItem interestItem = interestItems.get(position);
                String interestName = interestItem.getName();
                if (isChecked)
                    mInterestList.add(interestName);
                else
                    mInterestList.remove(interestName);

                interestItem.setChecked(isChecked);
                interestsAdapter.setData(interestItems);
                interestsAdapter.notifyDataSetChanged();
            }
        });

        lvInterestList.setAdapter(interestsAdapter);
        //[END - Set Data]

        dialog.show();
    }

    /**
     * Save new discovery rules to server.
     */
    private void saveDiscoveryRulesToServer() {
        User user = BaseDataManager.getInstance().getUser();
        if (user == null)
            return;

        if (mInterestList.size() < 3) {
            Toast.makeText(getContext(), R.string.set_interests_message, Toast.LENGTH_LONG).show();
            return;
        }

        Map<String, Object> updateData = new HashMap<>();

        String interestData = "";
        for (String interest: mInterestList){
            if (interestData.equals(""))
                interestData = interest;
            else
                interestData = interestData + "/" + interest;
        }

        updateData.put("isAnonymousMode", swtAnonymousMode.isChecked());
        updateData.put("discoverGender", spSelectGender.getSelectedItemPosition());
        updateData.put("discoveryRange", spDiscoveryRange.getSelectedItemPosition());
        updateData.put("ageRange", spAgeRange.getSelectedItemPosition());

        updateData.put("discoveryMode", mDiscoveryMode);
        updateData.put("countryFilter", mCountryFilter);
        updateData.put("onlineFilter", spOnlineFilter.getSelectedItemPosition());
        updateData.put("serviceFilter", spServiceFilter.getSelectedItemPosition());

        updateData.put("discoverNationality", spSelectNationality.getSelectedItem().toString());
        updateData.put("discoveryInterest", interestData);
        updateData.put("discoverableMap", swtDiscoverableMap.isChecked());
        updateData.put("pushNotification", swtPush.isChecked());
        updateData.put("notifications", swtNotifications.isChecked());
        updateData.put("showPhoneNumber", swtShowPhoneNumber.isChecked());
        updateData.put("canSeeMyAge", swtCanSeeAge.isChecked());

        FirebaseDatabase.getInstance().getReference().child("users")
                .child(FirebaseUtils.getUserId()).child("discoveryRules").updateChildren(updateData);

        if (!swtDiscoverableMap.isChecked()) {
            DatabaseReference ref = FirebaseDatabase.getInstance().getReference("positions");
            GeoFire geoFire = new GeoFire(ref);
            String userId = FirebaseAuth.getInstance().getCurrentUser().getUid();
            geoFire.removeLocation(userId);
        }

        Toast.makeText(getActivity(), getString(R.string.save_success_message), Toast.LENGTH_LONG).show();
    }
    //[START - Import Contacts]

    /**
     * Get Contacts.
     * At first, check permission.
     */
    public void getContacts() {
        // Verify that all required contact permissions have been granted.
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.READ_CONTACTS)
                != PackageManager.PERMISSION_GRANTED) {
            // Contacts permissions have not been granted.
            //Log.i(TAG, "Contact permissions has NOT been granted. Requesting permissions.");
            requestContactsPermissions();
        } else {
            // Contact permissions have been granted. Show the contacts fragment.
            //Log.i(TAG, "Contact permissions have already been granted. Displaying contact details.");
            Thread t = new Thread(new Runnable() {
                public void run() {
                    getAllContactsFromAddressBook();
                }
            });
            t.start();
        }
    }

    /**
     * Request contact permission
     */
    private void requestContactsPermissions() {
        // BEGIN_INCLUDE(contacts_permission_request)
        if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                Manifest.permission.READ_CONTACTS)) {

            // Display a SnackBar with an explanation and a button to trigger the request.
            Snackbar.make(getActivity().findViewById(android.R.id.content),
                    R.string.permission_contact_rationale, Snackbar.LENGTH_INDEFINITE)
                    .setAction(R.string.ok, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            ActivityCompat.requestPermissions(getActivity(),
                                    new String[]{Manifest.permission.READ_CONTACTS},
                                    REQUEST_READ_CONTACTS);
                        }
                    })
                    .show();
        } else {
            // Contact permissions have not been granted yet. Request them directly.
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.READ_CONTACTS}, REQUEST_READ_CONTACTS);
        }
        // END_INCLUDE(contacts_permission_request)
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_READ_CONTACTS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    Thread t = new Thread(new Runnable() {
                        public void run() {
                            getAllContactsFromAddressBook();
                        }
                    });

                    t.start();

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }
        }
    }

    /**
     * Get all contacts from address book.
     */
    private void getAllContactsFromAddressBook() {
        ContactObject contactObject;

        ContentResolver contentResolver = getContext().getContentResolver();
        Cursor cursor = contentResolver.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " ASC");
        if (cursor.getCount() > 0) {
            while (cursor.moveToNext()) {

                int hasPhoneNumber = Integer.parseInt(cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER)));
                if (hasPhoneNumber > 0) {
                    String id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
                    String name = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));

                    contactObject = new ContactObject();
                    contactObject.setContactName(name);

                    Cursor phoneCursor = contentResolver.query(
                            ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                            null,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                            new String[]{id},
                            null);
                    if (phoneCursor.moveToNext()) {
                        String phoneNumber = phoneCursor.getString(phoneCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                        contactObject.setContactNumber(phoneNumber);
                    }

                    phoneCursor.close();
                    contactObject.setContactId(Long.parseLong(id));
                    importContactsToServer(contactObject.getContactNumber());
                    FirebaseUtils.updateAchievements(AppConfig.Achievement.ACHIEVEMENT_IMPORT_CONTACTS, getContext());
                }
            }
        }
    }

    /**
     * Query with phoneNumber and add the user to contacts.
     * @param phoneNumber the phoneNumber whose have to be looked for...
     */
    private void importContactsToServer(String phoneNumber) {
        if (TextUtils.isEmpty(phoneNumber))
            return;

        if (phoneNumber.contains("#"))
            return;

        boolean isSameCountry = false;

        final User user = BaseDataManager.getInstance().getUser();
        if (!phoneNumber.contains("+")) {
            String first = phoneNumber.substring(0,1);
            if (first.equals("0")) {
                phoneNumber = user.countryCode + phoneNumber.substring(1);
                isSameCountry = true;
            } else {
                return;
            }
        }

        phoneNumber = phoneNumber.replace(" ", "");
        phoneNumber = phoneNumber.replace("-", "");

        if (isSameCountry && (phoneNumber.length() != user.phoneNumber.length()))
            return;

        //Check own phone number
        if (phoneNumber.equals(user.phoneNumber))
            return;

        DatabaseReference reference = FirebaseDatabase.getInstance().getReference();
        if (reference == null)
            return;

        Query query = reference.child("users").orderByChild("phoneNumber").equalTo(phoneNumber);
        query.addListenerForSingleValueEvent(new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        // do something with the individual "issues"
                        User userData = snapshot.getValue(User.class);
                        userData.key = snapshot.getKey();
                        if (!user.userProfile.isValid)
                            continue;

                        if (!BaseDataManager.isDuplicatedContact(userData.key)) {
                            Map<String, Object> userContact = new HashMap<>();
                            userContact.put("userId", userData.key);

                            FirebaseUtils.getDatabaseReference().child("contacts")
                                    .child(FirebaseUtils.getUserId()).push().updateChildren(userContact);

                            //Send Notification
                            FirebaseUtils.sendNotification(userData.key, AppConfig.Notification.NOTIFICATION_IMPORTED_CONTACT);
                            Toast.makeText(getContext(), R.string.added_user_to_contact, Toast.LENGTH_LONG).show();
                        }
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }
    //[End - Import Contacts]

    @OnClick(R.id.btnImportAddressValue)
    public void onImportAddressBook() {
        getContacts();
    }

    @OnClick(R.id.btnSave)
    public void onSave() {
        saveDiscoveryRulesToServer();
    }

    @OnClick(R.id.spSelectInterests)
    public void onSelectInterests() {
        selectInterests();
    }

    @OnClick(R.id.tvCountryFilter)
    public void onSelectCountry() {
        final CountryPicker picker = CountryPicker.newInstance("SelectCountry");
        picker.setListener(new CountryPickerListener() {
            @Override
            public void onSelectCountry(String name, String code, String dialCode) {
                picker.dismiss();
                setCountry(name, dialCode);
            }
        });

        picker.show(getActivity().getSupportFragmentManager(), "COUNTRY_CODE_PICKER");
    }

    @OnClick(R.id.btnApplyTheme)
    public void onClickApplyTheme() {
        ThemeManager.getInstance().setThemeType(mCurrentThemeType);
        applyTheme();

        //For setting theme style
        Intent intent = new Intent(getActivity(), MainActivity.class);
        startActivity(intent);
        getActivity().finish();
    }

    @OnClick(R.id.btnSalmonColor)
    public void onClickSalmonColor() {
        mCurrentThemeType = ThemeManager.SalmonColor;
        showThemeView.setBackgroundColor(getResources().getColor(R.color.salmon_color));
    }

    @OnClick(R.id.btnMediumGreenColor)
    public void onClickMediumGreenColor() {
        mCurrentThemeType = ThemeManager.MediumGreenColor;
        showThemeView.setBackgroundColor(getResources().getColor(R.color.medium_green_color));
    }

    @OnClick(R.id.btnBlueColor)
    public void onClickBlueColor() {
        mCurrentThemeType = ThemeManager.BlueColor;
        showThemeView.setBackgroundColor(getResources().getColor(R.color.blue_color));
    }

    @OnClick(R.id.btnHotPinkColor)
    public void onClickHotPinkColor() {
        mCurrentThemeType = ThemeManager.HotPinkColor;
        showThemeView.setBackgroundColor(getResources().getColor(R.color.hot_pink_color));
    }

    @OnClick(R.id.btnDarkIndigoColor)
    public void onClickDarkIndigoColor() {
        mCurrentThemeType = ThemeManager.DarkIndigoColor;
        showThemeView.setBackgroundColor(getResources().getColor(R.color.dark_indigo_color));
    }

    @OnClick(R.id.btnDarkSalmonColor)
    public void onClickDarkSalmonColor() {
        mCurrentThemeType = ThemeManager.DarkSalmonColor;
        showThemeView.setBackgroundColor(getResources().getColor(R.color.dark_salmon_color));
    }

    @OnClick(R.id.btnBlueVioletColor)
    public void onClickBlueVioletColor() {
        mCurrentThemeType = ThemeManager.BlueVioletColor;
        showThemeView.setBackgroundColor(getResources().getColor(R.color.blue_violet_color));
    }

    @OnClick(R.id.btnSlatGrayColor)
    public void onClickSlatGrayColor() {
        mCurrentThemeType = ThemeManager.SlatGrayColor;
        showThemeView.setBackgroundColor(getResources().getColor(R.color.slat_gray_color));
    }

    @OnClick(R.id.btnRoyalBlueColor)
    public void onClickRoyalBlueColor() {
        mCurrentThemeType = ThemeManager.RoyalBlueColor;
        showThemeView.setBackgroundColor(getResources().getColor(R.color.royal_blue_color));
    }
}
